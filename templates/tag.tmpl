{#  -*- coding: utf-8 -*- #}
{% extends 'list_post.tmpl' %}
{% import 'feeds_translations_helper.tmpl' as feeds_translations with context %}
{% import 'post_helper.tmpl' as helper with context %}

{% block extra_head %}
    {{ feeds_translations.head(tag, kind, rss_override=False) }}
    <noscript><style>.tag-search{ display: none; }</style></noscript>
{% endblock %}

{% block content %}
<article class="tagpage">
    <header>
        {% if category_path %}
        <nav class="breadcrumbs">
        <ul class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="link://category_index">
                    <img src="/assets/img/open-iconic/home.svg"
                         class="icon" alt="home" />
                </a>
            </li>
        {% for part in category_path %}
            {% set path = '/'.join(category_path[:loop.index]) %}
            {% if not loop.last %}
            <li class="breadcrumb-item">
            {% else %}
            <li class="breadcrumb-item active">
            {% endif %}
                <a href="{{ _link('category', path) }}">{{ part }}</a>
            </li>
            {% else %}
        {% endfor %}
        </ul>
        </nav>
        {% endif %}
        <h1>{{ title|e }}</h1>
        {% if description %}
            <p>{{ description }}</p>
        {% endif %}
        {% if subcategories %}
        {{ messages('Subcategories:') }}
        <ul class="list-inline">
            {% for name, link in subcategories %}
            <li class="list-inline-item"><a class="badge badge-primary" href="{{ link }}">{{ name|e }}</a></li>
            {% endfor %}
        </ul>
        {% endif %}
        {{ feeds_translations.translation_link(kind) }}
    </header>
    {% if posts %}
        <div class="tag-search input-group mb-4">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <img src="/assets/img/open-iconic/magnifying-glass.svg"
                         class="icon" alt="home" />
                </span>
            </div>
            <input type="text" id="search-input" class="form-control"
                   placeholder="Monster name" style="max-width: 20em;"
                   oninput="filterPosts(this)"
                   >
        </div>
        <div class="postlist">
        {% for post in sort_posts(posts, 'title') %}
            <div class="row mb-2 align-items-center" data-slug={{ post.meta('slug') }}>
                <div class="col-sm-3 title">
                    <a href="{{ post.permalink() }}" class="listtitle">{{ post.title()|e }}</a>
                </div>
                <div class="col">
                    {{ helper.html_tags(post) }}
                </div>
            </div>
        {% endfor %}
        </div>
    {% endif %}
</article>
{% endblock %}

{% block extra_js %}
<script src="/assets/js/tag-search.js"></script>
{% endblock %}
