import click
import csv
import os
import re

BESTIARY_DIR = "../bestiary/bfrpg-3rd-edition/core/"
IN_FNAME = "_tags.csv"


def parse_tags_from_csv(csv_file: str):
    "Parses the tags and file slugs from the CSV tag file"
    table = {}
    with open(csv_file, "rt") as in_f:
        reader = csv.reader(in_f)
        header = next(reader)
        for row in reader:
            slug = row[1]
            tags = sorted(t for t in row[2:] if t)
            table[slug] = tags
    return table


@click.command()
@click.option(
    "--bestiary-path",
    type=click.STRING,
    help="Directory containing the bestiary files to process",
)
@click.option(
    "--csv-file",
    type=click.STRING,
    help="The CSV file containing the tags for each creature",
)
def import_tags(csv_file: str, bestiary_path: str):
    "Imports tags from a CSV file and writes them to the matching creature file"

    table = parse_tags_from_csv(csv_file)
    print(f"{len(table)} creatures with tags loaded")

    for fname in os.listdir(bestiary_path):
        path = os.path.join(bestiary_path, fname)
        slug, ext = os.path.splitext(fname)
        if ext == ".rst":
            tags = ", ".join(table[slug])

            print(f"Setting tags for {path}: {tags}")

            text = open(path).read()
            text = re.sub(
                "^\.\. tags:.*$", f".. tags: {tags}", text, count=1, flags=re.MULTILINE
            )
            with open(path, "wt") as f_out:
                f_out.write(text)
        else:
            print(f"Skipping {fname}...")
    print("Done.")


if __name__ == "__main__":
    import_tags()
