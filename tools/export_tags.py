#!/usr/bin/env python
# coding: utf-8
import click
import re
import os


def extract_metadata(f, hit_dice: bool = False):
    "Extracts the metadata from a bestiary entry file"
    data = {}
    pattern = r"\.\. (\w+):\s*(.*)"
    hd_pattern = r"[\s\|]*Hit Dice:[\s\|]*(\d+)"
    # hd_range_pattern = "Hit Dice:\s*\|\s*(\d+) to (\d+)"

    for line in f:
        metadata = re.match(pattern, line)
        if metadata:
            key, val = metadata[1], metadata[2]
            data[key] = val

        if hit_dice:
            hd_match = re.match(hd_pattern, line)
            if hd_match:
                data["hd"] = hd_match[1]

    # Parse the tags
    if "tags" in data:
        data["tags"] = sorted(x.strip() for x in data["tags"].split(","))

    # create and append hit dice tags
    if hit_dice and "hd" in data:
        data["tags"].append(f"hd-{data['hd']}")

    return data


@click.command()
@click.option(
    "--input-path",
    type=click.STRING,
    help="Directory containing the bestiary files to process",
)
@click.option("--output-path", type=click.STRING, help="The CSV file to output")
@click.option(
    "--hit-dice",
    type=click.BOOL,
    is_flag=True,
    help="If set, hit dice values are written as additional tags",
)
def extract_tags(input_path: str, output_path: str, hit_dice: bool):
    "Extracts tags from all creatures in a directory and writes them to a CSV file"

    with open(output_path, "wt") as out_f:
        out_f.write("Title,Slug,Tags\n")
        files = sorted(os.listdir(input_path))
        for fname in files:
            in_path = os.path.join(input_path, fname)
            print(f"Processing {in_path}")
            with open(in_path, "rt") as in_f:
                data = extract_metadata(in_f, hit_dice)
                if data:
                    # print(data)
                    print(f"    Found tags: {data['tags']}")
                    out_f.write(
                        f"\"{data['title']}\",{data['slug']},{','.join(data['tags'])}\n"
                    )


if __name__ == "__main__":
    extract_tags()
