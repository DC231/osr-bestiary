SHELL=/bin/sh
.SILENT:

build:
	nikola build

serve:
	nikola serve --browser

requirements: requirements.in
	pip-compile --resolver=backtracking --output-file=- > requirements.txt

clean:
	echo Cleaning ...
	rm -rf .doit.db.* public/ cache/ __pycache__/
	echo ... done

.PHONY: export_tags
export_tags:
	python ./tools/export_tags.py --input-path "./bestiary/bfrpg-3rd-edition/core" --output-path "./bestiary/bfrpg-3rd-edition/core/_tags.csv" --hit-dice
	python ./tools/export_tags.py --input-path "./bestiary/bfrpg-3rd-edition/field-guide-1" --output-path "./bestiary/bfrpg-3rd-edition/field-guide-1/_tags.csv" --hit-dice
	python ./tools/export_tags.py --input-path "./bestiary/bfrpg-3rd-edition/field-guide-2" --output-path "./bestiary/bfrpg-3rd-edition/field-guide-2/_tags.csv" --hit-dice

.PHONY: import_tags
import_tags:
	python ./tools/import_tags.py --bestiary-path "./bestiary/bfrpg-3rd-edition/core" --csv-file "./bestiary/bfrpg-3rd-edition/core/_tags.csv"
	python ./tools/import_tags.py --bestiary-path "./bestiary/bfrpg-3rd-edition/field-guide-1" --csv-file "./bestiary/bfrpg-3rd-edition/field-guide-1/_tags.csv"
	python ./tools/import_tags.py --bestiary-path "./bestiary/bfrpg-3rd-edition/field-guide-2" --csv-file "./bestiary/bfrpg-3rd-edition/field-guide-2/_tags.csv"