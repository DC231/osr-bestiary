.. title: Derej Mongoose
.. slug: derej-mongoose
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Derej Mongoose
.. type: text
.. image:

+-----------------+-----------------------------+
| Armor Class:    | 13                          |
+-----------------+-----------------------------+
| Hit Dice:       | 1*                          |
+-----------------+-----------------------------+
| No. of Attacks: | 1 bite (special, see below) |
+-----------------+-----------------------------+
| Damage:         | 1d6 bite                    |
+-----------------+-----------------------------+
| Movement:       | 40'                         |
+-----------------+-----------------------------+
| No. Appearing:  | Special                     |
+-----------------+-----------------------------+
| Save As:        | Fighter: 1                  |
+-----------------+-----------------------------+
| Morale:         | 12                          |
+-----------------+-----------------------------+
| Treasure Type:  | None                        |
+-----------------+-----------------------------+
| XP:             | 37                          |
+-----------------+-----------------------------+

The **Derej Mongoose** has 1 attack per round normally, but if it hits an opponent, it adds 1 attack per round for the next round; subsequently, on each round when it hits with all of its attacks, it adds an additional attack per round for the next round. On any round where it misses with at least one attack, the number of attacks it can make in the next round decreases by 1 (to a minimum of 1 attack per round); if it is unable to attack any foe in any given round, the number of attacks it can make returns to 1 upon the next round.
