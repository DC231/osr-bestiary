.. title: Illusion Trapper
.. slug: illusion-trapper
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-9
.. description: Illusion Trapper
.. type: text
.. image:

+-----------------+------------------+
| Armor Class:    | 18               |
+-----------------+------------------+
| Hit Dice:       | 9** (AB +8)      |
+-----------------+------------------+
| No. of Attacks: | 1 bite           |
+-----------------+------------------+
| Damage:         | 5d4 bite         |
+-----------------+------------------+
| Movement:       | 30'              |
+-----------------+------------------+
| No. Appearing:  | 1                |
+-----------------+------------------+
| Save As:        | Fighter: 9       |
+-----------------+------------------+
| Morale:         | 9                |
+-----------------+------------------+
| Treasure Type:  | None (see below) |
+-----------------+------------------+
| XP:             | 1,225            |
+-----------------+------------------+

The **Illusion Trapper** is a very proficient hunter. It digs a 40 foot diameter funnel-shaped pit and casts **hallucinatory** **terrain** to match the surrounding terrain. Creatures that come near the pit must save vs. Paralysis each round they remain in the area or slip on the loose soil and fall to the bottom. The illusion trapper attacks from its covering with its large mandibles, and on a successful hit attaches to the target. It will not open its mandibles until it or its prey is dead. Any creature bitten by the illusion trapper must save vs. Poison or be paralyzed for 3d6 rounds. Paralyzed creatures automatically take 5d4 points of damage each round that it remains in the trapper's grip.

While it does not keep any treasure itself, there is a chance of items left behind from previous victims.
