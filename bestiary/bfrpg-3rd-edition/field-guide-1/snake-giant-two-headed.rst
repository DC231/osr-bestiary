.. title: Snake, Giant Two-Headed
.. slug: snake-giant-two-headed
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-5
.. description: Snake, Giant Two-Headed
.. type: text
.. image:

+-----------------+------------------+
| Armor Class:    | 14               |
+-----------------+------------------+
| Hit Dice:       | 5*               |
+-----------------+------------------+
| No. of Attacks: | 2 bites + poison |
+-----------------+------------------+
| Damage:         | 1d8 bite         |
+-----------------+------------------+
| Movement:       | 30'              |
+-----------------+------------------+
| No. Appearing:  | 1d4              |
+-----------------+------------------+
| Save As:        | Fighter: 5       |
+-----------------+------------------+
| Morale:         | 8                |
+-----------------+------------------+
| Treasure Type:  | None             |
+-----------------+------------------+
| XP:             | 405              |
+-----------------+------------------+

A **Giant Two-Headed Snake** is rarely surprised, as at least one head is usually alert. Each head can attack an individual target, although the targets have to be close to each other. Its poison is quite potent, and saves are made with a -2 penalty.
