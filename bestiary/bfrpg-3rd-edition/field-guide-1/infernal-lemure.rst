.. title: Infernal, Lemure
.. slug: infernal-lemure
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-3
.. description: Infernal, Lemure
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 12         |
+-----------------+------------+
| Hit Dice:       | 3          |
+-----------------+------------+
| No. of Attacks: | 1 claw     |
+-----------------+------------+
| Damage:         | 1d4 claw   |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 5d6        |
+-----------------+------------+
| Save As:        | Fighter: 3 |
+-----------------+------------+
| Morale:         | 6 (11)     |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 145        |
+-----------------+------------+

A **Lemure** is said to be the soul of a damned one, converted into a wretched form to serve more powerful infernals. Its body resembles the one it had in life, covered in bubbling pitch and utterly devoid of speech or intelligence. All other infernals can telepathically control a lemure without effort; while it is thus controlled, a lemure has a morale of 11. A lemure regenerates 1 hp of normal damage per round, even if reduced to 0 hp; damage from magic weapons, fire, spells, or holy water cannot be regenerated in this way.
