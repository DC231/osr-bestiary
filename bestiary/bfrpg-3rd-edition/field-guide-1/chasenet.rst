.. title: Chasenet
.. slug: chasenet
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Chasenet
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 15                    |
+-----------------+-----------------------+
| Hit Dice:       | 1                     |
+-----------------+-----------------------+
| No. of Attacks: | 1 spines/1 bite       |
+-----------------+-----------------------+
| Damage:         | 1d12 spines, 1d4 bite |
+-----------------+-----------------------+
| Movement:       | 60'                   |
+-----------------+-----------------------+
| No. Appearing:  | 1d4                   |
+-----------------+-----------------------+
| Save As:        | Fighter: 1            |
+-----------------+-----------------------+
| Morale:         | 5                     |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 25                    |
+-----------------+-----------------------+

In appearance a **Chasenet** looks like a brightly-colored ball of fluff in a wide range of extravagant colors. A pile of Chasenets sleeping under a tree, as is their wont, can look like a bed of flowers in the distance.

Much like the fox, the chasenet is the darling of the hunting courtier set. A chasenets is extremely fast and agile, which accounts for its rather high AC. A chasenet is generally an inoffensive creature, preferring to run rather than fight, only attacking if cornered.

If cornered, the chasenet will turn and launch itself at its attacker with long porcupine-like spines erupting from its fur. If the chasenet hits, it will continue to attack ferociously by biting its adversary until removed by force or its attacker perishes.
