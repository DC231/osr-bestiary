.. title: Skeleton, Leaded
.. slug: skeleton-leaded
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Skeleton, Leaded
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 16 (see below)    |
+-----------------+-------------------+
| Hit Dice:       | 1                 |
+-----------------+-------------------+
| No. of Attacks: | 1 punch or weapon |
+-----------------+-------------------+
| Damage:         | 1d8 or by weapon  |
+-----------------+-------------------+
| Movement:       | 20'               |
+-----------------+-------------------+
| No. Appearing:  | 2d6               |
+-----------------+-------------------+
| Save As:        | Fighter: 1        |
+-----------------+-------------------+
| Morale:         | 12                |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 25                |
+-----------------+-------------------+

A **Leaded Skeleton** is an altered form of a standard skeleton with a coat of lead over its bones, making it slower but much tougher. It moves slowly like a zombie, and thus always attacks last.

It takes only half damage from edged weapons, and only a single point from arrows, bolts, and sling stones (plus any magical bonus). As with all undead, they can be Turned by a Cleric (as a skeleton), and is immune to **sleep**, **charm**, and **hold** magic. As it is mindless, no form of mind reading is of any use against it. A leaded skeleton never fails morale, and thus always fights until destroyed.
