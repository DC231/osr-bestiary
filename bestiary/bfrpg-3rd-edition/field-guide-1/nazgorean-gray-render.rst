.. title: Nazgorean, Gray Render
.. slug: nazgorean-gray-render
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-10
.. description: Nazgorean, Gray Render
.. type: text
.. image:

+-----------------+---------------------+
| Armor Class:    | 19                  |
+-----------------+---------------------+
| Hit Dice:       | 10 (AB +9)          |
+-----------------+---------------------+
| No. of Attacks: | 2 claws and 1 bite  |
+-----------------+---------------------+
| Damage:         | 1d6 claws, 2d6 bite |
+-----------------+---------------------+
| Movement:       | 30'                 |
+-----------------+---------------------+
| No. Appearing:  | 1                   |
+-----------------+---------------------+
| Save As:        | Fighter: 10         |
+-----------------+---------------------+
| Morale:         | 12                  |
+-----------------+---------------------+
| Treasure Type:  | None                |
+-----------------+---------------------+
| XP:             | 1,300               |
+-----------------+---------------------+

A **Gray Render** stands about 9 feet tall in spite of its hunched posture and is about 4 feet wide, weighing about 4,000 pounds. It has a very amphibian, toad-like look, and is gray in color. Its forelimbs are rather small compared to its body, but have viciously sharp claws. A gray render has no eyes; instead, it depends on sound to locate prey (treat as 90 foot Darkvision). Thus, a silent opponent is as good as invisible to the monster. A gray render is never found in groups. When hunting, it hides and waits for prey to wander close.

A gray render that successfully bites establishes a hold, tearing the flesh for 2d6 points of damage each round until it or its prey is dead. Its jaws are so powerful that it is effectively impossible to break the hold through strength alone.
