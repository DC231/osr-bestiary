.. title: Vampire Spawn*
.. slug: vampire-spawn
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-4
.. description: Vampire Spawn*
.. type: text
.. image:

+----------------+---------------------------------+
| Armor Class:   | 15 ‡                            |
+----------------+---------------------------------+
| Hit Dice:      | 4*                              |
+----------------+---------------------------------+
| No of Attacks: | 1 punch or 1 bite               |
+----------------+---------------------------------+
| Damage:        | 1d6+3 punch, 1d3 bite + special |
+----------------+---------------------------------+
| Movement:      | 30'                             |
+----------------+---------------------------------+
| No. Appearing: | 1d4, Wild 1d4, Lair 2d4         |
+----------------+---------------------------------+
| Save As:       | Fighter: 4                      |
+----------------+---------------------------------+
| Morale:        | 9                               |
+----------------+---------------------------------+
| Treasure Type: | B                               |
+----------------+---------------------------------+
| XP:            | 280                             |
+----------------+---------------------------------+

A **Vampire Spawn** is an undead creature that is created when a vampire slays a mortal. Like their creators, a vampire spawn remains bound to its coffin and to the soil of its grave. It appears much as it did in life, except somehow hardened by its transformation.

A vampire spawn uses its inhuman strength when engaging mortals, hammering its foes with powerful blows. On any natural 20 attack roll, a vampire spawn will bite and drain the blood of its victim. The bite deals 1d3 points of damage and drains one level from the victim; a successful saving throw vs. Death Ray negates the latter effect (see Energy Drain in the **Encounter** section of the Basic Fantasy RPG Core Rules for details). A vampire spawn heals 1d8 hp for each level it drains. Once it bites, the vampire spawn will hold fast and drain the victim each round automatically; only a failed morale check or the death of the vampire spawn will break this hold.

Like vampires, a vampire spawn is unharmed by non-magical weapons and is immune to **sleep**, **charm**, and **hold** spells. A vampire spawn can be Turned by a Cleric (as a vampire). However, unlike its creator it can be slain when its hp is reduced to 0.
