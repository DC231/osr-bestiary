.. title: Canein
.. slug: canein
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Canein
.. type: text
.. image: png

+-----------------+------------------------------------------------------------------------+
| Armor Class:    | 14 (11)                                                                |
+-----------------+------------------------------------------------------------------------+
| Hit Dice:       | 1                                                                      |
+-----------------+------------------------------------------------------------------------+
| No. of Attacks: | 1 bite or 1 weapon                                                     |
+-----------------+------------------------------------------------------------------------+
| Damage:         | 1d4 bite or by weapon                                                  |
+-----------------+------------------------------------------------------------------------+
| Movement:       | 40'                                                                    |
+-----------------+------------------------------------------------------------------------+
| No. Appearing:  | 2d4, Wild 3d6, Lair 10d6                                               |
+-----------------+------------------------------------------------------------------------+
| Save As:        | Fighter: 1 (+2 vs Death Ray or Poison and Paralysis or Petrification). |
+-----------------+------------------------------------------------------------------------+
| Morale:         | 8                                                                      |
+-----------------+------------------------------------------------------------------------+
| Treasure Type:  | D                                                                      |
+-----------------+------------------------------------------------------------------------+
| XP:             | 25                                                                     |
+-----------------+------------------------------------------------------------------------+

**Caneins** are a race of dog-like humanoids known for their extreme sense of loyalty whether to liege, friend, or family. Although only marginally smaller than the average human, there is a great deal of physical variance among the individual caneins; some short and stocky, others lean, and variations in the coloration of their coats. However, all caneins share a similar facial structure similar to the various bulldog or boxer type dog breeds, having jowls and squat features. The honorable caneins follow knight-like codes and attitudes, often serving a patron. Most caneins speak Common or the predominate human language of the region; they have no true language of their own.

A canein has a keen sense of smell, able to identify individuals by scent alone. This also allows the canein to sense the presence of concealed or invisible creatures, and penalties associated with combating such foes are halved. A canein can also track with this ability; tracking a foe who takes no countermeasures to avoid being so tracked has an 80% chance of success, minus 15% for each hour the canein is behind the target. The GM must rule on the effects of any countermeasures taken.
