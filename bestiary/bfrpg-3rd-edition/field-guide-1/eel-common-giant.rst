.. title: Eel, Common & Giant
.. slug: eel-common-giant
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Eel, Common & Giant
.. type: text
.. image: png

+-----------------+------------+------------------------------------------------------------+
|                 | Common     | Giant                                                      |
+=================+============+============================================================+
| Armor Class:    | 11         | 12                                                         |
+-----------------+------------+------------------------------------------------------------+
| Hit Dice:       | 1          | 2, 4, or 6 (*)                                             |
+-----------------+------------+------------------------------------------------------------+
| No. of Attacks: | 1 bite     | 1 bite                                                     |
+-----------------+------------+------------------------------------------------------------+
| Damage:         | 1d6 bite   | 1d8, 1d10 or 1d12                                          |
+-----------------+------------+------------------------------------------------------------+
| Movement:       | 60' Swim   | 60' Swim                                                   |
+-----------------+------------+------------------------------------------------------------+
| No. Appearing:  | Wild 1d6   | Wild 1d6                                                   |
+-----------------+------------+------------------------------------------------------------+
| Save As:        | Fighter: 1 | Fighter: 2, 4, or 6                                        |
+-----------------+------------+------------------------------------------------------------+
| Morale:         | 8          | 8                                                          |
+-----------------+------------+------------------------------------------------------------+
| Treasure Type:  | None       | None                                                       |
+-----------------+------------+------------------------------------------------------------+
| XP:             | 25         | 2 HD 75; 2* HD 100;4 HD 240; 4* HD 280;6 HD 500; 6* HD 555 |
+-----------------+------------+------------------------------------------------------------+

**Common Eels** will often be found along reefs or in other areas where they can hide in holes and nooks in order to ambush prey. They are territorial and aggressive in defense of their lair.

A **Giant Eel** is the huge cousin to the common eel, and is similar in most ways except size. This great aquatic beast is often found in lost underwater ruins or as a guardian raised and trained by underwater races.

1 in 6 giant eels can emit an electrical shock up to three times per day, which will affect those within a 20' radius. The shock causes 1d4 points of damage for each hit die of the giant eel; a saving throw vs. Dragon Breath is allowed for half damage. During a round when an eel uses its shock, it is immune to electrical attacks. On other rounds a giant electric eel has a +2 bonus on saves against electrical attacks.

Elemental*

Elementals are incarnations of the elements that compose existence.

It is possible to summon an elemental by one of three means: By the use of a *staff*, or of a *device*, or by casting a *spell*. For each elemental type, separate statistics are provided for each of these three categories.

Due to their highly magical nature, elementals cannot be harmed by non-magical weapons.

The Core Rules present elementals conforming to the classical elements of European tradition (air, earth, fire, and water). Asian traditions present a different group: fire, earth, metal, water, and wood. Metal and wood elementals are presented below, completing this set (when added to the fire, earth, and water elementals found in the Core Rules). Additionally, cold and lightning elementals are provided for those who wish to be less traditional. As always, the Game Master decides what sort of monsters appear in his or her world.
