.. title: Sphinx
.. slug: sphinx
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-12
.. description: Sphinx
.. type: text
.. image: png

+-----------------+-------------------+
| Armor Class:    | 18                |
+-----------------+-------------------+
| Hit Dice:       | 12* (AB +10)      |
+-----------------+-------------------+
| No. of Attacks: | 2 claws           |
+-----------------+-------------------+
| Damage:         | 2d4+5 claw        |
+-----------------+-------------------+
| Movement:       | 50' Fly 40' (20') |
+-----------------+-------------------+
| No. Appearing:  | 1                 |
+-----------------+-------------------+
| Save As:        | Fighter: 12       |
+-----------------+-------------------+
| Morale:         | 8                 |
+-----------------+-------------------+
| Treasure Type:  | E                 |
+-----------------+-------------------+
| XP:             | 1975              |
+-----------------+-------------------+

A **Sphinx** is a massive winged lion with a human-like face. A typical sphinx is nearly 10 feet long and weighs over 800 pounds. A sphinx has Darkvision with a range of 60 feet, and can **detect magic** and **detect invisible** automatically. It can speak all languages of men and dragons, as well as its own tongue. It prefers witty discourse over open combat, but will defend itself and its home if threatened.

Because its flight is rather clumsy, a sphinx prefers to fight on the ground, tearing with its razor-sharp claws. A sphinx's roar causes all creatures within 100 feet to be affected with **fear** for 2d6 rounds (as the spell) unless they save vs. Spells. After a roar, a sphinx must wait 1d4 rounds before roaring again. A sphinx casts spells as either a Magic-user (40%), Cleric (40%), or sometimes as both (20%) equivalent to half its HD (6th level).
