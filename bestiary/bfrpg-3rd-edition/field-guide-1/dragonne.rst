.. title: Dragonne
.. slug: dragonne
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-9
.. description: Dragonne
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 18                     |
+-----------------+------------------------+
| Hit Dice:       | 9* (AB +8)             |
+-----------------+------------------------+
| No. of Attacks: | 1 bite and 2 claws     |
+-----------------+------------------------+
| Damage:         | 2d6 bite, 2d4 claws    |
+-----------------+------------------------+
| Movement:       | 40' Fly 30'            |
+-----------------+------------------------+
| No. Appearing:  | 1d6 Wild 1d6 Lair 1d10 |
+-----------------+------------------------+
| Save As:        | Fighter: 9             |
+-----------------+------------------------+
| Morale:         | 10                     |
+-----------------+------------------------+
| Treasure Type:  | None                   |
+-----------------+------------------------+
| XP:             | 1,150                  |
+-----------------+------------------------+

A **Dragonne** appears as a strange combination of a lion and dragon, possessing huge claws, fangs, and eyes. Its scales and stiff hair are the color of brass. A dragonne is about 12 feet long and weighs about 700 pounds. It is very intelligent and communicates in one or more languages of its home territory. A dragonne’s wings are useful only for short flights, carrying the creature for 10 to 20 minutes at a time at relatively slow speeds.

A dragonne attacks by biting and clawing. In addition, every 1d4 rounds a dragonne can produce a tremendous roar. To anyone within 120 feet, the roar causes temporary weakness, resulting in a -2 penalty to attack rolls, damage, and any strength checks for 2d6 rounds unless he or she saves vs. Paralysis. Those within 30 feet are also deafened for the same period, with no save allowed. A deafened creature can react only to what it can see or feel, is surprised on 1-3 on 1d6, and suffers a -1 penalty to its initiative rolls. However, a deafened creature is immune to further roars until its deafness alleviates (after the 2d6 rounds).
