.. title: Elemental, Cold*
.. slug: elemental-cold
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-8
.. description: Elemental, Cold*
.. type: text
.. image:

+-----------------+------------+--------------+--------------+
|                 | **Staff**  | **Device**   | **Spell**    |
+=================+============+==============+==============+
| Armor Class:    | 18 ‡       | 20 ‡         | 22 ‡         |
+-----------------+------------+--------------+--------------+
| Hit Dice:       | 8*         | 12* (AB +10) | 16* (AB +12) |
+-----------------+------------+--------------+--------------+
| No. of Attacks: | 1 punch or stomp + special               |
+-----------------+------------+--------------+--------------+
| Damage:         | 1d12       | 2d8          | 3d6          |
+-----------------+------------+--------------+--------------+
| Movement:       | 40' Fly 30'                              |
+-----------------+------------------------------------------+
| No. Appearing:  | -- special --                            |
+-----------------+------------+--------------+--------------+
| Save As:        | Fighter: 8 | Fighter: 12  | Fighter: 16  |
+-----------------+------------+--------------+--------------+
| Morale:         | -- 10 --                                 |
+-----------------+------------------------------------------+
| Treasure Type:  | -- None --                               |
+-----------------+------------+--------------+--------------+
| XP:             | 945        | 1,975        | 3,385        |
+-----------------+------------+--------------+--------------+

A **Cold Elemental** resembles a crude, headless ice statue with long, sharp icicles in place of hands. A cold elemental takes double damage from fire attacks. It deals an additional 1d8 points of damage against creatures that are hot or flaming in nature, as well as creatures made of liquids or oozes. A cold elemental's body is so bitterly cold that creatures within 5 feet take 1d6 points of damage automatically, unless they are immune to the effects of cold. Any liquids the cold elemental touches immediately freezes solid. A cold elemental cannot enter places where the temperature is above 50 degrees Fahrenheit.
