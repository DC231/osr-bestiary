.. title: Terror Bird
.. slug: terror-bird
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-3
.. description: Terror Bird
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 13                 |
+-----------------+--------------------+
| Hit Dice:       | 3                  |
+-----------------+--------------------+
| No. of Attacks: | 1 bite             |
+-----------------+--------------------+
| Damage:         | 1d8 bite           |
+-----------------+--------------------+
| Movement:       | 60'                |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d6, Lair 1d6 |
+-----------------+--------------------+
| Save As:        | Fighter: 3         |
+-----------------+--------------------+
| Morale:         | 8                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 145                |
+-----------------+--------------------+

A **Terror Bird** is a flightless prehistoric bird. It weighs around 800 pounds and is armed with a massive beak; it is for this reason that it is often called an "axe beak”. It hunts in small packs, and aggressively pursues weaker prey.
