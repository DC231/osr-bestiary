.. title: Skragg
.. slug: skragg
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-4
.. description: Skragg
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 14                      |
+-----------------+-------------------------+
| Hit Dice:       | 4*                      |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws / 1 bite        |
+-----------------+-------------------------+
| Damage:         | 1d4 claw, 1d8 bite      |
+-----------------+-------------------------+
| Movement:       | 40' Swim 30'            |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 1d6, Lair 2d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 4              |
+-----------------+-------------------------+
| Morale:         | 10 (8)                  |
+-----------------+-------------------------+
| Treasure Type:  | B, D in lair            |
+-----------------+-------------------------+
| XP:             | 280                     |
+-----------------+-------------------------+

A **Skragg** is a smaller semi-aquatic relative of the common troll, but appears slightly stockier. It is sometimes called a swamp troll. It is more likely to communicate with potential victims rather than just attack, demanding some sort of tribute; it will normally leave would-be victims alone if its demands are met.

A skragg attacks with claws and its teeth. Like a regular troll, it has the ability to regenerate; however, a skragg must have at least 50% of its body immersed in water for its regeneration to take effect. Unlike a typical troll, a skragg regenerates from acid damage, but it fears fire like other trolls; the lower morale rating (in parenthesis) is used when the skragg faces attackers armed with fire.
