.. title: Ape, Winged
.. slug: ape-winged
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-4
.. description: Ape, Winged
.. type: text
.. image: png

+-----------------+------------------------+
| Armor Class:    | 14                     |
+-----------------+------------------------+
| Hit Dice:       | 3                      |
+-----------------+------------------------+
| No. of Attacks: | 2 claws / 1 rock       |
+-----------------+------------------------+
| Damage:         | 1d4 claw, 1d6 rock     |
+-----------------+------------------------+
| Movement:       | 40' Fly 40' (10')      |
+-----------------+------------------------+
| No. Appearing:  | 1d6, Wild 2d4,Lair 2d4 |
+-----------------+------------------------+
| Save As:        | Fighter: 4             |
+-----------------+------------------------+
| Morale:         | 7 (9 in flight)        |
+-----------------+------------------------+
| Treasure Type:  | None                   |
+-----------------+------------------------+
| XP:             | 145                    |
+-----------------+------------------------+

A **Winged Ape** resembles an ordinary carnivorous ape, save for the bat-like wings sprouting from its back. An adult male winged ape is 4 to 5 feet tall and weighs about 200 to 250 pounds. A winged ape often prefers to attack from the air, throwing rocks as far as 50 feet or by dropping them. Each ape can carry aloft 1d4+2 rocks. Attacks against opponents more than 30 feet away (in any direction) are made at -2 to hit.

Aranea

+-----------------+--------------------------------------------------------------------------------------------------------------+
| Armor Class:    | 13                                                                                                           |
+-----------------+--------------------------------------------------------------------------------------------------------------+
| Hit Dice:       | 4**                                                                                                          |
+-----------------+--------------------------------------------------------------------------------------------------------------+
| No. of Attacks: | spider form: 1 bite, web or spellshybrid form: 1 bite, web, spells, or weaponhumanoid form: spells or weapon |
+-----------------+--------------------------------------------------------------------------------------------------------------+
| Damage:         | 1d6 bite+poison or by weapon                                                                                 |
+-----------------+--------------------------------------------------------------------------------------------------------------+
| Movement:       | natural spider form: 50' Climb 30' humanoid or hybrid form: 30'                                              |
+-----------------+--------------------------------------------------------------------------------------------------------------+
| No. Appearing:  | 1d6, Wild 1d6, Lair 1                                                                                        |
+-----------------+--------------------------------------------------------------------------------------------------------------+
| Save As:        | Magic-User: 4                                                                                                |
+-----------------+--------------------------------------------------------------------------------------------------------------+
| Morale:         | 7                                                                                                            |
+-----------------+--------------------------------------------------------------------------------------------------------------+
| Treasure Type:  | D                                                                                                            |
+-----------------+--------------------------------------------------------------------------------------------------------------+
| XP:             | 320                                                                                                          |
+-----------------+--------------------------------------------------------------------------------------------------------------+

An **Aranea** is an intelligent, shape-changing spider-creature with sorcerous powers. It has three distinct forms; in its natural form, one appears as a giant spider having a pair of small arms (about 2 ft long) located just below its fanged mandibles. The second form is a hybrid spider-humanoid form, a sort of a spider-centaur having a multi-eyed spider face. The third form is that of a humanoid which might be any size from halfling to human; other than its dark, coarse hair and slightly bulging eyes, this form is not particularly spider-like. The humanoid form is distinctive; an individual aranea cannot change its humanoid form, either in terms of appearance nor size. An aranea remains in one form until it chooses to assume a new one, and can only change forms once per round.

Aranea generally speak Common and may be able to speak other humanoid languages as well. They have Darkvision with a 60' range, and can cast spells as 4th level magic-users. These powers can be used in any form.

In human or hybrid form the aranea may utilize weapons and other equipment of the same sorts that might be used by normal characters. In these forms one has a movement rate of 30' per round. The humanoid form has no other special abilities beyond spells and weapon use. In spider or hybrid form the aranea may bite; those bitten must save vs. Poison or die, in addition to taking normal damage.

In either hybrid or spider form an aranea may create a web up to six times per day, in a fashion similar to the **web** spell. This effect is not magical; it has a maximum range of 50 feet, and covers at most three 10'x10'x10' cubes (or equivalent volume).

Aranea in spider form may move through any web, whether created by magic, by a giant spider, or by an aranea, at the listed movement rate. This makes an aranea effectively immune to the **web** spell.
