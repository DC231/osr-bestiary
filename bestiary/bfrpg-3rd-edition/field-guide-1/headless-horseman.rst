.. title: Headless Horseman
.. slug: headless-horseman
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-2
.. description: Headless Horseman
.. type: text
.. image:

+-----------------+-----------------------------------+
| Armor Class:    | According to armor worn           |
+-----------------+-----------------------------------+
| Hit Dice:       | 7+**                              |
+-----------------+-----------------------------------+
| No. of Attacks: | 1 weapon (also see Mount)         |
+-----------------+-----------------------------------+
| Damage:         | 1d8 or by weapon (also see Mount) |
+-----------------+-----------------------------------+
| Movement:       | 30' (also see Mount)              |
+-----------------+-----------------------------------+
| No. Appearing:  | 1 (plus Mount)                    |
+-----------------+-----------------------------------+
| Save As:        | Fighter: 7+                       |
+-----------------+-----------------------------------+
| Morale:         | special                           |
+-----------------+-----------------------------------+
| Treasure Type:  | None                              |
+-----------------+-----------------------------------+
| XP:             | 800+ (plus XP of Mount)           |
+-----------------+-----------------------------------+

A **Headless Horseman** is a powerful undead warrior. It appears in knightly garb, similar to what it wore in life. Of course, as it name indicates it is headless, but in lieu of its head it may wear a jack-o-lantern, helmet, or other decoration. Upon sighting a headless horseman, characters of less than 5th level must save vs. Spells or be stricken with fear, running away until out of sight.

Each headless horseman is a Fighter with a level equivalent to its HD, and it attacks appropriately. It can be Turned by Clerics (as a vampire, but roll at -4). As with all undead, it is immune to **sleep**, **charm**, and **hold** spells, as well as cold, acid, poison, and electricity.

A headless horseman is always accompanied by its mount, usually an undead (skeleton or zombie) warhorse or similar creature. Occasionally, a more powerful mount might accompany a higher-level headless horseman, perhaps even an undead dragon. This undead mount is fearless and can only be Turned if the headless horseman itself is successfully Turned first.

Heucova*

+-----------------+-------------------------+
| Armor Class:    | 16 †                    |
+-----------------+-------------------------+
| Hit Dice:       | 2**                     |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws or 1 weapon     |
+-----------------+-------------------------+
| Damage:         | 1d4 claw or by weapon   |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 2d4 |
+-----------------+-------------------------+
| Save As:        | Cleric: 2               |
+-----------------+-------------------------+
| Morale:         | 10                      |
+-----------------+-------------------------+
| Treasure Type:  | D                       |
+-----------------+-------------------------+
| XP:             | 125                     |
+-----------------+-------------------------+

A **Heucova** is a Cleric who has been cursed to undeath for his or her faithlessness. It resembles a skeleton wrapped in old, tattered robes or rusting armor. Small points of red light can be seen in each of its empty eye sockets. A heucova speaks and reads all the languages it knew in life.

A heucova is a cowardly combatant, preferring to set up traps and ambushes for potential interlopers. It will attack Clerics before anyone else. Those struck by the heucova's claws must save vs. Poison or contract a terrible wasting disease. Each day the target takes 1d3 points of Constitution damage. Those reduced to 0 Constitution die, and rise as a zombie on the following day, under the control of the heucova. A **cure** **disease** spell must be used to prevent death. Ability points lost due to a heucova's disease return at a rate of 1 per day of complete rest. All heucova are capable of casting spells as a Cleric (level 1d4+1), however these spells are always reversed.

A heucova can be Turned by a Cleric (as a wight), and like all undead are immune to **sleep**, **charm**, and **hold** spells. A heucova can only harmed by silver or magical weapons. In addition, it takes 1d6 points of damage from the touch of a holy symbol.
