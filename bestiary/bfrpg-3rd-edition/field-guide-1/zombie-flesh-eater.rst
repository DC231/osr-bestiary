.. title: Zombie, Flesh Eater
.. slug: zombie-flesh-eater
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-2
.. description: Zombie, Flesh Eater
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 14                 |
+-----------------+--------------------+
| Hit Dice:       | 2                  |
+-----------------+--------------------+
| No. of Attacks: | 2 claws/1 bite     |
+-----------------+--------------------+
| Damage:         | 1d3 claw, 1d6 bite |
+-----------------+--------------------+
| Movement:       | 40'                |
+-----------------+--------------------+
| No. Appearing:  | 2d8                |
+-----------------+--------------------+
| Save As:        | Fighter: 2         |
+-----------------+--------------------+
| Morale:         | 12                 |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 75                 |
+-----------------+--------------------+

A **Flesh Eater Zombie** is an undead creature similar to a zombie but even more dangerous. Like all undead, it is immune to spells that affect the mind (including **sleep**, **charm**, and **hold**). A flesh eater zombie may be Turned by Clerics (as a zombie). It feasts on the flesh of living creatures, preferring to target intelligent humanoids.

In combat it is surprisingly quick, and attacks with a flurry of claws and bites. While its claws are capable weapons, it is the creature's bite that is most deadly. Those who are bitten and survive have a 5% chance per point of damage of contracting a fatal disease, causing death in 2d4 turns. Those who die from this disease rise in 2d4 rounds as a flesh eater zombie. **Cure** **disease** will prevent death, or if cast on the corpse after death, will prevent the corpse from rising.
