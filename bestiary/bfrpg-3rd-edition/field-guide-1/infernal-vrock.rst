.. title: Infernal, Vrock*
.. slug: infernal-vrock
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-8
.. description: Infernal, Vrock*
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 13 ‡                          |
+-----------------+-------------------------------+
| Hit Dice:       | 8*                            |
+-----------------+-------------------------------+
| No. of Attacks: | 2 claws, 2 talons, 1 bite     |
+-----------------+-------------------------------+
| Damage:         | 1d4 claw, 1d8 talon, 1d6 bite |
+-----------------+-------------------------------+
| Movement:       | 40' Fly 120'                  |
+-----------------+-------------------------------+
| No. Appearing:  | 1d4                           |
+-----------------+-------------------------------+
| Save As:        | Fighter: 8                    |
+-----------------+-------------------------------+
| Morale:         | 11                            |
+-----------------+-------------------------------+
| Treasure Type:  | B                             |
+-----------------+-------------------------------+
| XP:             | 945                           |
+-----------------+-------------------------------+

A **Vrock** is the warrior of the infernals. It comes in many horrible forms, but the most common one resembles a blend of the ugliest features of a man, a vulture, and a bat. A vrock can attack with all five of its attacks while in flight, but cannot use its talons on the ground. A vrock is immune to non-magical weapons. It can cast **darkness**, **detect** **invisibility**, and **telekinesis** at will, and once per day has a 10% chance of summoning one of their own kind to fight alongside it.
