.. title: Derej Rat
.. slug: derej-rat
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Derej Rat
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 13         |
+-----------------+------------+
| Hit Dice:       | 1*         |
+-----------------+------------+
| No. of Attacks: | 1 bite     |
+-----------------+------------+
| Damage:         | 1d6 bite   |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | Special    |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 37         |
+-----------------+------------+

Each time a **Derej Rat** is slain, its original hit points are distributed to any remaining derej rats in its group, until all of the derej rats are destroyed.
