.. title: Mimic
.. slug: mimic
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-7
.. description: Mimic
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 15         |
+-----------------+------------+
| Hit Dice:       | 7**        |
+-----------------+------------+
| No. of Attacks: | 1 slam     |
+-----------------+------------+
| Damage:         | 3d4 slam   |
+-----------------+------------+
| Movement:       | 10'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 7 |
+-----------------+------------+
| Morale:         | 10         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 800        |
+-----------------+------------+

A master of deception, a **Mimic** can assume the general shape of any object or creature that fills roughly 150 cubic feet. A mimic’s body is hard and has a rough texture, no matter what appearance it might present. Anyone who closely examines the mimic can easily detect the ruse. A mimic can speak Common. In combat a mimic will often surprise an unsuspecting adventurer by lashing out with a heavy pseudopod.

A mimic exudes a thick slime that acts as a powerful adhesive, holding fast any creatures or items that touch it. A weapon that strikes an adhesive-coated mimic is stuck fast unless the wielder succeeds on an open doors check to pull the weapon free. Strong alcohol dissolves the adhesive in 3 rounds. A mimic can dissolve its adhesive at will, and the substance will break down 5 rounds after the creature dies.
