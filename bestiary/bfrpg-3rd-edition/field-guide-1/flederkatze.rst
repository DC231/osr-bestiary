.. title: Flederkatze*
.. slug: flederkatze
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-2
.. description: Flederkatze*
.. type: text
.. image: png

+-----------------+-------------------------+
| Armor Class:    | 19 ‡                    |
+-----------------+-------------------------+
| Hit Dice:       | 2**                     |
+-----------------+-------------------------+
| No. of Attacks: | 2 claws, 1 bite +poison |
+-----------------+-------------------------+
| Damage:         | 1d2 claw, 1d3 bite      |
+-----------------+-------------------------+
| Movement:       | 40' Fly 60'             |
+-----------------+-------------------------+
| No. Appearing:  | 1                       |
+-----------------+-------------------------+
| Save As:        | Magic-User: 2           |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 125                     |
+-----------------+-------------------------+

A **Flederkatze** (“flitter-cats”) is a magical creature that appears to be a mix of feline and bat-like features. It has dark fur with leathery wings sprouting from its back. Its head is cat-like but with bat-like ears. A flederkatze can fold its wings close to its body, appearing to be a normal cat unless closely inspected. A flederkatze has exceptional hearing, including a form of echolocation with a 120 foot range; normal invisibility is easily detected, but magical **silence** effectively negates this power. Its actual eyesight is quite poor (roughly 30 feet), and it suffers discomfort in bright sunlight (-1 attack penalty in bright or magical light).

A flederkatze attacks with claws and bite like other felines. Its bite contains a toxin that causes 1 additional point of damage each round for 10 rounds as the poison travels through the body. Each round the affected can roll a save vs. Poison to halt any further damage, although subsequent bites will produce the wounding effect anew (resetting the 10-round duration). Only one such point of poison damage is applied each round, even if multiple bites are scored without successfully saving. In addition to physical attacks, a flederkatze can **detect** **magic** at will, become **invisible** at will, and once per day can **bestow** **curse** (reverse of **remove** **curse**) as a 7th level caster (usually utilizing the “-4 to attack rolls and saves” version).

Silver or magical weapons are required to strike a flederkatze. So long as it has at least 1 hp remaining, the creature regenerates 1 hp each round; if reduced to less than 1 hp a flederkatze dies. It saves against magic (including wands) with a +4 bonus.
