.. title: Glyptodon
.. slug: glyptodon
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-4
.. description: Glyptodon
.. type: text
.. image: png

+-----------------+-----------------------+
| Armor Class:    | 18                    |
+-----------------+-----------------------+
| Hit Dice:       | 4                     |
+-----------------+-----------------------+
| No. of Attacks: | 1 tail or trample     |
+-----------------+-----------------------+
| Damage:         | 1d8 tail, 1d8 trample |
+-----------------+-----------------------+
| Movement:       | 20' (10')             |
+-----------------+-----------------------+
| No. Appearing:  | 1d6                   |
+-----------------+-----------------------+
| Save As:        | Fighter: 4            |
+-----------------+-----------------------+
| Morale:         | 7                     |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 240                   |
+-----------------+-----------------------+

A **Glyptodon** is a prehistoric herbivorous mammal similar to an armadillo, only much larger with a club-like tail. Adult specimens may grow to the size of wagons. It is covered in bony plates, but is usually only dangerous when harassed or attacked.
