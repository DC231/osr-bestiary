.. title: Infernal, Quasit*
.. slug: infernal-quasit
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-2
.. description: Infernal, Quasit*
.. type: text
.. image:

+-----------------+----------------------------+
| Armor Class:    | 19 ‡                       |
+-----------------+----------------------------+
| Hit Dice:       | 2**                        |
+-----------------+----------------------------+
| No. of Attacks: | 2 claws / 1 bite           |
+-----------------+----------------------------+
| Damage:         | 1d2 claw +poison, 1d3 bite |
+-----------------+----------------------------+
| Movement:       | 30'                        |
+-----------------+----------------------------+
| No. Appearing:  | 1                          |
+-----------------+----------------------------+
| Save As:        | Magic-User: 2              |
+-----------------+----------------------------+
| Morale:         | 7                          |
+-----------------+----------------------------+
| Treasure Type:  | None                       |
+-----------------+----------------------------+
| XP:             | 125                        |
+-----------------+----------------------------+

A **Quasit** is a diminutive demonic being, roughly humanoid in shape and standing about 2 feet tall. It is a natural shape-shifter, able to change at will into the form of a gigantic centipede, huge bat, or a wolf, all with horrific visages that set it apart from a normal animal. In all forms the quasit has 60 foot Darkvision.

In its natural demonic form, a quasit attacks with its poisonous claws and bite. The poisonous claws cause an unnatural burning itch that will temporarily reduce the Dexterity of the target by 1 point for each successful attack. The points return 10 minutes after the end of combat. In its other forms, see the relevant monster entry for its attack forms. In addition to physical attacks, a quasit has several magical qualities available in any of its forms. They can **detect** **magic** at will, become **invisible** at will, and once per day can **cause** **fear** (reversed **remove** **fear**) as a 7th level caster.

As an infernal being, a quasit is immune to electrical and poison attacks, and receives only half damage from acid, cold, or fire-based attacks. Magical weapons or spells are required to strike a quasit. In addition, so long as it has at least 1 hp remaining, a quasit regenerates 1 hp every round; if reduced below 1 hp a quasit will die. A quasit saves against magic (including wands) with a +4 bonus.
