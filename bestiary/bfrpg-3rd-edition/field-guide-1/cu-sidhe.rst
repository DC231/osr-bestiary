.. title: Cu-Sidhe
.. slug: cu-sidhe
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Cu-Sidhe
.. type: text
.. image: png

+-----------------+-------------------------+----------------------------+
|                 | Common                  | Special                    |
+=================+=========================+============================+
| Armor Class:    | 16                      | 16 †                       |
+-----------------+-------------------------+----------------------------+
| Hit Dice:       | 1                       | 1** (+2d8 hp enlarged)     |
+-----------------+-------------------------+----------------------------+
| No. of Attacks: | 1 bite                  | 1 bite                     |
+-----------------+-------------------------+----------------------------+
| Damage:         | 1d4 bite                | 1d4 bite (2d4 enlarged)    |
+-----------------+-------------------------+----------------------------+
| Movement:       | 50'                     | 50'                        |
+-----------------+-------------------------+----------------------------+
| No. Appearing:  | 1d4                     | 1d4                        |
+-----------------+-------------------------+----------------------------+
| Save As:        | Fighter: 1(Elf bonuses) | Magic-User: 1(Elf Bonuses) |
+-----------------+-------------------------+----------------------------+
| Morale:         | 8                       | 9                          |
+-----------------+-------------------------+----------------------------+
| Treasure Type:  | None                    | None                       |
+-----------------+-------------------------+----------------------------+
| XP:             | 25                      | 100                        |
+-----------------+-------------------------+----------------------------+

**Cu-Sidhe** are an elven breed of canine. Their fur is patterned like tree bark, giving them excellent camouflage; so long as one remains still, there is only a 10% chance it will be detected in forest terrain. Even indoors, underground, or in non-preferred terrain they are able to hide such that there is only a 30% chance of detection. They have Darkvision with a 60 foot range, and particularly acute canine senses. As with most canines, they prefer to attack as a pack, and will generally avoid combat if met singly.

Most cu-sidhe are much like other wolf or dog breeds, loyal and obedient pets and working dogs. One in six pups born is a special exception, having human-level intellect and the ability to learn magical skills. These special cu-sidhe can **detect** **magic** and **detect** **invisibility** at will. A magic-wielding cu-sidhe can cast a form of **growth of** **animals** on itself twice per day, lasting up to 1 hour. This also grants a temporary 2d8 bonus hit points while enlarged (as if granted by a **potion of heroism**). While enlarged, they may be used as mounts by small or medium characters.

Special cu-sidhe are only hit by weapons which are silver or magical; furthermore, so long as one has at least 1 remaining hit point, it is able to regenerate 1 hit point each round. However, if reduced below 1 hp, a cu-sidhe will die like any other creature. They save vs. all magic with a +4 bonus.
