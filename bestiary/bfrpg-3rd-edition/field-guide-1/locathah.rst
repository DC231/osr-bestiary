.. title: Locathah
.. slug: locathah
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-2
.. description: Locathah
.. type: text
.. image: png

+-----------------+-------------------------------+
| Armor Class:    | 14                            |
+-----------------+-------------------------------+
| Hit Dice:       | 2                             |
+-----------------+-------------------------------+
| No. of Attacks: | 1 spear or 1 light crossbow   |
+-----------------+-------------------------------+
| Damage:         | 1d6 spear, 1d6 light crossbow |
+-----------------+-------------------------------+
| Movement:       | 10' swim 60'                  |
+-----------------+-------------------------------+
| No. Appearing:  | 1d4, Wild 1d20, Lair 3d10+70  |
+-----------------+-------------------------------+
| Save As:        | Fighter: as per hit dice      |
+-----------------+-------------------------------+
| Morale:         | 7                             |
+-----------------+-------------------------------+
| Treasure Type:  | D                             |
+-----------------+-------------------------------+
| XP:             | 75                            |
+-----------------+-------------------------------+

Although humanoid in shape, a **Locathah** is more fish than man. The average locathah stands 5 feet tall and weighs 175 pounds. Females and males look very much alike, although the former can be recognized by the two ochre stripes marking its egg sacs. A locathah speaks its own language.

Any attack from a locathah usually begins with it loosing volleys of bolts from its unique crossbow; this special crossbow has a range of 60 feet underwater as well as normal ranges on the surface. If it manages to set up an ambush or other trap, it continues to employ its crossbow for as long as possible, otherwise wielding a spear. Although primarily used for fishing, this spear makes a formidable weapon. A locathah lacks teeth, claws, and other natural weapons, so it is not especially dangerous if unarmed; it will generally flee if caught unarmed.
