.. title: Bisren
.. slug: bisren
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Bisren
.. type: text
.. image: png

+-----------------+--------------------------------+
| Armor Class:    | 15 (11)                        |
+-----------------+--------------------------------+
| Hit Dice:       | 1+2                            |
+-----------------+--------------------------------+
| No. of Attacks: | 1 gore, charge, or by weapon   |
+-----------------+--------------------------------+
| Damage:         | 1d6 gore, charge, or by weapon |
+-----------------+--------------------------------+
| Movement:       | 40' (subject to encumbrance)   |
+-----------------+--------------------------------+
| No. Appearing:  | 1d8, Wild 5d8, Lair 5d8        |
+-----------------+--------------------------------+
| Save As:        | Fighter: 1                     |
+-----------------+--------------------------------+
| Morale:         | 9                              |
+-----------------+--------------------------------+
| Treasure Type:  | D                              |
+-----------------+--------------------------------+
| XP:             | 25                             |
+-----------------+--------------------------------+

The **Bisren** appear to be related in some way to minotaurs. It appears as a bison-headed humanoid about 7 to 8 feet tall. While a normal minotaur has both distinct humanoid and bull features, a bisren is uniformly hybridized with complete coat coverage, hooves, and a tail. They are normally peaceful nomads. A bisren speaks its own language and most can speak Common as well.

A bisren can gore for 1d6 points of damage with its horns or use weapons. It often charges into battle with a gore attack (+2 to hit with double damage, following all normal charging rules) and then switches to weaponry for the remainder of the fight. It must choose whether to attack with weapons or to gore; it cannot do both in a single round. A bisren has a +1 bonus on feats of strength such as opening doors due to its great mass.
