.. title: Nazgorean, Frogman
.. slug: nazgorean-frogman
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-3
.. description: Nazgorean, Frogman
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 13                    |
+-----------------+-----------------------+
| Hit Dice:       | 3                     |
+-----------------+-----------------------+
| No. of Attacks: | 2 weapons             |
+-----------------+-----------------------+
| Damage:         | 1d8+1 or by weapon +1 |
+-----------------+-----------------------+
| Movement:       | 40'                   |
+-----------------+-----------------------+
| No. Appearing:  | 2d6                   |
+-----------------+-----------------------+
| Save As:        | Fighter: 3            |
+-----------------+-----------------------+
| Morale:         | 10                    |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 145                   |
+-----------------+-----------------------+

A **Frogman** is a seven-foot-tall creature with a body shaped like a muscular humanoid, with a second set of weaker arms below its stronger pair. It is apparently sexless, and its heads are frog-like but set with a second pair of eyes below the normal set. A frogman never bothers to wear any sort of clothing, but does use belts or similar harnesses to support its weapons and equipment.

A frogman actually has two separate but fully cooperative brains. One brain operates the eyes and arms on one side of the body, while the other brain operates the eyes and arms on the other side. One brain is always in control; it is not possible to play one brain against the other. A frogman is generally armed with a single-edged sword, though it will use any sort of one-handed weapon which can be wielded by the stronger upper arms. Two-handed weapons are never used, as a frogman has difficulty with the level of cooperative control needed to use them; the exception is two-handed spears, of which it can wield two each, using one in each set of upper and lower arms.

A frogman can move in nearly complete silence, surprising opponents on 1-3 on 1d6. A frogman receives a +1 bonus on damage due to its great strength.
