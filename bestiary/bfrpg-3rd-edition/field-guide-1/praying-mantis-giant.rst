.. title: Praying Mantis, Giant
.. slug: praying-mantis-giant
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-5
.. description: Praying Mantis, Giant
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 16                 |
+-----------------+--------------------+
| Hit Dice:       | 5                  |
+-----------------+--------------------+
| No. of Attacks: | 1 bite             |
+-----------------+--------------------+
| Damage:         | 1d12 bite          |
+-----------------+--------------------+
| Movement:       | 40' Fly 120'       |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d4, Lair 1d6 |
+-----------------+--------------------+
| Save As:        | Fighter: 5         |
+-----------------+--------------------+
| Morale:         | 8                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 360                |
+-----------------+--------------------+

A **Giant Praying Mantis** is greatly camouflaged creature, moving slowly, and waiting motionless for hours. It ambushes and feeds on anything smaller than itself. It possesses a chameleon-like ability to change its exoskeleton color to blend in with its surroundings. Typically, a hunting praying mantis will surprise on 1-5 of 1d6; locating one that is hiding (from a distance) is equivalent to finding a secret door. It can fly for brief periods, covering 120 feet in a hop, but will only do so to flee or cross rough terrain.
