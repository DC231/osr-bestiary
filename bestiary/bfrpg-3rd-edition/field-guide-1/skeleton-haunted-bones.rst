.. title: Skeleton, Haunted Bones
.. slug: skeleton-haunted-bones
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-3
.. description: Skeleton, Haunted Bones
.. type: text
.. image: jpg

+-----------------+-------------------+
| Armor Class:    | 15 (see below)    |
+-----------------+-------------------+
| Hit Dice:       | 3                 |
+-----------------+-------------------+
| No. of Attacks: | 1 punch or weapon |
+-----------------+-------------------+
| Damage:         | 1d6 or by weapon  |
+-----------------+-------------------+
| Movement:       | 50'               |
+-----------------+-------------------+
| No. Appearing:  | 1d4, Wild 2d4     |
+-----------------+-------------------+
| Save As:        | Fighter: 3        |
+-----------------+-------------------+
| Morale:         | 11                |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 145               |
+-----------------+-------------------+

A **Haunted Bones** is the undead skeletal remains of a fallen warrior possessed by a malicious spirit. Unlike an ordinary mindless skeleton, a haunted bones is controlled by a malevolent intelligence residing within it. It appears as a skeleton clad in the armor and rotten clothes from its former life, moving with an unearthly speed and precision and fighting with deadly skill.

Like an ordinary skeleton, it takes only half damage from edged weapons, and only a single point from arrows, bolts, and sling stones (plus any magical bonus). As with all undead, it can be Turned by a Cleric (as a ghoul), and is immune to **sleep**, **charm**, and **hold** spells.
