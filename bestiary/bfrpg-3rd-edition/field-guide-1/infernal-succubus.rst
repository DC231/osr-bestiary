.. title: Infernal, Succubus*
.. slug: infernal-succubus
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-7
.. description: Infernal, Succubus*
.. type: text
.. image: png

+-----------------+--------------------------------+
| Armor Class:    | 20 ‡                           |
+-----------------+--------------------------------+
| Hit Dice:       | 7**                            |
+-----------------+--------------------------------+
| No. of Attacks: | 2 claws + special or by weapon |
+-----------------+--------------------------------+
| Damage:         | 1d4 claw or by weapon          |
+-----------------+--------------------------------+
| Movement:       | 30' Fly 50'                    |
+-----------------+--------------------------------+
| No. Appearing:  | 1                              |
+-----------------+--------------------------------+
| Save As:        | Cleric: 6                      |
+-----------------+--------------------------------+
| Morale:         | 7                              |
+-----------------+--------------------------------+
| Treasure Type:  | I, L                           |
+-----------------+--------------------------------+
| XP:             | 800                            |
+-----------------+--------------------------------+

A **Succubus** is a female demonic entity. In her natural form, one appears as a beautiful winged humanoid temptress. A succubus can speak any language, and has Darkvision with a range of 120 feet.

A succubus prefers to avoid combat whenever she can, using her spell-like powers instead. When forced to, she can attack with her claws or with a normal weapon. A succubus can use the following spells at will (as a 12th level caster): **charm** **person**, **suggestion**, **darkness 15' radius**, **dimension** **door**,** ESP**, and **clairaudience** (as the potion).

A succubus can change shape at will, and will use this ability to assume a pleasing guise relevant to her chosen target; this deception can be maintained indefinitely.

If the succubus can get a charmed individual alone, she will drain the victim through her kisses. A charmed victim will submit to this willingly. Each round of kissing applies one negative level to the recipient, and all lost hp are transferred to the succubus (even if this temporarily raises her above her normal maximum; excess points are temporary and only last a single day). An unwilling target of such affections (i.e. one not charmed) must be restrained, obviously, but if she can do so she will; draining the life of a victim in this way is still her preferred method of killing.

As an infernal, a succubus is immune to lightning and poison, and takes only half damage from acid, cold, or fire-based attacks. Magical weapons are required to hit a succubus in combat.
