.. title: Banshee*
.. slug: banshee
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-7
.. description: Banshee*
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 19 ‡       |
+-----------------+------------+
| Hit Dice:       | 7**        |
+-----------------+------------+
| No. of Attacks: | 1 touch    |
+-----------------+------------+
| Damage:         | Special    |
+-----------------+------------+
| Movement:       | 60'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 7 |
+-----------------+------------+
| Morale:         | 8          |
+-----------------+------------+
| Treasure Type:  | E          |
+-----------------+------------+
| XP:             | 800        |
+-----------------+------------+

**Banshees** are to the fey what ghosts, wraiths, and spectres are to humans. It usually resembles a colorless, ash-white elf in ragged clothing and chains. It understands whatever languages it spoke in life, but rarely speaks, instead sobbing uncontrollably. Once per day, the banshee's endless weeping reaches a hideous crescendo, and anyone within a 50-foot radius who hears it must save vs. Death Ray or die in 2d6 rounds; those who fail their saving throw may be saved by application of a **remove curse** spell. The touch of a banshee does no damage, but it drains 1d4 levels. Because it is incorporeal, a banshee can only be hit by magic weapons.

A banshee is undead, and thus immune to **sleep**, **charm,** and **hold** spells. It it can be Turned by a Cleric, as a vampire. One can walk on water, but if it crosses running water it loses the ability to drain energy or to wail for 2d12 days.
