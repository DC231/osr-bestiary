.. title: Hippocampus/Kelpie
.. slug: hippocampus-kelpie
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-2
.. description: Hippocampus/Kelpie
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 13                |
+-----------------+-------------------+
| Hit Dice:       | 2/2*              |
+-----------------+-------------------+
| No. of Attacks: | 2 hooves          |
+-----------------+-------------------+
| Damage:         | 1d4 hoof          |
+-----------------+-------------------+
| Movement:       | Swim 80'          |
+-----------------+-------------------+
| No. Appearing:  | Wild 10d10/Wild 1 |
+-----------------+-------------------+
| Save As:        | Fighter: 2        |
+-----------------+-------------------+
| Morale:         | 7                 |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 75/100            |
+-----------------+-------------------+

A **Hippocampus** is the horse of the sea, with a mane that looks like seaweed and a dolphin’s tail. Although they appear to be mammals, they are able to breathe underwater.

A **Kelpie** is the flesh-eating freshwater cousin of the hippocampus, and is able to take the form of a normal horse or human for 2 turns each day. It is cruel and loves nothing more than to drown and devour its victims.
