.. title: Derej Pit Creatures
.. slug: derej-pit-creatures
.. date: 2019-11-08 13:50:17.320434
.. tags: 
.. description: Derej Pit Creatures
.. type: text
.. image:

These creatures were created by Derej the Mage to train slaves destined for the fighting pits. As they were designed for training, the number of these creatures appearing is generally equal to the sum of the levels of the group of adventurers facing them. They will always attack until reduced to zero hit points.

**Derej** **Pit** **Creatures** are easily identified as they are all white with a diamond-shaped red mark on their foreheads. When reduced to zero hit points, a derej pit creature dissolves into a harmless white mist.
