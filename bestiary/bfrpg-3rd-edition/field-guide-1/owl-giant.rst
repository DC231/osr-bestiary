.. title: Owl, Giant
.. slug: owl-giant
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-6
.. description: Owl, Giant
.. type: text
.. image:

+-----------------+-----------------------+
| Armor Class:    | 15                    |
+-----------------+-----------------------+
| Hit Dice:       | 6                     |
+-----------------+-----------------------+
| No. of Attacks: | 2 claws               |
+-----------------+-----------------------+
| Damage:         | 1d6 claw              |
+-----------------+-----------------------+
| Movement:       | 10' Fly 70'           |
+-----------------+-----------------------+
| No. Appearing:  | 1, Lair 1d2, Wild 1d4 |
+-----------------+-----------------------+
| Save As:        | Fighter: 6            |
+-----------------+-----------------------+
| Morale:         | 9                     |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 500                   |
+-----------------+-----------------------+

A **Giant Owl** is a nocturnal bird of prey, feared for its ability to hunt and attack in near silence. It is intelligent and naturally suspicious. A typical giant owl stands about 9 feet tall, has a wingspan of up to 20 feet, and resembles its smaller cousins in nearly every way.

A giant owl attacks by gliding silently just a few feet above its prey and plunging to strike when directly overhead. A giant owl can see five times as far as a human can in dim light.
