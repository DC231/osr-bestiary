.. title: Infernal, Spined Devil*
.. slug: infernal-spined-devil
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-8
.. description: Infernal, Spined Devil*
.. type: text
.. image: png

+-----------------+----------------------------------------------------------+
| Armor Class:    | 19 ‡                                                     |
+-----------------+----------------------------------------------------------+
| Hit Dice:       | 8*                                                       |
+-----------------+----------------------------------------------------------+
| No. of Attacks: | 1 bite or 2 blades (or horns) or spines or by weapon     |
+-----------------+----------------------------------------------------------+
| Damage:         | 1d6 bite + poison, 1d8+2 horn, 4d6 spines or by weapon+2 |
+-----------------+----------------------------------------------------------+
| Movement:       | 30'                                                      |
+-----------------+----------------------------------------------------------+
| No. Appearing:  | 2-5 (wild), 3-18 (lair)                                  |
+-----------------+----------------------------------------------------------+
| Save As:        | Fighter: 8                                               |
+-----------------+----------------------------------------------------------+
| Morale:         | 9                                                        |
+-----------------+----------------------------------------------------------+
| Treasure Type:  | None                                                     |
+-----------------+----------------------------------------------------------+
| XP:             | 945                                                      |
+-----------------+----------------------------------------------------------+

A **Spined Devil** is a fearsome beastman who stands taller than the tallest man. A spined devil appears to be a powerfully-built humanoid with thick gray hides of leather-like scales and small thorny spines protruding from the crevices. A pair of thick bony blades protrude from its elbows; these blades can be retracted into the monster's body at will. A male will also have an impressive rack of curved horns protruding from its head. Spined devils tend to favor spears and swords as weapons.

Magic or magical weapons are needed to harm a spined devil, which are immune to non-magical fire and poison; further, they take only half damage from magical fire. The bite of a spined devil is poisonous, but not fatal; anyone bitten must make a saving throw vs. Poison or fall unconscious for 1d6 minutes. Once every 1d6 rounds, a spined devil can fire a burst of small barbed spines from its arms and legs, covering a 5 foot radius around the monster; any creature within the area takes 4d6 points of damage from the spines. A successful saving throw vs. Dragon Breath reduces damage from this attack by half.
