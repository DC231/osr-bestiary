.. title: Faun (and Ibix)
.. slug: faun-and-ibix
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Faun (and Ibix)
.. type: text
.. image: jpg

+-----------------+---------------------------------+
| Armor Class:    | 15 (11)                         |
+-----------------+---------------------------------+
| Hit Dice:       | 1                               |
+-----------------+---------------------------------+
| No. of Attacks: | 1 miniature weapon              |
+-----------------+---------------------------------+
| Damage:         | 1d6 or by weapon                |
+-----------------+---------------------------------+
| Movement:       | 40'                             |
+-----------------+---------------------------------+
| No. Appearing:  | 1d8, Wild 5d8, Lair 5d8         |
+-----------------+---------------------------------+
| Save As:        | Fighter: 1 (with Dwarf bonuses) |
+-----------------+---------------------------------+
| Morale:         | 8                               |
+-----------------+---------------------------------+
| Treasure Type:  | D                               |
+-----------------+---------------------------------+
| XP:             | 25                              |
+-----------------+---------------------------------+

A **Faun** is a fey-related race that resembles a sort of strange cross of goat with a small human or elf-like being. Standing only about 4 to 5 feet tall, it has a human-like torso and head, but the legs and feet of a goat. A faun can also have other features reminiscent of goats, such as small horns or large ears. Fauns share the halfling’s love of a simple agrarian life, and frequently are found wherever alcoholic beverages are made.

Fauns do not have their own language, preferring to speak Elvish amongst themselves. They also know the language of Halflings, their most common neighbors, and many also know the secret languages of fey races such as pixies or dryads. Most adventuring fauns who travel outside their small shires know Common.

The **Ibix** is the cousin to the faun, with a head that is much more goat-like. Unlike fauns, an ibix is ill-tempered and generally considered evil, sometimes even allying with humanoids such as goblins. It has identical statistics to those listed above, except that it speaks Goblin rather than Halfling.
