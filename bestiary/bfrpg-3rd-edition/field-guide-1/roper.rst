.. title: Roper*
.. slug: roper
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-10
.. description: Roper*
.. type: text
.. image:

+-----------------+-------------------------------------+
| Armor Class:    | 20                                  |
+-----------------+-------------------------------------+
| Hit Dice:       | 10* (AB +9) to 12* (AB +10)         |
+-----------------+-------------------------------------+
| No. of Attacks: | 1 bite + special                    |
+-----------------+-------------------------------------+
| Damage:         | 2d6 bite                            |
+-----------------+-------------------------------------+
| Movement:       | 10'                                 |
+-----------------+-------------------------------------+
| No. Appearing:  | 1                                   |
+-----------------+-------------------------------------+
| Save As:        | Fighter: 10 to 12                   |
+-----------------+-------------------------------------+
| Morale:         | 12                                  |
+-----------------+-------------------------------------+
| Treasure Type:  | I                                   |
+-----------------+-------------------------------------+
| XP:             | 10 HD 1,390; 11 HD 1,67012 HD 1,975 |
+-----------------+-------------------------------------+

A **Roper** stands 9 feet tall and tapers from 3 or 4 feet in diameter at the base to 1 foot across at the top. It weighs 2,200 pounds. A roper has 60 foot Darkvision and its coloration and temperature changes to match the features of the surrounding cave. It hunts prey by standing very still and imitating rock. This tactic often allows the roper to attack with surprise. When prey comes within reach, it lashes out with its strands. In melee, it bites adjacent opponents with its powerful maw.

If a roper hits with a strand attack, the strand latches onto the opponent’s body. This deals no damage but drags the stuck opponent 10 feet closer each subsequent round unless that creature breaks free (requires an open doors check). If a roper can draw in a creature within 10 feet of itself, it will bite with a +4 attack bonus in the same round. A strand has 10 hp and can be attacked instead of the body of the roper. If the strand is currently attached to a target, the attacker takes a -4 penalty on its attack roll. Severing a strand deals no damage to the roper itself.

A roper is immune to electricity and takes half damage from cold but is vulnerable against fire, saving at -4. It is otherwise very highly magically resistant, making all magical saves (except fire-based) with a +4 bonus.
