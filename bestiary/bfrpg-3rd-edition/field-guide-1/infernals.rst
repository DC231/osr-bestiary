.. title: Infernals
.. slug: infernals
.. date: 2019-11-08 13:50:17.320434
.. tags: 
.. description: Infernals
.. type: text
.. image:

Infernal beings are monstrosities with otherworldly or extra-dimensional origins. These beings are universally vile and at odds with the powers of goodness. There are several distinct races or groups of infernal beings, generally grouped by their origin. They might be called demons, devils, or other related terms.
