.. title: Celestial, Solar
.. slug: celestial-solar
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-7
.. description: Celestial, Solar
.. type: text
.. image: png

+-----------------+-------------+
| Armor Class:    | 17          |
+-----------------+-------------+
| Hit Dice:       | 7**         |
+-----------------+-------------+
| No. of Attacks: | 1 weapon    |
+-----------------+-------------+
| Damage:         | Weapon + 2  |
+-----------------+-------------+
| Movement:       | 50' Fly 90' |
+-----------------+-------------+
| No. Appearing:  | 1           |
+-----------------+-------------+
| Save As:        | Cleric: 10  |
+-----------------+-------------+
| Morale:         | 11          |
+-----------------+-------------+
| Treasure Type:  | E           |
+-----------------+-------------+
| XP:             | 800         |
+-----------------+-------------+

A **Solar** is a zealous champion of justice. It may appear in a variety of forms, but in general it is a very tall and beautiful humanoid figure, with at least one set of golden wings like those of a giant eagle. A solar knows the languages of all but the most utterly mindless of creatures.

A solar will usually fight in an honorable manner, but is not above pressing an obvious advantage. A solar casts spells as a 9th level Cleric, and can detect the surface thoughts of any creature within 100 feet. Additionally, it is capable of Turning undead and unholy creatures as a Cleric of 9th level.

A solar takes only half damage from non-magical weapons, and they are immune to **sleep**, **hold**, and **charm** spells, as well as illusions.
