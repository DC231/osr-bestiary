.. title: Sea Cat
.. slug: sea-cat
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-11
.. description: Sea Cat
.. type: text
.. image: png

+-----------------+--------------------+
| Armor Class:    | 18                 |
+-----------------+--------------------+
| Hit Dice:       | 11 (AB +9)         |
+-----------------+--------------------+
| No. of Attacks: | 2 claws/1 bite     |
+-----------------+--------------------+
| Damage:         | 1d6 claw, 1d8 bite |
+-----------------+--------------------+
| Movement:       | 10' Swim 40'       |
+-----------------+--------------------+
| No. Appearing:  | 1, Wild 1d10       |
+-----------------+--------------------+
| Save As:        | Fighter: 11        |
+-----------------+--------------------+
| Morale:         | 12                 |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 1,575              |
+-----------------+--------------------+

A **Sea Cat** is an aquatic hybrid having a feline head and a fish body. A typical sea cat is 12 feet long and weighs 800 pounds, and is sea green in color.

A sea cat attacks on sight, either for food or to defend its territory, and uses both claws and teeth. It displays amazing courage, fighting to the death even against creatures many times its size. Pairs and prides of sea cats attack in concert, trying to wear the opponent down until one beast can dispatch it.

A sea cat that hits with both claw attacks deals an extra 2d6 points of rending damage.
