.. title: Gerbalaine
.. slug: gerbalaine
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Gerbalaine
.. type: text
.. image:

+-----------------+--------------------------------+
| Armor Class:    | 15                             |
+-----------------+--------------------------------+
| Hit Dice:       | 1                              |
+-----------------+--------------------------------+
| No. of Attacks: | 1 punch or weapon (large form) |
+-----------------+--------------------------------+
| Damage:         | 1d4 or weapon (large form)     |
+-----------------+--------------------------------+
| Movement:       | 40'                            |
+-----------------+--------------------------------+
| No. Appearing:  | 1d6, Wild 2d4, Lair 4d8        |
+-----------------+--------------------------------+
| Save As:        | Fighter: 1 (Halfling bonuses)  |
+-----------------+--------------------------------+
| Morale:         | 6                              |
+-----------------+--------------------------------+
| Treasure Type:  | 1d4 random small gems          |
+-----------------+--------------------------------+
| XP:             | 25                             |
+-----------------+--------------------------------+

**Gerbalaines** are a very small race of fey beings. It has a mouse-like appearance, and because of its size it is often mistaken for a common field mouse unless examined closely. A gerbalaine is a tinkerer, using small bits of materials gathered to fashion its home; it often builds within walls, under floors, or otherwise right under the noses of big folk.

Up to 3 times per day, a gerbalaine may magically assume a larger form, growing to approximately halfling size. It is this form that gives the above statistics when pressed into a fight, although a gerbalaine is more likely to run away than fight. When in its natural mouse-size form, a gerbalaine has effectively only 1 hp, but is very difficult to hit (AC 22). A gerbalaine who saves (with Halfling bonuses) against an area-of-effect damaging spell takes no damage, and even if the save fails takes only half damage. A gerbalaine's skill with devices is comparable to a 10th-level Thief.
