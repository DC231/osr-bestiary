.. title: Nazgorean, Digester
.. slug: nazgorean-digester
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-12
.. description: Nazgorean, Digester
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 17                      |
+-----------------+-------------------------+
| Hit Dice:       | 12* (AB +10)            |
+-----------------+-------------------------+
| No. of Attacks: | 1 claw + special        |
+-----------------+-------------------------+
| Damage:         | 1d8 claw + special      |
+-----------------+-------------------------+
| Movement:       | 60'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1, Wild 1d3, Lair 1d3+3 |
+-----------------+-------------------------+
| Save As:        | Fighter: 12             |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 1,975                   |
+-----------------+-------------------------+

A **Digester** is a bizarre otherworldly being from Nazgor that seems to only exist to hunt and eat. It is about 5 feet tall, 7 feet long from snout to tail, and superficially similar to a bipedal dinosaur except for an odd aperture on its head that sprays digestive juices at prey. A digester does not have discernible eyes, locating prey by sound (treat as Darkvision to 60 feet); magical **silence** effectively blinds it.

A digester attacks by spraying a gout of acid in a cone 20 feet long and 20 feet wide at the base, causing 8d8 points of damage to everything in the area. A successful save vs. Dragon Breath will reduce damage by half. The digester can spray again every 1d4 rounds and otherwise attacks with one hind-claw for 1d8 points of damage.
