.. title: Death Dragon
.. slug: death-dragon
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-11
.. description: Death Dragon
.. type: text
.. image:

+-----------------+---------------------------------------------+
| Armor Class:    | 22                                          |
+-----------------+---------------------------------------------+
| Hit Dice:       | 11** (AB +9)                                |
+-----------------+---------------------------------------------+
| No. of Attacks: | 2 claws + paralysis/1 bite or breath/1 tail |
+-----------------+---------------------------------------------+
| Damage:         | 1d8 claws, 4d8 bite or breath, 1d8 tail     |
+-----------------+---------------------------------------------+
| Movement:       | 30' Fly 80'                                 |
+-----------------+---------------------------------------------+
| No. Appearing:  | 1                                           |
+-----------------+---------------------------------------------+
| Save As:        | Fighter: 11                                 |
+-----------------+---------------------------------------------+
| Morale:         | 10                                          |
+-----------------+---------------------------------------------+
| Treasure Type:  | H                                           |
+-----------------+---------------------------------------------+
| XP:             | 1,765                                       |
+-----------------+---------------------------------------------+

A **Death Dragon** is a skeletal monster, a sort of "dragon lich" who has chosen to become undead for reasons inscrutable to mortals. In place of whatever breath weapon it had in life, a death dragon breathes a cloud of freezing fog. In addition to dealing damage, this breath inflicts **mummy rot** (see **mummy** in the Core Rules for details) on those affected unless they save vs. Death Ray. Its claws inflict paralysis (like a ghoul), but elves are not immune; a saving throw vs. Paralysis is allowed to resist.
