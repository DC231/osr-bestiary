.. title: Darkmantle
.. slug: darkmantle
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-1
.. description: Darkmantle
.. type: text
.. image:

+-----------------+------------------+
| Armor Class:    | 17               |
+-----------------+------------------+
| Hit Dice:       | 1+2*             |
+-----------------+------------------+
| No. of Attacks: | 1 constriction   |
+-----------------+------------------+
| Damage:         | 1d4 constriction |
+-----------------+------------------+
| Movement:       | 20' Fly 60'      |
+-----------------+------------------+
| No. Appearing:  | 2d6              |
+-----------------+------------------+
| Save As:        | Fighter: 1       |
+-----------------+------------------+
| Morale:         | 7                |
+-----------------+------------------+
| Treasure Type:  | None             |
+-----------------+------------------+
| XP:             | 37               |
+-----------------+------------------+

While at rest, a **Darkmantle** looks much like a stalactite. Using a muscular 'foot' it attaches itself to the ceiling, its tentacles wrapped about its body. A darkmantle weighs about 30 pounds and can change its skin color to match the surrounding stone. It effectively has 90 foot Darkvision, achieved through a form of echolocation. Magical silence effectively blinds a darkmantle.

A darkmantle attacks by dropping onto its prey and wrapping its tentacles around the opponent’s head to constrict and suffocate its foe for 1d4 points of damage. A darkmantle that misses its initial attack will usually fly up and try to drop on the opponent again. After successfully attacking, the darkmantle automatically inflicts 1d4 points of damage each round. While attached to an individual, a darkmantle takes half damage from attacks; the other half is inflicted upon the individual it covers. Once per day a darkmantle can cast **darkness** (the reverse of **light**, with a 6-turn duration). It most often uses this ability just before attacking.
