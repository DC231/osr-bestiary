.. title: Zombraire (and Skeletaire)
.. slug: zombraire-and-skeletaire
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-2
.. description: Zombraire (and Skeletaire)
.. type: text
.. image: png

+-----------------+---------------------+---------------------+
|                 | Zombraire           | Skeletaire          |
+=================+=====================+=====================+
| Armor Class:    | 12 (see below)      | 13 (see below)      |
+-----------------+---------------------+---------------------+
| Hit Dice:       | 2* (variable)       | 1* (variable)       |
+-----------------+---------------------+---------------------+
| No. of Attacks: | 1 dagger or 1 spell | 1 dagger or 1 spell |
+-----------------+---------------------+---------------------+
| Damage:         | 1d4 or per spell    | 1d4 or per spell    |
+-----------------+---------------------+---------------------+
| Movement:       | 20'                 | 40'                 |
+-----------------+---------------------+---------------------+
| No. Appearing:  | 1                   | 1                   |
+-----------------+---------------------+---------------------+
| Save As:        | Magic-User: by HD   | Magic-User: by HD   |
+-----------------+---------------------+---------------------+
| Morale:         | 9 to 12 (see below) | 12                  |
+-----------------+---------------------+---------------------+
| Treasure Type:  | None                | None                |
+-----------------+---------------------+---------------------+
| XP:             | 100 (variable)      | 37                  |
+-----------------+---------------------+---------------------+

A **Zombraire** is a free-willed undead Magic-user. Like the zombie it resembles, a zombraire moves silently, is very strong, and must be literally hacked to pieces to be destroyed. However, it does not suffer the initiative penalty common to ordinary zombies. It takes only half damage from blunt weapons, and only a single point from arrows, bolts, and sling stones (plus any magical bonus). It may be Turned by a Cleric (as a wight), and is immune to **sleep**,** charm**, and** hold** spells.

A zombraire slowly rots away, and as it does it loses its sanity; this is represented by the variable morale listed. An insane zombraire fights to the death in hopes of being slain, thus ending its tortured existence.

The given statistics are for a zombraire formed from a 2nd-level Magic-user; the HD and saving throws of a zombraire are based on the level it had in life. A zombraire can cast spells as it did when living, but cannot learn new spells.

A **Skeletaire** is the final form of a zombraire which has rotted away completely. It takes only half damage from edged weapons, and only a single point from arrows, bolts, and sling stones (plus any magical bonus). It can be Turned by a Cleric (as a zombie), and is immune to **sleep**,** charm**, and **hold** spells. A skeletaire never fails morale, and thus always fights until destroyed.

The statistics above are for a skeletaire formed from a 2nd-level Magic-user. A skeletaire will have HD equal to the character's level minus 1, and will save as a Magic-user of the level equal to its HD. The skeletaire cannot speak, but still retains the ability to prepare and cast spells as it did in life (but like a zombraire, it cannot learn new spells).
