.. title: Boggart
.. slug: boggart
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-6
.. description: Boggart
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 14            |
+-----------------+---------------+
| Hit Dice:       | 6*            |
+-----------------+---------------+
| No. of Attacks: | 2 claws       |
+-----------------+---------------+
| Damage:         | 1d6 claw      |
+-----------------+---------------+
| Movement:       | 40'           |
+-----------------+---------------+
| No. Appearing:  | 1             |
+-----------------+---------------+
| Save As:        | Magic-User: 1 |
+-----------------+---------------+
| Morale:         | 7             |
+-----------------+---------------+
| Treasure Type:  | D             |
+-----------------+---------------+
| XP:             | 555           |
+-----------------+---------------+

What a **Boggart's** true form is none can say, but given the marks they leave on the bodies of its victims, one can be reasonably sure that it is clawed. A boggart does not appear to truly understand language, but it is capable of imitating a wide range of sounds, including speech. It feeds on fear, especially from a creature about to be slain.

A boggart prefers not to attack with its claws until it absolutely has to; instead it will use its inherent magical abilities. A boggart has a passive form of telepathy that allows it to know the greatest fear of any creature they see and project an illusory image of it. Creatures of 1 HD or less that view such an image must save vs. Death Ray or die of fright. Should this fail, the boggart will resort to its claws. While it relishes the sound of screaming, the boggart finds laughter unbearable and must check morale if it hears the sounds of genuine mirth.

In all other respects the illusion of a boggart functions like the spell **phantasmal** **force**. Although intelligent, a boggart is not affected by **charm** or **sleep** spells, nor illusions of any kind.
