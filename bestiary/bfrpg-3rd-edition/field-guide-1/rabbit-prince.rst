.. title: Rabbit Prince
.. slug: rabbit-prince
.. date: 2019-11-08 13:50:17.320434
.. tags: hd-10
.. description: Rabbit Prince
.. type: text
.. image: png

+-----------------+------------------+
| Armor Class:    | 18               |
+-----------------+------------------+
| Hit Dice:       | 10* (AB +9)      |
+-----------------+------------------+
| No. of Attacks: | 1 weapon (sword) |
+-----------------+------------------+
| Damage:         | 2d6 sword        |
+-----------------+------------------+
| Movement:       | 50' Leap 20'     |
+-----------------+------------------+
| No. Appearing:  | 1                |
+-----------------+------------------+
| Save As:        | Fighter: 10      |
+-----------------+------------------+
| Morale:         | 10               |
+-----------------+------------------+
| Treasure Type:  | None             |
+-----------------+------------------+
| XP:             | 1,390            |
+-----------------+------------------+

The **Rabbit Prince** appears as a four foot tall, bipedal rabbit dressed in ruined finery, wearing an old crown and wielding a broken sword. A rabbit prince will roam the wilderness, occasionally attacking farming villages. It has a deep hatred for predators and will defend fellow (ordinary) rabbits from any attackers it sees.

The rabbit prince attacks with supernatural strength, moving rapidly and erratically so that anyone trying to attack with ranged weapons suffers a -2 penalty to his or her attack roll. In addition to normal movement, a rabbit prince can leap up to 20 feet and still make an attack in the same round.

Instead of attacking, a rabbit prince may rapidly thump its foot loudly upon the ground. Anyone within 20 feet must save vs. Paralysis or be unable to cast spells for the remainder of that round (but other actions are still possible). Any spells which would be cast simultaneously with the rabbit prince's initiative are lost, just as if the caster had been attacked.

A rabbit prince is a solitary creature, almost never found in each others' company.
