.. title: Armorer*
.. slug: armorer
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-4
.. description: Armorer*
.. type: text
.. image:

+-----------------+-----------------------------------------------+
| Armor Class:    | Same as armor type ‡                          |
+-----------------+-----------------------------------------------+
| Hit Dice:       | 4*                                            |
+-----------------+-----------------------------------------------+
| No. of Attacks: | Special                                       |
+-----------------+-----------------------------------------------+
| Damage:         | Leather: 1d6; Chainmail: 1d8; Platemail: 1d10 |
+-----------------+-----------------------------------------------+
| Movement:       | Host’s movement rate                          |
+-----------------+-----------------------------------------------+
| No. Appearing:  | 1                                             |
+-----------------+-----------------------------------------------+
| Save As:        | Same as host                                  |
+-----------------+-----------------------------------------------+
| Morale:         | 11                                            |
+-----------------+-----------------------------------------------+
| Treasure Type:  | None                                          |
+-----------------+-----------------------------------------------+
| XP:             | 400                                           |
+-----------------+-----------------------------------------------+

An **Armorer** is a creature that changes its shape and latches onto an armored host to replace armor of any sort, including magical sets. When this creature is worn it grants the same armor class as the armor type it replaced, but does not replicate any other special abilities. An armorer will retain the same form until scared off its host or killed.

When a host is badly hurt, the armorer will attack the host. The damage done depends on the type of armor that was assumed. Because of this, the armorer inflicts damage depending on the armor type. The damage is 1d6 for leather armor, 1d8 for chainmail, and 1d10 for platemail. Once the host is killed, the armorer will live off its corpse until another suitable host is found. Once a new host is found, the armorer will drop the corpse and replace the armor that the new host is wearing.

The armorer can only be hit by magical weapons or spells. If the host is attacked by magical weapons or spells the armorer will take equal damage. If the armorer dies on a host, the armor the host originally wore re-appears nearby, returning from the extra-dimensional space it was being held in.
