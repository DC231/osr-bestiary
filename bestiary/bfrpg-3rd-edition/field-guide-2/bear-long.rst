.. title: Bear, Long
.. slug: bear-long
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-7
.. description: Bear, Long
.. type: text
.. image: png

+-----------------+-----------------------------------------+
| Armor Class:    | 16                                      |
+-----------------+-----------------------------------------+
| Hit Dice:       | 7                                       |
+-----------------+-----------------------------------------+
| No. of Attacks: | 2 claws/1 bite plus special             |
+-----------------+-----------------------------------------+
| Damage:         | 2d4 claw, 2d6 bite, special (see below) |
+-----------------+-----------------------------------------+
| Movement:       | 40' (20’)                               |
+-----------------+-----------------------------------------+
| No. Appearing:  | 1d6                                     |
+-----------------+-----------------------------------------+
| Save As:        | Fighter: 7                              |
+-----------------+-----------------------------------------+
| Morale:         | 7                                       |
+-----------------+-----------------------------------------+
| Treasure Type:  | None                                    |
+-----------------+-----------------------------------------+
| XP:             | 670                                     |
+-----------------+-----------------------------------------+

A **Long Bear** is an abomination created by some botched **growth** or **enlargement** magic. One is over 30’ long and has a dozen legs, and may be found in a variety of colors ranging from black to golden tan.

It attacks first with its claws and powerful bite. Where a normal bear can only has one pair of forelegs with which to hug, a long bear effectively has many pairs of forelegs; if both of its normal claw attacks hit, it hugs for an additional 2d6 points of damage, and may roll two additional attacks for the second pair of forelegs, doing the same claw damage as for the first. If both of these attacks hit, it may proceed to the next pair in the same way. However, a long bear may only attack and hug with one pair of forelegs if the opponent is small, at most two if the opponent is medium (i.e. man sized), and at most three pairs if the opponent is large sized. If a long bear's attacks are not all against the same opponent, it may not hug, but if both paws in one "rank" hit, it may then attack with the next rank, and even a third if both of the second-rank attacks hit. In no case can it perform a hug if it has attacked more than one opponent in the round.
