.. title: Shambling Mound, Lesser
.. slug: shambling-mound-lesser
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-3
.. description: Shambling Mound, Lesser
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 19         |
+-----------------+------------+
| Hit Dice:       | 3**        |
+-----------------+------------+
| No. of Attacks: | 2          |
+-----------------+------------+
| Damage:         | 2d4/2d4    |
+-----------------+------------+
| Movement:       | 20'        |
+-----------------+------------+
| No. Appearing:  | 1d4        |
+-----------------+------------+
| Save As:        | Fighter: 3 |
+-----------------+------------+
| Morale:         | 9          |
+-----------------+------------+
| Treasure Type:  | C          |
+-----------------+------------+
| XP:             | 205        |
+-----------------+------------+

**Shambling Mounds**, also called shamblers, appear to be heaps of rotting vegetation 6’ tall and weighing about 500 pounds. They are actually intelligent carnivorous plants. A shambler’s brain and sensory organs are located in its upper body.

A shambling mound is immune to lightning, and in fact gains a hit die each time it is attacked by lightning, up to a maximum 3 additional hit dice; these hit dice are lost at a rate of one per hour, while hit points granted in this way are lost first when the monster takes damage. Further, it is resistant to both cold and fire, suffering no damage on a successful savings throw or half damage if the save fails.

If a shambling mound hits with both its attacks against the same creature, that creature is engulfed by the monster. The victim can no longer attack or cast spells, and suffers 1d6 points of damage each round due to suffocation. It can only engulf man-sized or smaller creatures and may only engulf one such creature at a time. It will expel any such creature from its body 1d4 rounds after the victim dies. Attacks against a shambling mound which has engulfed a victim require a savings throw vs. Death Ray for the victim each time the monster is hit; if the save fails, the victim suffers half (rounded down) of the damage done to the monster (which still takes full damage). The victim does benefit from the monster's resistance to fire or cold, but takes full damage from lightning attacks.
