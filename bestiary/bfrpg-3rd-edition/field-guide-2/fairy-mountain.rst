.. title: Fairy, Mountain*
.. slug: fairy-mountain
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-10
.. description: Fairy, Mountain*
.. type: text
.. image: jpg

+-----------------+---------------------+
| Armor Class:    | 18‡                 |
+-----------------+---------------------+
| Hit Dice:       | 10*                 |
+-----------------+---------------------+
| No. of Attacks: | 4 bludgeon          |
+-----------------+---------------------+
| Damage:         | 1d10/1d10/1d10/1d10 |
+-----------------+---------------------+
| Movement:       | 30’                 |
+-----------------+---------------------+
| No. Appearing:  | 1                   |
+-----------------+---------------------+
| Save As:        | Magic-user: 10      |
+-----------------+---------------------+
| Morale:         | 12                  |
+-----------------+---------------------+
| Treasure Type:  | none                |
+-----------------+---------------------+
| XP:             | 1,390               |
+-----------------+---------------------+

A **Mountain Fairy** is a manifestation of the conscious of a mountain, appearing with stony skin, craggy outcroppings, and a slate gray motif as its attributes.

A mountain fairy attacks using its large fists to pummel its foes into submission. Its stony skin gives it great protection, functioning as Hardness 8 (reducing all incoming damage by 8).
