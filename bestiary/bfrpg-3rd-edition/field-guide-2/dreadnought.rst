.. title: Dreadnought
.. slug: dreadnought
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-10
.. description: Dreadnought
.. type: text
.. image: png

+-----------------+----------------------+
| Armor Class:    | 24                   |
+-----------------+----------------------+
| Hit Dice:       | 10*                  |
+-----------------+----------------------+
| No. of Attacks: | 1 lance or 2 cannons |
+-----------------+----------------------+
| Damage:         | 2d12 or 3d6/3d6      |
+-----------------+----------------------+
| Movement:       | 30'                  |
+-----------------+----------------------+
| No. Appearing:  | 1                    |
+-----------------+----------------------+
| Save As:        | Fighter: 10          |
+-----------------+----------------------+
| Morale:         | 11                   |
+-----------------+----------------------+
| Treasure Type:  | Special              |
+-----------------+----------------------+
| XP:             | 1,390                |
+-----------------+----------------------+

A **Dreadnought** is a 20 foot tall metal construct in the shape of a giant knight, holding a huge lance and with a pair of long, cylindrical cannons mounted on each of its shoulders.

It attacks primarily from range; its cannons have an effective range of 300’. The cannons possess extreme destructive power, capable of causing severe deformation of the surroundings of its targets. If, however, one gets close it will attack with its lance, charging up to twice its movement for the first attack.

As a construct it possesses no mind of its own and therefore is not subject to **charm**, **sleep**, or similar effects.
