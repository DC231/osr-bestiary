.. title: Geminate Serpent
.. slug: geminate-serpent
.. date: 2019-11-08 13:50:52.588217
.. tags: 
.. description: Geminate Serpent
.. type: text
.. image:

A **Geminate Serpent** is a very long, double-headed snake. Its heads are similar to those of a dragon; the two are commonly mistaken if the whole creature is not seen. These creatures are of Elvish origin and live just as long if not longer. Similarly to many other creatures that live a long time, a geminate serpent can be categorized based on its age, ranging from 1 to 7.

A geminate serpent uses its long body to restrain its victims in combat; it can carry and crush one creature per 20' of its body length. Creatures crushed take subduing damage, and are taken to its lair unconscious for the young to meet. Creatures that are caught have their armor, weapons, and shiny or dangerous-looking items removed, such as books from robed fellows. These items are then stored in the serpent’s treasure pile. Creatures caught are kept in a hole that is as deep as half the parents’ length. Once every two days a creature is removed to fight the child by itself under the supervision of the parent. Those who are winning will be dropped back into the hole with the others, or eaten if none have been eaten or killed by the child within 3 days.

Geminate serpents live to breed and will always have a child in its lair. An adult female of this species will take care of its young until they are in their second or third stage of age categorization. During this time the parents will take turns hunting for food, bringing back unconscious creatures if possible. If there is only one parent the child will accompany them on hunts for non-intelligent creatures. If a geminate serpent is with its young it will have a morale of 12 and will never leave its side, even if it means its own death.

Each geminate serpent may use its breath weapon as many times a day as its hit dice. It may however only use its breath weapon every other round. It can use a non-empowered version (basically just fog) for use as cover as often as it likes. Breath from geminate serpents has unusual effects, and will affect those with hit dice less than or equal to its own hit dice unless otherwise stated, or a save vs. Dragon Breath is made; creatures with at least five hit dice less than the hit dice of the Serpent do not get a save. The breaths will stay in the area until the number of turns equal to the age category of the serpent has passed. The breath weapons are long lines emitting from the mouth of the serpent with the specified lengths and widths. Each geminate serpent is immune to its own breath weapon, as well as all spells or poisons that replicate that particular effect.

All geminate serpents speak their own language, Serpentine; in addition, one has a chance of knowing Elvish dependent on its age category, as specified in the tables below for each type.
