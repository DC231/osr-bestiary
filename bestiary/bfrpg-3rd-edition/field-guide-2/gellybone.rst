.. title: Gellybone
.. slug: gellybone
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-2
.. description: Gellybone
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 14 (see below) |
+-----------------+----------------+
| Hit Dice:       | 2              |
+-----------------+----------------+
| No. of Attacks: | 1 claw         |
+-----------------+----------------+
| Damage:         | 1d6            |
+-----------------+----------------+
| Movement:       | 30'            |
+-----------------+----------------+
| No. Appearing:  | 2d4+3          |
+-----------------+----------------+
| Save As:        | Fighter: 2     |
+-----------------+----------------+
| Morale:         | 12             |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 75             |
+-----------------+----------------+

**Gellybones** are skeletons that have passed through a special magical procedure that converted their bones into a gelatin-like structure. They take only half damage from blunt weapons, and only a single point from arrows, bolts, or sling stones (plus any magical bonus). As with all undead, they can be Turned by a Cleric, and are immune to **sleep**, **charm**, or **hold** magic. As they are mindless, no form of mind reading is of any use against them.
