.. title: Dracomander, Lightning*
.. slug: dracomander-lightning
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-9
.. description: Dracomander, Lightning*
.. type: text
.. image:

+-----------------+------------------------------------------------+
| Armor Class:    | 20 ‡                                           |
+-----------------+------------------------------------------------+
| Hit Dice:       | 9**                                            |
+-----------------+------------------------------------------------+
| No. of Attacks: | 2 bites or breath/1 tail + lightning           |
+-----------------+------------------------------------------------+
| Damage:         | 2d8 or breath/2d8 or breath/1d6+ 1d8 per round |
+-----------------+------------------------------------------------+
| Movement:       | 30' Fly 80' (15')                              |
+-----------------+------------------------------------------------+
| No. Appearing:  | 1, Wild 1, Lair 1d4                            |
+-----------------+------------------------------------------------+
| Save As:        | Fighter: 9 (as Hit Dice)                       |
+-----------------+------------------------------------------------+
| Morale:         | 8                                              |
+-----------------+------------------------------------------------+
| Treasure Type:  | H                                              |
+-----------------+------------------------------------------------+
| XP:             | 1,225                                          |
+-----------------+------------------------------------------------+

A **Lightning Dracomander** resembles a giant snake more than 12' long with two dragon-like heads (with long serpentine necks) and a pair of dragon wings. Its scales are all the colors of lightning: white, blue, purple, and yellow. A lightning dracomander constantly emits little bolts of lightning; all non-lightning-resistant creatures within 20' of the monster suffer 1d8 points of damage per round from being struck by them. Lightning dracomanders are immune to damage from any type of electrical or lightning attack.

A lightning dracomander may project its breath weapon from either mouth, or even from both, but cannot use the breath weapon more times than it has hit dice in any day (in other words, an adult lightning dracomander cannot use the breath weapon 9 times per day from each mouth, but rather 9 times per day *total*). Anyone in the area of effect of both breath weapons in the same round suffers damage only from one of them (generally the one with the higher damage total).

As with the lightning salamander, this monster has only one mind despite having two heads.

**Lightning Dracomander Age Table**

+-----------------+-----+-----+-----+-----+-----+-----+------+
| Age Category    | 1   | 2   | 3   | 4   | 5   | 6   | 7    |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Hit Dice        | 6   | 7   | 8   | 9   | 10  | 11  | 12   |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Attack Bonus    | +6  | +7  | +8  | +8  | +9  | +9  | +10  |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Breath Weapon   | Lightning (Line)                         |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Length          | -   | 45' | 55' | 65' | 75' | 85' | 95'  |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Width           | -   | 15' | 20' | 25' | 30' | 35' | 40'  |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Chance/Talking  | 0%  | 30% | 35% | 40% | 50% | 65% | 75%  |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Spells by Level |     |     |     |     |     |     |      |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Level 1         | -   | 1   | 2   | 4   | 4   | 4   | 4    |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Level 2         | -   | -   | 1   | 2   | 2   | 2   | 3    |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Level 3         | -   | -   | -   | -   | 1   | 2   | 3    |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Bite            | 2d4 | 2d6 | 2d6 | 2d8 | 2d8 | 2d8 | 2d10 |
+-----------------+-----+-----+-----+-----+-----+-----+------+
| Tail            | 1d4 | 1d4 | 1d4 | 1d6 | 1d6 | 1d8 | 1d8  |
+-----------------+-----+-----+-----+-----+-----+-----+------+
