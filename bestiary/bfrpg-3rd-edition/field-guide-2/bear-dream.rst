.. title: Bear, Dream
.. slug: bear-dream
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-8
.. description: Bear, Dream
.. type: text
.. image: png

+-----------------+-------------------+
| Armor Class:    | 17                |
+-----------------+-------------------+
| Hit Dice:       | 8*                |
+-----------------+-------------------+
| No. of Attacks: | 2 claw+hug/1 bite |
+-----------------+-------------------+
| Damage:         | 1d4/1d4 + 2d8/1d8 |
+-----------------+-------------------+
| Movement:       | 40'               |
+-----------------+-------------------+
| No. Appearing:  | 1d4, Lair 2d8     |
+-----------------+-------------------+
| Save As:        | Fighter: 8        |
+-----------------+-------------------+
| Morale:         | 9                 |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 940               |
+-----------------+-------------------+

A **Dream Bear** appears like a black bear, but is phantasmal in nature, hazy and partially transparent with misty currents visible within it.

It attacks by biting and clawing its foes with its front paws; if it succeeds both claw attacks against the same target it performs a hug attack.

Anyone bitten by a dream bear must save vs. Spells or suffer from a curse known only as **“bear in mind.”** This curse causes the victim to have vivid, realistic dreams of living as a bear, and strongly compels the afflicted person to behave like a bear. The victim may save vs. Magic Wands to resist these impulses for a day. The curse may be removed with a casting of **remove curse**. After a month of this curse the victim must save vs. Spells or turn into a dream bear. This check should be performed every month until the curse is removed. This change, if it happens, can only be reversed by a **wish**.

Dream bears live in small communities of less than a dozen deep in the most ancient forests of the world. They are fiercely territorial and will fiercely attack interlopers.
