.. title: Phoenix*
.. slug: phoenix
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-9
.. description: Phoenix*
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 17‡            |
+-----------------+----------------+
| Hit Dice:       | 9**            |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 1d6/1d6/2d6    |
+-----------------+----------------+
| Movement:       | 10' Fly 90'    |
+-----------------+----------------+
| No. Appearing:  | 1              |
+-----------------+----------------+
| Save As:        | Fighter: 9     |
+-----------------+----------------+
| Morale:         | 10             |
+-----------------+----------------+
| Treasure Type:  | D              |
+-----------------+----------------+
| XP:             | 1,225          |
+-----------------+----------------+

A **Phoenix** is a magnificent scarlet-plumed bird with a wingspan of over 25’. A phoenix's body is about 10’ long from beak to tail apart from a few long reeves like those of a pheasant. It weighs just under a ton. A phoenix is intelligent and can telepathically communicate. It can only be harmed by magical weapons, and is immune to any charm effects, **hold monster** and similar spells, as well as all fire damage. A phoenix is a powerful ally to the forces of good, and it is sometimes said that it is a spirit of rebirth and renewal.

When it is threatened, a phoenix cloaks itself in an aura of fire that deals 3d6 points of damage to any creature within 10’ of it. If the phoenix dies, its body vanishes in a burst of flame that functions as a **fireball** spell, dealing 9d6 points of damage, half of which is divine spiritual radiance that cannot be resisted by any means. When these death throes are concluded a single egg can be found amidst the ashes from which a new phoenix will hatch in no more than a day. No power short of a **wish** can prevent the phoenix's rebirth. A phoenix will occasionally grant a tail feather as a gift to those who aid it. This feather can be brewed into a potion with the combined effect of the **raise dead** and **heal** spells.
