.. title: Shaddick's Demonic Automata
.. slug: shaddick-s-demonic-automata
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-12
.. description: Shaddick's Demonic Automata
.. type: text
.. image:

+-----------------+-------------------+
| Armor Class:    | 20                |
+-----------------+-------------------+
| Hit Dice:       | 12**              |
+-----------------+-------------------+
| No. of Attacks: | 6 blades or spell |
+-----------------+-------------------+
| Damage:         | 2d12 per blade    |
+-----------------+-------------------+
| Movement:       | 30'               |
+-----------------+-------------------+
| No. Appearing:  | 1                 |
+-----------------+-------------------+
| Save As:        | Fighter: 12       |
+-----------------+-------------------+
| Morale:         | 12                |
+-----------------+-------------------+
| Treasure Type:  | A                 |
+-----------------+-------------------+
| XP:             | 2,075             |
+-----------------+-------------------+

**Demonic Automata** were created by the mad wizard Shaddick in an attempt to create artificial life separate from the gods. A demonic automaton is an entity of nightmares, a towering construct made of burnished metal in the shape of a man. It will repulse any who behold it to such an extent that they must make a save vs. Magic Wands or start madly babbling until calmed by a casting of **remove fear**. Once this save has been passed they are immune to this effect.

It has numerous blades sprouting from its back on long spindle-like arms. These are used to slice at anything that gets near the automata. It hates all life and it will fight until destroyed.

Three times per day it can call upon the power of its creator, giving it the ability to cast a Magic-User spell of up to third level.
