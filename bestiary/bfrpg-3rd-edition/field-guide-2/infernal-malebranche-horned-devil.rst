.. title: Infernal, Malebranche (Horned Devil)*
.. slug: infernal-malebranche-horned-devil
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-5
.. description: Infernal, Malebranche (Horned Devil)*
.. type: text
.. image: png

+-----------------+--------------------------+
| Armor Class:    | 24‡                      |
+-----------------+--------------------------+
| Hit Dice:       | 5+5**                    |
+-----------------+--------------------------+
| No. of Attacks: | 1 tail/1 weapon or spell |
+-----------------+--------------------------+
| Damage:         | 1d4/by weapon            |
+-----------------+--------------------------+
| Movement:       | 30' Fly 20’              |
+-----------------+--------------------------+
| No. Appearing:  | 1d6                      |
+-----------------+--------------------------+
| Save As:        | Fighter: 6               |
+-----------------+--------------------------+
| Morale:         | 9                        |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 450                      |
+-----------------+--------------------------+

A **Malebranche** is a devil far larger than any man. Its skin is crimson or dark gray and is extremely thick and sturdy. It has a large set of horns on its head owing to its name. The horn tends to be used to rank hierarchy and not as an offensive weapon. A malebranche also has large bat-like wings which it can use to fly short or medium distances.

Most malebranche will be armed with a two-tined fork that causes 2d6 damage, although a minority of them will be armed with a barbed whip instead. The barbed whip only deals 1d4 damage but will also stun an opponent for the same amount of rounds (unless a save vs. Magic Wands is made). A stunned opponent cannot attack but can still defend him- or herself (no loss to AC). With either one of these weapons the malebranche will still make use of its very sharp tail. It deals 1d4 damage but also causes the struck opponent to lose 1 HP every following round unless a healing spell or potion is used.

As the malebranche is from another plane it can only be hurt by magical weapons (+1 or better). It can also cast **wall of fire** and **cause fear** once per day.
