.. title: Octobat
.. slug: octobat
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-8
.. description: Octobat
.. type: text
.. image: png

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 8*                      |
+-----------------+-------------------------+
| No. of Attacks: | 4 tentacles or acid ink |
+-----------------+-------------------------+
| Damage:         | 1d6/1d6/1d6/1d6 or 3d6  |
+-----------------+-------------------------+
| Movement:       | 60'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d6                     |
+-----------------+-------------------------+
| Save As:        | Fighter: 8              |
+-----------------+-------------------------+
| Morale:         | 12                      |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 945                     |
+-----------------+-------------------------+

An **Octobat** is an intelligent subterranean creature that will often work for powerful evil individuals such as a wizard or warlord. This dangerous creature tends to bludgeon victims with its four tentacles. For fear of losing a tentacle, an octobat will not entangle larger or seemingly strong opponents, but against smaller or less powerful prey it will wrap itself about its target if two or more tentacle attacks are successful. The entangled individual is crushed and strangled, receiving 2d6 damage automatically each round (instead of regular tentacle attacks). The entangled individual must successfully roll an open doors attempt to break free, but may be entangled again in subsequent rounds.

The octobat can squirt an acidic ink at a single opponent up to 20’ away, causing 3d6 damage (save vs. Dragon Breath for half-damage). The octobat can also employ this attack against an entangled opponent. This attack is usable 4 times per day and not more than once every 1d4 rounds. This acidic ink can dissolve a web (such as the spell) in one full round. The octobat is immune to this acid ink (whether its own or from other octobats).
