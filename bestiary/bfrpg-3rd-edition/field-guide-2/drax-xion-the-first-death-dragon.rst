.. title: Drax'xion, The First Death Dragon
.. slug: drax-xion-the-first-death-dragon
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-19
.. description: Drax'xion, The First Death Dragon
.. type: text
.. image: jpg

+-----------------+---------------------------------------------+
| Armor Class:    | 28                                          |
+-----------------+---------------------------------------------+
| Hit Dice:       | 19** (AB +12)                               |
+-----------------+---------------------------------------------+
| No. of Attacks: | 2 claws + paralysis/1 bite or breath/1 tail |
+-----------------+---------------------------------------------+
| Damage:         | 2d10/2d10/6d8 or 3d10                       |
+-----------------+---------------------------------------------+
| Movement:       | 30' Fly 80'                                 |
+-----------------+---------------------------------------------+
| No. Appearing:  | 1                                           |
+-----------------+---------------------------------------------+
| Save As:        | Magic-User: 19                              |
+-----------------+---------------------------------------------+
| Morale:         | 12                                          |
+-----------------+---------------------------------------------+
| Treasure Type:  | H,A,G (x2)                                  |
+-----------------+---------------------------------------------+
| XP:             | 4,850                                       |
+-----------------+---------------------------------------------+

**Drax'xion** is a fabled undead dread dragon. A former consort to an evil goddess, he was cast out to die, but achieved undeath by another mysterious deity. He lies in wait in a state of lichdom, hidden away inside his cursed lair for the time to avenge his death and rebirth. His influence is small, almost to a point of few knowing of him let alone his true terrifying might.

Drax'xion has three breath weapons and various spell-like abilities. He has a flame breath (cone-shaped) 110' long and 60' wide and a death cloud of the same size, dealing 3d10 damage. Anyone hit by the cloud must make a save vs. Death Ray or contract mummy rot, a disease that prevents normal or magical healing. A **cure disease** spell must be cast on the victim before he or she may regain hit points.

His third breath weapon is the dreaded death wind, which is 60' long and 30' wide. Anyone hit must save vs. Dragon Breath or die. Drax'xion only uses this when he feels he has no choice, as he can only summon the negative energy to do it once every few months. Any destroyed undead in the cloud’s area-of-effect will arise to serve him as though they had never taken damage.

Edged weapons deal only half damage to Drax'xion and Clerics cannot Turn the undead dragon.

Drax'xion can summon 3 haunted bones (see the **Basic Fantasy Field Guide volume 1**) each turn. There can be no more than 6 haunted bones summoned at any one time.

Drax'xion can cast **death**, **disintegrate**, and **ice storm** twice per day. He can cast **regenerate** and **phantasmal force** three times per day.

In addition to his treasure hoard, Drax'xion holds within his lair 50,000 gp worth of rare gems, 10,000 gp worth of ancient paintings and portraits, 5,000 gp in rare books and tomes, a spellbook with 12,000 gp worth of spells inscribed in it, a **+5 Shortsword of Ice**, a +**4 Mace of Fire**, a **Dagger of Wishes**, and a **Ring of Invisibility**.

Merely destroying Drax'xion's body will not kill him; an adventurer needs to find his phylactery and destroy it, else he will rise again. It appears as an ornate crystal jar wrapped in dragon's teeth, hidden in his hoard. Only very powerful magic (**disintegrate** or a **wish**) or catastrophic natural damage is able to destroy it.

If an adventurer were to take the phylactery, Drax'xion will hunt him or her down to retrieve it. He is able to locate the phylactery from anywhere, regardless of distance.
