.. title: Snake, Rainbow
.. slug: snake-rainbow
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-4
.. description: Snake, Rainbow
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 14                 |
+-----------------+--------------------+
| Hit Dice:       | 4*                 |
+-----------------+--------------------+
| No. of Attacks: | 1                  |
+-----------------+--------------------+
| Damage:         | 1d6 or special     |
+-----------------+--------------------+
| Movement:       | 30'                |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d4, Lair 1d6 |
+-----------------+--------------------+
| Save As:        | Fighter: 4         |
+-----------------+--------------------+
| Morale:         | 8                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 280                |
+-----------------+--------------------+

A **Rainbow Snake** is about 6' long and striped in all colors. This snake is said to have been created by a **sticks to snakes** spell cast by a gnome. The poison has the same effect as a **confusion** spell cast for 1d4 rounds. A rainbow snake can instead spray a 15' cone of multi-colored confetti that has the following effect:

+----------+----------------------------------------------------+
| **1d10** | **Effect**                                         |
+----------+----------------------------------------------------+
| 1-3      | Stuns the creature for 1d6 rounds                  |
+----------+----------------------------------------------------+
| 4-6      | Renders the creature unconscious for 1d6 rounds    |
+----------+----------------------------------------------------+
| 7-9      | Blinds the creature for 1d6 rounds                 |
+----------+----------------------------------------------------+
| 10       | Roll twice and apply both; a roll of 10 is ignored |
+----------+----------------------------------------------------+

A save vs. Poison is allowed to half the duration of the confetti or **confusion** poison, and blind creatures are not affected by the confetti.
