.. title: Fairy, Ocean*
.. slug: fairy-ocean
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-10
.. description: Fairy, Ocean*
.. type: text
.. image: png

+-----------------+------------------------+
| Armor Class:    | 18‡                    |
+-----------------+------------------------+
| Hit Dice:       | 10*                    |
+-----------------+------------------------+
| No. of Attacks: | 4 water jets or flood  |
+-----------------+------------------------+
| Damage:         | 1d8/1d8/1d8/1d8 or 4d6 |
+-----------------+------------------------+
| Movement:       | 30’                    |
+-----------------+------------------------+
| No. Appearing:  | 1                      |
+-----------------+------------------------+
| Save As:        | Magic-user: 10         |
+-----------------+------------------------+
| Morale:         | 12                     |
+-----------------+------------------------+
| Treasure Type:  | none                   |
+-----------------+------------------------+
| XP:             | 1,390                  |
+-----------------+------------------------+

An **Ocean Fairy** is a manifestation of the consciousness of an ocean, appearing with vibrantly-colored shells, foaming waves hinted at by mirages, and an aquamarine motif as its attributes.

An ocean fairy can produce massive amounts of water, an ability it uses to flood ships and drown its foes. Furthermore, it can use this ability to shoot high-pressure jets of water at its foes.
