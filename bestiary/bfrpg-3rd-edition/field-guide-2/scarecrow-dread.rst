.. title: Scarecrow, Dread
.. slug: scarecrow-dread
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-4
.. description: Scarecrow, Dread
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 14                 |
+-----------------+--------------------+
| Hit Dice:       | 4 + 2** (+5)       |
+-----------------+--------------------+
| No. of Attacks: | 1 slam or 1 weapon |
+-----------------+--------------------+
| Damage:         | 1d6+1 or by weapon |
+-----------------+--------------------+
| Movement:       | 40'                |
+-----------------+--------------------+
| No. Appearing:  | 1                  |
+-----------------+--------------------+
| Save As:        | Fighter: 4         |
+-----------------+--------------------+
| Morale:         | 12 (8)             |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 320                |
+-----------------+--------------------+

Sometimes during its creation a living scarecrow may become possessed by an evil spirit, becoming a **Dread Scarecrow**. These malevolent creatures take delight in seeking out and tormenting the living by inducing terror. During the day it hides in the fields as a normal scarecrow, but during the night it stalks, looking for unwary prey. In all respects it looks like a normal scarecrow except for the deep red-orange glow from within its pumpkin head and frightening visage.

A dread scarecrow is immune to fear (morale of 12), but has a self-preservation instinct. Use the morale of 8 to determine whether a dread scarecrow decides to flee from combat if it is outmatched or outnumbered.

Because of its evil spirit, a dread scarecrow has several powers. It has Darkvision out to 60'. It also has a gaze attack that it chooses to either **cause fear** or **bestow curse** against a single target out to 30' who fails a savings throw vs. Spells. It also has a maniacal cackle that works likes the **bane** spell on anyone within 50' who fails a saving throw vs. Spells. Those who make their save are immune to the effect for 24 hours.

A dread scarecrow is also very nimble and quick, so it does not receive the initiative penalty like other living scarecrows, but is still weak against fire, suffering 1 extra point per die of damage and has a -2 penalty to save against fire-based attacks. A dread scarecrow has a magic resistance of 25%, but since it possesses an intelligence it is subject to mind-affecting magic and effects.
