.. title: Fyrenewt*
.. slug: fyrenewt
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-2
.. description: Fyrenewt*
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 16 (12)‡                        |
+-----------------+---------------------------------+
| Hit Dice:       | 2+2*                            |
+-----------------+---------------------------------+
| No. of Attacks: | 1 weapon or breath              |
+-----------------+---------------------------------+
| Damage:         | By weapon or 2d4                |
+-----------------+---------------------------------+
| Movement:       | 30'                             |
+-----------------+---------------------------------+
| No. Appearing:  | Wild 3d8, Lair 10d10            |
+-----------------+---------------------------------+
| Save As:        | Fighter: 2                      |
+-----------------+---------------------------------+
| Morale:         | 8                               |
+-----------------+---------------------------------+
| Treasure Type:  | Q, S, U individually; B in lair |
+-----------------+---------------------------------+
| XP:             | 100                             |
+-----------------+---------------------------------+

Usually found in arid regions of volcanic activity, a **Fyrenewt** is perhaps distantly related to wugs (see the **Basic Fantasy Field Guide** **volume** **1**) or lizard men (see the **Basic Fantasy RPG Core Rules**). A fyrenewt has the general appearance of a man-sized humanoid amphibian of the salamander sort. Rather than slick or slimy, a fyrenewt's skin is rather rough and sandy in texture and has hues of crimson-tinted browns. Fyrenewts speak their own language.

The typical fyrenewt warrior wears chainmail armor and wields a metal weapon such as a sword or metal-shafted lance. In addition to a weapon attack, the fyrenewt can breathe forth a spray (cone-shaped) of fluid that instantly ignites upon contact with air. This breath weapon is usable no more than once every ten rounds, and causes 2d4 damage (or half if a save vs. Dragon Breath succeeds) to any creatures within 10’ in front of the fyrenewt.

A fyrenewt is immune to non-magical fire attacks, and even against magical fire it takes only half damage, and saves are made at +4. Conversely, cold-based attacks against a fyrenewt causes double damage and the fyrenewt has an additional -2 penalty on any appropriate saves.

Fyrenewts have stronger leaders that wear plate mail (AC 18) and have 4+4 hit dice (280 XP). This leader's breath weapon causes 4d4 damage (1d4 per HD). There may be even stronger chieftains or kings. About 1d4 fyrenewts may also be a priest with Clerical levels equivalent to one's HD. They prefer spells involving heat, flame, or fire. If the GM utilizes the optional druid supplement, then the fyrenewt may choose fire-oriented spells from the druid spell list. Even traditional spells are often cast in such a way as to include fire, smoke, heat or the like in ways that are cosmetic only. For instance, a **protection from evil** spell might appear to line the fyrenewt in a flame-like aura.
