.. title: Deep One, Common
.. slug: deep-one-common
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-3
.. description: Deep One, Common
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 16                   |
+-----------------+----------------------+
| Hit Dice:       | 3+3                  |
+-----------------+----------------------+
| No. of Attacks: | 2 claws or by weapon |
+-----------------+----------------------+
| Damage:         | 1d4/1d4 or by weapon |
+-----------------+----------------------+
| Movement:       | 20' Swim 30'         |
+-----------------+----------------------+
| No. Appearing:  | 1d8, Lair 5d8        |
+-----------------+----------------------+
| Save As:        | Fighter: 3           |
+-----------------+----------------------+
| Morale:         | 8                    |
+-----------------+----------------------+
| Treasure Type:  | A                    |
+-----------------+----------------------+
| XP:             | 145                  |
+-----------------+----------------------+

A **Common Deep One** is a scaled humanoid resembling both frog and fish. Its huge unblinking eyes give it Darkvision to 60' and superior eyesight while underwater. Its clawed hands and feet are webbed, enabling exceptional swimming ability. As inhuman as a deep one looks, it can produce offspring from unions with various humanoids that look completely normal (for the humanoids).

Communities of deep ones are found far down in bodies of water, but generally within proximity of coastal humanoid settlements. In such a community one will find more powerful deep ones with additional powers.
