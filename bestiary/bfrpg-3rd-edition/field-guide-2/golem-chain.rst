.. title: Golem, Chain*
.. slug: golem-chain
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-20
.. description: Golem, Chain*
.. type: text
.. image: jpg

+-----------------+-----------------------+
| Armor Class:    | 24‡                   |
+-----------------+-----------------------+
| Hit Dice:       | 20** (+13)            |
+-----------------+-----------------------+
| No. of Attacks: | 3 lashes + special    |
+-----------------+-----------------------+
| Damage:         | 4d6/4d6/4d6 + special |
+-----------------+-----------------------+
| Movement:       | 50'                   |
+-----------------+-----------------------+
| No. Appearing:  | 1                     |
+-----------------+-----------------------+
| Save As:        | Fighter: 20** (+13)   |
+-----------------+-----------------------+
| Morale:         | 12                    |
+-----------------+-----------------------+
| Treasure Type:  | H                     |
+-----------------+-----------------------+
| XP:             | 5,650                 |
+-----------------+-----------------------+

The **Chain Golem** is most frequently found as a guardian of libraries, palaces, and treasure hoards. Perfectly resembling a large amount of regular chain made from various metals, it is often disguised as decoration, hung from the walls and ceiling or coiled in a corner. When disturbed, the chain golem will rise up from the floor and violently attack anything nearby that is not its master.

At its average height, the chain golem stands around 15' tall and rattles noisily as it moves, much like a snake, along the ground. The various chains can weigh up to around 3,000 pounds. Powered by the essence of a trapped air elemental, it attacks its targets with extreme prejudice, directing years of hatred at being trapped on the mortal plane towards any in its path. As a standard attack it lashes out with several long chains.

The chain golem can choose to direct all three of its attacks onto a single enemy. If all three hit, the target is swept up in its chains and can be subject to one action per round. These actions include:
