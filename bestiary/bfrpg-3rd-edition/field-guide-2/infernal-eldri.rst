.. title: Infernal, Eldri*
.. slug: infernal-eldri
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-9
.. description: Infernal, Eldri*
.. type: text
.. image: jpg

+-----------------+---------------------------------------+
| Armor Class:    | 16‡                                   |
+-----------------+---------------------------------------+
| Hit Dice:       | 9** (AB +7)                           |
+-----------------+---------------------------------------+
| No. of Attacks: | 2 claws or fire blast or spell        |
+-----------------+---------------------------------------+
| Damage:         | 1d4/1d4 or 2d6 or by spell            |
+-----------------+---------------------------------------+
| Movement:       | 50' Fly 90’                           |
+-----------------+---------------------------------------+
| No. Appearing:  | 1                                     |
+-----------------+---------------------------------------+
| Save As:        | Magic-User: 14                        |
+-----------------+---------------------------------------+
| Morale:         | 9                                     |
+-----------------+---------------------------------------+
| Treasure Type:  | G, O + 1d4+1 non-weapon magical items |
+-----------------+---------------------------------------+
| XP:             | 1,225                                 |
+-----------------+---------------------------------------+

The **Eldri** are a breed of infernals that focus their attentions on the accumulation of dark magical secrets and evil lore. They are incredibly skilled in the use of magic and rival vegas (see **Basic Fantasy Field Guide** **volume 1**) in power. Both male and female varieties of eldri exist, and regardless of superficial gender they are universally beautiful. Their blood-red skin, bright orange-red hair, and single smooth horn that grows from their forehead gives them a devilish appearance.

An eldri has the ability to create blasts of flame from its hands up to 120’. Its nails are as hard as iron and as sharp as daggers, and will use them only if caught in melee. In addition, an eldri has the ability to fly. It has the ability to cast spells as a 15th-level Magic-User. It prefers spells that deal direct damage over subtler magics. An eldri spends its life in search of new magic and always has a small stock of magical items it has discovered over the course of its existence as reflected in the treasure type above.

An eldri is immune to lightning and poison, and takes only half damage from acid, cold, or fire-based attacks. In addition, an eldri is only affected by magical weapons and spells of 2nd-level or higher. An eldri that is killed in combat will dissolve to fine ash, leaving only its horn behind. If this horn is not destroyed within 1 year, the eldri will form a new body with all the memories it had up to the time of its death. The exact means to destroy an eldri's horn is up to individual GMs to devise.
