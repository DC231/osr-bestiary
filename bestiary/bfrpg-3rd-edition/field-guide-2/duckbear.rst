.. title: Duckbear
.. slug: duckbear
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-6
.. description: Duckbear
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 15                     |
+-----------------+------------------------+
| Hit Dice:       | 6                      |
+-----------------+------------------------+
| No. of Attacks: | 2 claws/1 peck + 1 hug |
+-----------------+------------------------+
| Damage:         | 1d8/1d8/1d10 + 3d6     |
+-----------------+------------------------+
| Movement:       | 40'                    |
+-----------------+------------------------+
| No. Appearing:  | Wild 1d4, Lair 1d6     |
+-----------------+------------------------+
| Save As:        | Fighter: 5             |
+-----------------+------------------------+
| Morale:         | 10                     |
+-----------------+------------------------+
| Treasure Type:  | C                      |
+-----------------+------------------------+
| XP:             | 400                    |
+-----------------+------------------------+

Contrary to its name, a **Duckbear** is not a bear with features of a duck. Rather, a duckbear has the body of a bear with the head of a pigeon. Its whole body is covered with dull gray feathers with a band of shiny colorful feathers around its neck. Both male and female duckbears stand around 9’ tall and weigh up to 1,600 pounds. A duckbear, much like its pigeon counterpart, is fairly stupid and only attacks an intruder encroaching on its lair or if attacked first. If a group of duckbears outnumber their opponents their morale is boosted to 12.
