.. title: Infernal, Arachnea*
.. slug: infernal-arachnea
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-6
.. description: Infernal, Arachnea*
.. type: text
.. image:

+-----------------+-------------------------------------------------+
| Armor Class:    | 16 (11)†                                        |
+-----------------+-------------------------------------------------+
| Hit Dice:       | 6*                                              |
+-----------------+-------------------------------------------------+
| No. of Attacks: | 1 bite or kiss or dagger                        |
+-----------------+-------------------------------------------------+
| Damage:         | 1d6 + special or 1d6 + special or 1d4 + special |
+-----------------+-------------------------------------------------+
| Movement:       | 60'                                             |
+-----------------+-------------------------------------------------+
| No. Appearing:  | 1                                               |
+-----------------+-------------------------------------------------+
| Save As:        | Magic-User: 6                                   |
+-----------------+-------------------------------------------------+
| Morale:         | 8                                               |
+-----------------+-------------------------------------------------+
| Treasure Type:  | I                                               |
+-----------------+-------------------------------------------------+
| XP:             | 555                                             |
+-----------------+-------------------------------------------------+

**Arachnea** appears as a strikingly beautiful nude or elegantly-dressed young woman, or as a giant black widow spider. She can cast spells in either form and is highly intelligent. She will attempt to seduce or cast **charm person** on her target to lure in for a kiss (woman form) or bite (spider form). She takes one round to change forms. She is highly magical in nature and can only be hit by magical or silvered weapons. She may utilize the following spell-like abilities at-will in either form: **charm person**, **teleport**, **hold person**, **web**, **suggestion**, **darkness 15' radius**, **ESP**, and **clairaudience** (as the potion). She can also summon 1d6 giant poisonous spiders to her aid.

When in spider form, she has AC 16 and attacks with a bite, with a poison that does 1d6+3 damage per round for 6 rounds (save vs. Poison to avoid).

When in her beautiful human form she has AC 11. She generally avoids combat, instead preferring to use **charm** and **suggestion** to manipulate others to do her bidding. Her kiss will drain 1d6 HP per round from a charmed or willing individual. The hit points drained from a kiss will heal her an equal amount and can even give her additional temporary hit points over and above her normal maximum (lasts up to 1 hour after draining). If it is impossible to avoid a fight, she may use a dagger that is poisoned with her own venom. The venom is weakened when used this way and only causes 2 points of damage for 3 rounds; a save vs. Poison will halt such damage, though subsequent hits will require a new save.

If she loses more than half of her HP she will try to cast **teleport** on her next round and escape.
