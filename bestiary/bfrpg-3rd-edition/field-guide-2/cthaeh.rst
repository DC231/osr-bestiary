.. title: Cthaeh*
.. slug: cthaeh
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-12
.. description: Cthaeh*
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 11‡                           |
+-----------------+-------------------------------+
| Hit Dice:       | 12**                          |
+-----------------+-------------------------------+
| No. of Attacks: | 1 swarm of mirror butterflies |
+-----------------+-------------------------------+
| Damage:         | 3d6 + blind                   |
+-----------------+-------------------------------+
| Movement:       | None                          |
+-----------------+-------------------------------+
| No. Appearing:  | 1                             |
+-----------------+-------------------------------+
| Save As:        | Magic-User: 12                |
+-----------------+-------------------------------+
| Morale:         | 6                             |
+-----------------+-------------------------------+
| Treasure Type:  | Fruit (worth 2d8x1,000 gp)    |
+-----------------+-------------------------------+
| XP:             | 2,175                         |
+-----------------+-------------------------------+

The **Cthaeh** is a creature of the fey, and an extremely dangerous one at that. It manifests itself as an ancient tree that somewhat resembles an ancient gnarled oak, branches bristling with metallic brass-like leaves that are hard-edged and razor sharp, multicolored, and shaped like long, thin knives. It prominently displays multitudes of strange and differing fruits and beautiful exotic flowers amongst the lowest of its leaves, both blooming on the tree at the same time. Butterflies with wings like radiant mirrors lazily circle the tree and subtly kill any birds, insects, or small animals that move too close to the cthaeh. The butterflies then proceed to feast on the carrion and rapidly strip them to the bones. While they may appear to be separate symbiotic organisms, do not be fooled; they are just as much a part of the cthaeh as its leaves or branches.

The cthaeh is intelligent as well as cunning and devious, but it is also naturally malevolent. The tree-like being entertains itself by tricking mortals into eating its fruit and seeking the forbidden knowledge the cthaeh freely distributes. These adventurous souls only far later fully realize the true cost of these gifts. The cthaeh speaks all languages and also has the ability to teach Magic-Users new and potent spells.

One of its flowers, which resembles the end of a pitcher plant, is a swirling mix of brilliant shades of violet and deepest blue. It secretes magical ink that the Cthaeh favor as a means of temptation. A second flower heals wounds, functioning as a **heal** spell when brewed into tea, while a third is a highly-potent panacea, functioning as a combination of **cure disease** and **neutralise poison** spells.

There is a fruit borne upon its branches that is shaped like the most perfect apple, deep red and ripe. It will cause near-instant death upon any contact with the skin, and if ingested the victim will rise from the grave a month later as a vampire. A different fruit bears seeds shaped like dragon teeth that when planted in the ground grow into wooden-armored skeletons (functioning the same as skeletons and obedient to the summoner’s commands).

Death and misfortune follow those who are blessed by the cthaeh. So long as one enjoys the benefits of the gifts, random encounters rolls of fives and sixes always re-roll, and there is a 20% chance that double the number of monsters rolled will appear. The saves of the "blessed" and those around them suffer a -2 penalty (also applied to monster reaction rolls), and Wisdom-based ability rolls suffer a -1 penalty. In addition their families tend to encounter tragedy far more often than most, and relationships will tend to end in violence and suffering. These detriments can be avoided if the recipients eschew the gifts of the cthaeh by removing them completely from their person.

The cthaeh is a powerful Magic-User itself, being able to use up to 6th-level spells. It favors **teleportation**, **disintegration**, **charm person**, and **ESP**. A cthaeh is also a master of illusion and can make itself appear as any natural object within a turn.
