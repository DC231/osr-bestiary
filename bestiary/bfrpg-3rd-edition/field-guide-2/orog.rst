.. title: Orog
.. slug: orog
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-3
.. description: Orog
.. type: text
.. image: jpg

+-----------------+---------------------------+
| Armor Class:    | 15 (11)                   |
+-----------------+---------------------------+
| Hit Dice:       | 3                         |
+-----------------+---------------------------+
| No. of Attacks: | 1 weapon                  |
+-----------------+---------------------------+
| Damage:         | 1d10+1 or by weapon +1    |
+-----------------+---------------------------+
| Movement:       | 20' Unarmored 40'         |
+-----------------+---------------------------+
| No. Appearing:  | 1d8, Wild 2d10, Lair 4d10 |
+-----------------+---------------------------+
| Save As:        | Fighter: 3                |
+-----------------+---------------------------+
| Morale:         | 9                         |
+-----------------+---------------------------+
| Treasure Type:  | S, T, U each: A in lair   |
+-----------------+---------------------------+
| XP:             | 145                       |
+-----------------+---------------------------+

**Orogs** are a fierce militaristic race of humanoids related to orcs. They are sometimes referred to as great orcs and are believed to be the descendants of orc/ogre hybrids. An orog resembles an orc but is larger and more muscular, standing between 6’ and 7’ tall and weighing 250 pounds on average. An orog is highly intelligent and often leads a group of orcs or serve orcish leaders as special bodyguards. They sometimes hire themselves out as mercenaries for other humanoids and giants.

An orog has Darkvision to 60'. It speaks the language of orcs as well as Common and possibly Goblin, Dwarven, or Elvish.

An orog sometimes marches into battle carrying a special standard representing their clan. While fighting under a standard all orogs and allied orcs gain a +1 bonus to attack rolls and morale.

The statistics given above is for a standard orog in chain mail armor and wielding a two-handed weapon. One out of every eight orogs will be a seasoned veteran of 5 HD (360 XP) with a +2 bonus to damage. Normal orogs led by a seasoned veteran gain a +1 bonus to their morale. In lairs of 16 or more orogs, there will be a chieftain of 7 HD (670 XP), AC 17 (11), and has a +3 bonus to damage. In their lair orogs never fail a morale check as long as the chieftain is alive.
