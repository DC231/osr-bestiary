.. title: Dynamo
.. slug: dynamo
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-2
.. description: Dynamo
.. type: text
.. image: jpg

+-----------------+-------------------+
| Armor Class:    | 15                |
+-----------------+-------------------+
| Hit Dice:       | 2*                |
+-----------------+-------------------+
| No. of Attacks: | 3 lightning bolts |
+-----------------+-------------------+
| Damage:         | 1d4/1d4/1d4       |
+-----------------+-------------------+
| Movement:       | Fly 40'           |
+-----------------+-------------------+
| No. Appearing:  | 2d6               |
+-----------------+-------------------+
| Save As:        | Fighter: 2        |
+-----------------+-------------------+
| Morale:         | 6                 |
+-----------------+-------------------+
| Treasure Type:  | None              |
+-----------------+-------------------+
| XP:             | 100               |
+-----------------+-------------------+

A **Dynamo** is a six inch metal sphere that floats a few feet off the ground and is enveloped by an aura of lightning. This lightning ranges from a pale blue to a deep purple. Anyone who touches the lightning with something conductive (such as a metal sword) must save vs. Spells or receive a -3 penalty on attack rolls due to numbness for 1 turn. This penalty is not cumulative.

A dynamo fires bolts of lightning at its foes. Despite appearances these spheres are quite cowardly, each making a morale check at half health.
