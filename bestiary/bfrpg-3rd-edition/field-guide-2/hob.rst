.. title: Hob
.. slug: hob
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-1
.. description: Hob
.. type: text
.. image: jpg

+-----------------+-------------------+
| Armor Class:    | 14 (11)           |
+-----------------+-------------------+
| Hit Dice:       | 1-1               |
+-----------------+-------------------+
| No. of Attacks: | 1 weapon          |
+-----------------+-------------------+
| Damage:         | 1d6 or by weapon  |
+-----------------+-------------------+
| Movement:       | 20' Unarmored 30' |
+-----------------+-------------------+
| No. Appearing:  | 1d6, Lair 5d10    |
+-----------------+-------------------+
| Save As:        | Thief: 1          |
+-----------------+-------------------+
| Morale:         | 7                 |
+-----------------+-------------------+
| Treasure Type:  | R                 |
+-----------------+-------------------+
| XP:             | 10                |
+-----------------+-------------------+

A **Hob** is a hairy relative of the goblin that typically stands around 3' to 3 1/2' tall. Each has a skin color that ranges from a yellowish through orange tones, to a red hue, with the occasional wart of a deeper, darker color. Eye colors range from gray to brown to black, with the red glow of a nocturnal animal in them when light reflects off of them. They may have hair coloring ranging from red to brown.

A hob often wears old ratty clothing but otherwise takes great pride in its appearance. However, if you give a hob a set of new clothes, it will go away forever. Likewise, a hob takes pride in its work and is often found in civilized homes helping with chores. The only payment it will accept is a bowl of porridge with a slab of butter on it. This pride also carries into the hob's personal life, though, and if it is offended, it will become a great nuisance until one makes amends with him.

Hobs have been known to serve in the night watch of cities and villages. In this capacity, the hob is as tenacious as his goblin cousins in defense of his home. A hob makes an excellent scout, and its skill with a short bow or crossbow matches that of any goblin.

Hobs are also very clannish among themselves, and such clans will often form a community within a city or caves or catacombs below such settlements. This clan lair is called a “hob hole”, and the hobs will choose a clan chief to run the operations of the clan.

1 in 6 hobs will be a warrior of 3-3 HD (145 XP). A hob gains a +1 bonus to its morale if led by a warrior. In its lair 1 in 20 will be a clan elder of 5-5 HD (350 XP) with AC 15 (11) and has a +1 bonus to damage due to strength. In a lair of over 30 hobs, there will be a clan chief of 7-7 HD (670 XP), with AC 16 (11) and a +1 bonus to damage. A hob has a +2 bonus to morale while its clan chief is present (this is not cumulative with the bonus given by a warrior leader). In addition, a lair has a chance equal to 1 on 1d6 of a Cleric being present (or 1-2 on 1d6 if a clan chief is present). A Cleric is equivalent to a regular hob statistically, but has Clerical abilities at level 1d4+1.
