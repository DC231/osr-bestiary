.. title: Golem, Gold
.. slug: golem-gold
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-13
.. description: Golem, Gold
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 16          |
+-----------------+-------------+
| Hit Dice:       | 13* (+10)   |
+-----------------+-------------+
| No. of Attacks: | 1 crook     |
+-----------------+-------------+
| Damage:         | 3d6         |
+-----------------+-------------+
| Movement:       | 40'         |
+-----------------+-------------+
| No. Appearing:  | 1           |
+-----------------+-------------+
| Save As:        | Fighter: 13 |
+-----------------+-------------+
| Morale:         | 12          |
+-----------------+-------------+
| Treasure Type:  | Special     |
+-----------------+-------------+
| XP:             | 2,285       |
+-----------------+-------------+

**Gold Golems** were originally made by a pharaoh as an ostentatious display of wealth. Although it is powerful in combat, its cost far exceeds its utility, therefore it is quite rare to encounter one. It is always womanly-shaped, wearing the semblance of costly robes (which are part of the golem, and thus made of animated gold). It fights with golden crooks. In addition to its attack, each gold golem can cast the spell **bestow curse** (usually applying the –4 penalty on attack rolls and saves variant) once every seven rounds. It can also **levitate** itself at-will.

A gold golem contains an amount of gold sufficient to mint 80,000 gp.
