.. title: Salamander, Lightning*
.. slug: salamander-lightning
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-10
.. description: Salamander, Lightning*
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 20‡                     |
+-----------------+-------------------------+
| Hit Dice:       | 10* (+9)                |
+-----------------+-------------------------+
| No. of Attacks: | 2 bites + lightning     |
+-----------------+-------------------------+
| Damage:         | 12d4/2d4 + 1d8/round    |
+-----------------+-------------------------+
| Movement:       | 40'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 2d4, Lair 2d4 |
+-----------------+-------------------------+
| Save As:        | Fighter: 10             |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | E                       |
+-----------------+-------------------------+
| XP:             | 1390                    |
+-----------------+-------------------------+

**Lightning Salamanders** come from the Elemental Plane of Air. A lightning salamander resembles a giant snake more than 12' long with two dragon-like heads (on short but flexible necks). Its scales are all the colors of lightning: white, blue, purple, and yellow. A lightning salamander constantly emits little bolts of lightning; all creatures within 20' of the salamander that are not lightning-resistant suffer 1d8 points of damage per round. A lightning salamander is immune to damage from any type of electrical or lightning attack. It is intelligent and can speak the language of the Plane of Air, and many will also know Elvish, Common, and/or Dragon.

Despite having two heads a lightning salamander has only one mind; either head may speak or both may, but it is very rare to meet a lightning salamander who can speak different words with each head at the same time (although those who can are known to sing duets with themselves, which may give away its location to those listening).

Flame, frost, and lightning salamanders hate each other, and each type will attack the others on sight, in preference to any other nearby foe.
