.. title: Serpentine Pseudodragon
.. slug: serpentine-pseudodragon
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-3
.. description: Serpentine Pseudodragon
.. type: text
.. image:

+-----------------+--------------+
| Armor Class:    | 19           |
+-----------------+--------------+
| Hit Dice:       | 3            |
+-----------------+--------------+
| No. of Attacks: | 1 bite       |
+-----------------+--------------+
| Damage:         | 1d6 + poison |
+-----------------+--------------+
| Movement:       | 10' Fly 50’  |
+-----------------+--------------+
| No. Appearing:  | 1d3          |
+-----------------+--------------+
| Save As:        | Fighter: 3   |
+-----------------+--------------+
| Morale:         | 10           |
+-----------------+--------------+
| Treasure Type:  | None         |
+-----------------+--------------+
| XP:             | 175          |
+-----------------+--------------+

The **Serpentine Pseudodragon** is a relatively small reptilian creature that possesses several prominent dragon-like features, including wings, horns, a pair of clawed forelegs, a crest of spikes traveling down its spine, and an unusual, near-human level of intelligence. The adult has but a single pair of legs. Its neck and torso can each stretch up to a foot long and the rest of its body tapers down into a 4' long tail that is often patterned and tapers to an end. It can communicate with nearby humanoids through limited telepathy, expressing emotions and general concepts. It can mimic a wide range of calls of other creatures, needing only to hear them a few times to learn.

The serpentine pseudodragon is a very gentle animal despite its highly lethal bite (save vs. Poison or die), normally striking only in self-defense. It feeds primarily on mice and small insects and is nocturnal. It prefers to inhabit warm and humid climates such as tropical rain forests and jungles, as its normal coloration, which varies anywhere between emerald green and bright red, allows it to camouflage itself best among the wide variety of life that such climates normally teem with. It is highly valued by wizards and collectors as it is quite rare, and while lethal in a standard dose, its poison can also be used as a strong painkiller and has some potent hallucinogenic properties.

Some subspecies of the serpentine pseudodragon are coated in colorful feathers with intricate patterns that rival even the brightest of birds of paradise. These subspecies possess limited speech and are known to be mischievous to the point of being almost unmanageable. Many prefer to live among humanoids in tropical areas, and they can be seen around the world as exotic pets for wealthy sailors and bountiful pirates. The feathered variety lack their more reptilian kin's poisonous bite but make up for it with the ability to spit its saliva up to 40', causing paralysis for an hour upon a failed save vs. Paralysis.
