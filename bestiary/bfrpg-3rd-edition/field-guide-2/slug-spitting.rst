.. title: Slug, Spitting
.. slug: slug-spitting
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-2
.. description: Slug, Spitting
.. type: text
.. image: jpg

+-----------------+--------------------------------+
| Armor Class:    | 11                             |
+-----------------+--------------------------------+
| Hit Dice:       | 2**                            |
+-----------------+--------------------------------+
| No. of Attacks: | 1 bite or spit                 |
+-----------------+--------------------------------+
| Damage:         | 1d4 + special or 1d8 + special |
+-----------------+--------------------------------+
| Movement:       | 10'                            |
+-----------------+--------------------------------+
| No. Appearing:  | 1d6                            |
+-----------------+--------------------------------+
| Save As:        | Magic-User: 1                  |
+-----------------+--------------------------------+
| Morale:         | 9                              |
+-----------------+--------------------------------+
| Treasure Type:  | None                           |
+-----------------+--------------------------------+
| XP:             | 125                            |
+-----------------+--------------------------------+

The **Spitting Slug** is a gastropod generally about the size of a medium pig and weighs about 500 lbs, although some have been seen that are larger than oxen. It can be found in damp shaded areas feeding on detritus and anything else that it can catch.

Although generally slow-moving, the spitting slug possesses a muscular internal "mouth" that it can extend outward rapidly to engulf a limb-sized object and latch on. Inside its mouth are small spines that secrete a paralyzing toxin (save vs. Paralysis + Constitution bonus or be unable to move) and digestive acids dealing 1 point of damage per round for as long as the slug is attached. A character wearing armor automatically saves, and does not take the acid damage* (see below). Characters that are affected by the toxin get a save each round to overcome the poison. If the victim is not paralyzed treat this similar to wrestling with the victim as the defender.

A slug which has successfully paralyzed its prey will attempt to swallow the victim, expanding its body to fit as much food as it can. Most human-sized creatures take 1d6+3 rounds to be engulfed, and as little as 1d4+1 rounds for smaller player races. Even when engulfed a player may continue to make a saving throw or try to escape if not paralyzed. Any attacks against the slug while it is attached will deal half-damage to the victim.

Additionally, true to its name once per day the spitting slug is able to spit a glob of its digestive acid that does 1d8 points of damage + 1 point of damage per round for 3 rounds on a hit (a save vs. Death Ray + Dexterity bonus negates the damage per round). Armor may also negate the damage from the acid*. The acid is stored in special glands within the slug, so simply touching or even stabbing into the slug will not cause the object to dissolve.

\* The acid is capable of dissolving armor. Every point of damage from the acid reduces the armor’s AC by 1 to a minimum of 11, at which point the armor has dissolved beyond being useful, and the acid thereafter deals damage to the victim instead.
