.. title: Geminate Serpent, Sea
.. slug: geminate-serpent-sea
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-7
.. description: Geminate Serpent, Sea
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 19                               |
+-----------------+----------------------------------+
| Hit Dice:       | 7**                              |
+-----------------+----------------------------------+
| No. of Attacks: | 2 bite or 1 breath or constrict* |
+-----------------+----------------------------------+
| Damage:         | 2d4/2d4 or breath or 1d6*        |
+-----------------+----------------------------------+
| Movement:       | 20' Swim 60’                     |
+-----------------+----------------------------------+
| No. Appearing:  | Lair 1d3+1                       |
+-----------------+----------------------------------+
| Save As:        | Fighter: 13                      |
+-----------------+----------------------------------+
| Morale:         | 10                               |
+-----------------+----------------------------------+
| Treasure Type:  | H+J                              |
+-----------------+----------------------------------+
| XP:             | 1,500                            |
+-----------------+----------------------------------+

**Sea Geminate Serpents** live in oceans and other large bodies of water. Each bite it takes has a 30% chance of paralyzing the target unless a save is made. Excelling in underwater combat, a sea geminate serpent prefers to blast its opponents off ships and boats by firing water from its mouth. However, once this has been done it must spend 1d4-1 rounds underwater filling up to do this again. Anything hit by a water spray will be moved five times the sea geminate serpent’s age category feet away from it, usually off their ship; a save vs. Dragon Breath can be made to dodge. Sea geminate serpents do not have lungs, and can only spend 5 rounds above water before needing to dive again; they must be submerged for at least 1 round to fully replenish their breath. Sea geminate serpents have their lairs in underwater caves, in the shallows safe from larger water-dwelling creatures.

**Sea Geminate Serpent Age Table**

+--------------+-----+-----+-----+------+------+------+------+
| Age Category | 1   | 2   | 3   | 4    | 5    | 6    | 7    |
+--------------+-----+-----+-----+------+------+------+------+
| Length       | 50’ | 70’ | 80’ | 100’ | 110’ | 130’ | 150’ |
+--------------+-----+-----+-----+------+------+------+------+
| Hit Dice     | 6   | 6   | 7   | 7    | 8    | 8    | 9    |
+--------------+-----+-----+-----+------+------+------+------+
| Attack Bonus | +6  | +7  | +7  | +7   | +8   | +9   | +9   |
+--------------+-----+-----+-----+------+------+------+------+
| Breath Type  | Water Spray (Line)                          |
+--------------+-----+-----+-----+------+------+------+------+
| Length       | -   | 40’ | 50’ | 60’  | 60’  | 70’  | 75’  |
+--------------+-----+-----+-----+------+------+------+------+
| Width        | -   | 10’ | 10’ | 20’  | 20’  | 30’  | 30’  |
+--------------+-----+-----+-----+------+------+------+------+
| Bite         | 1d6 | 1d8 | 2d4 | 2d4  | 3d4  | 4d4  | 3d6  |
+--------------+-----+-----+-----+------+------+------+------+
| Constrict    | 1d4 | 1d4 | 1d6 | 1d6  | 2d6  | 3d4  | 3d6  |
+--------------+-----+-----+-----+------+------+------+------+
| Talk         | -   | 10% | 20% | 40%  | 50%  | 60%  | 70%  |
+--------------+-----+-----+-----+------+------+------+------+
