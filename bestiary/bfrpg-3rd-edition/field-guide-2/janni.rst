.. title: Janni
.. slug: janni
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-3
.. description: Janni
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 15 (13)                          |
+-----------------+----------------------------------+
| Hit Dice:       | 3+1                              |
+-----------------+----------------------------------+
| No. of Attacks: | 1 weapon                         |
+-----------------+----------------------------------+
| Damage:         | 1d8+2 or by weapon +2            |
+-----------------+----------------------------------+
| Movement:       | 30' Unarmored 40'                |
+-----------------+----------------------------------+
| No. Appearing:  | Wild 2d20, Lair 3d20             |
+-----------------+----------------------------------+
| Save As:        | Fighter: 3                       |
+-----------------+----------------------------------+
| Morale:         | 8                                |
+-----------------+----------------------------------+
| Treasure Type:  | Q, R, S each; A in groups of 30+ |
+-----------------+----------------------------------+
| XP:             | 145                              |
+-----------------+----------------------------------+

**Janni** are the semi-magical descendants of djinni/human pairings. At a glance they appear to be normal humans with dark hair and skin sporting powerful and attractive builds. They favor living in desert environs, where they have both the safety and privacy they crave. Half of all janni tribes are nomadic and move from oasis to oasis, herding their goats, horses, and camels. A janni is polite and charming, and enjoys the company of foreign travelers. They are also highly honorable, and do not take kindly to insult or injury. A janni speaks Common fluently.

A janni is an expert horsemen and prefers to fight from horseback. Its uses large two-handed scimitars and longbows in combat. A janni will pursue its enemies over great distances and show little mercy to dishonorable combatants. It will sometimes ally itself with groups of desert-dwelling humans, and occasionally hire itself out as a mercenary.

One out of every 16 janni will be a hardened warrior of 5+2 HD (360 XP) and have a +2 bonus to damage. Regular janni led by a hardened warrior gain a +1 bonus to their morale. In groups of 30 or more, there will be a sheikh of 8+3 HD (875 XP) with AC 17 (13) and a +3 bonus to damage. Janni never fail morale as long as their sheikh lives. In addition, a lair has a 1-3 on 1d6 chance (1-4 on 1d6 if a sheikh is present) of a vizier being present. A vizier is equivalent to a normal janni statistically, but has Clerical abilities at level 1d4+1.
