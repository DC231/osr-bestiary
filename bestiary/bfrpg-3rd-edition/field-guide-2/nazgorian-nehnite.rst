.. title: Nazgorian, Nehnite
.. slug: nazgorian-nehnite
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-1
.. description: Nazgorian, Nehnite
.. type: text
.. image:

+-----------------+--------------------------------+
| Armor Class:    | 11 soft portions, 15 otherwise |
+-----------------+--------------------------------+
| Hit Dice:       | 1d4* per MU level (AB +1)      |
+-----------------+--------------------------------+
| No. of Attacks: | 1 bite or small weapon         |
+-----------------+--------------------------------+
| Damage:         | 1d2+poison or 1d4 or by weapon |
+-----------------+--------------------------------+
| Movement:       | 30'                            |
+-----------------+--------------------------------+
| No. Appearing:  | 1d4 Wild, 2d4 Lair             |
+-----------------+--------------------------------+
| Save As:        | Magic-User: 1+                 |
+-----------------+--------------------------------+
| Morale:         | 8                              |
+-----------------+--------------------------------+
| Treasure Type:  | Chance of magical wand         |
+-----------------+--------------------------------+
| XP:             | 13                             |
+-----------------+--------------------------------+

About the size of an average dog, a **Nehnite** has a segmented chitinous body similar to what one might see on certain spider or insect species. From this lower portion the nehnite's head and small manipulating arms and hands can be extended to give the creature an almost centaur-like form. The similarity ends there as the upper portion resembles something like a slimy salamander or eel. This soft vulnerable portion (AC 11) can be fully retracted into the carapace (AC 15) when threatened, with only its alien face showing through.

A nehnite avoids combat, preferring to command other Nazgorians (see the **Basic Fantasy Field Guide** **volume 1**) to protect it, but if cornered may bite with its poisonous fangs. Those bitten must save vs. Poison or suffer an additional 2d6 points of damage.

Like other Nazgoreans, nehnites have an alien intelligence which is impossible for others to understand, causing them to be immune to **charm** magic as well as **ESP** or any other form of mind-reading. Each can cast spells like a Magic-User, but with unfamiliar, even strange displays. For instance, one might produce a **fireball** effect made of green lightning, or **magic missiles** in the form of acid globes.

Nehnites are fond of carrying wands; each has a 10% per hit die chance of owning one, and if one is indicated, another 5% per hit die chance of owning a second. Such a wand has a 75% chance of being of Nazgorean origin, and thus possibly unusable by a normal character. However, a nehnite can always use a wand of the more common type, even without knowing (or speaking!) the command word or words.

Nehnites can command certain other Nazgoreans. One is almost never found without some frogmen around to serve it, and occasionally one might encounter a powerful nehnite protected by a gray render.

Nehnites, like other outsiders from Nazgor, suffer damage when exposed to sunlight or dry air. Sunlight alone causes 1d4 points of damage per hour, as does dry air; exposure to both results in 1d8 points of damage per hour.
