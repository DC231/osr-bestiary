.. title: Beebear
.. slug: beebear
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-7
.. description: Beebear
.. type: text
.. image: png

+-----------------+--------------------------+
| Armor Class:    | 16                       |
+-----------------+--------------------------+
| Hit Dice:       | 7*                       |
+-----------------+--------------------------+
| No. of Attacks: | 2 claw/1 bite or 1 sting |
+-----------------+--------------------------+
| Damage:         | 2d4/2d4/2d6 or special   |
+-----------------+--------------------------+
| Movement:       | 40' Fly 5’               |
+-----------------+--------------------------+
| No. Appearing:  | 1d6                      |
+-----------------+--------------------------+
| Save As:        | Fighter: 7               |
+-----------------+--------------------------+
| Morale:         | 7                        |
+-----------------+--------------------------+
| Treasure Type:  | None                     |
+-----------------+--------------------------+
| XP:             | 735                      |
+-----------------+--------------------------+

A **Beebear** is a bear that has been affected by a failed **polymorph** spell. As a result, it has black and yellow stripes and a minute pair of wings, granting it a very limited flight capability. A beebear is very loud and can be heard up to 30' away. Those stung by a beebear must make a save vs. Poison or be turned into a beebear. A **remove curse** or **polymorph** spell can reverse this effect. After stinging a victim, the beebear takes 1d8 damage and is unable to sting for the same amount of days as health lost. Brightly-colored clothing attracts these creatures as well as confuses them, as they think it is a flower. This results in the beebear sitting on anyone wearing bright colors and mauling them.
