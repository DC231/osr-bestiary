.. title: Aeromi
.. slug: aeromi
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-1
.. description: Aeromi
.. type: text
.. image:

+-----------------+-----------------------------------+
| Armor Class:    | 12                                |
+-----------------+-----------------------------------+
| Hit Dice:       | 1                                 |
+-----------------+-----------------------------------+
| No. of Attacks: | 2 claws or 1 weapon               |
+-----------------+-----------------------------------+
| Damage:         | 1d4/1d4 or by weapon              |
+-----------------+-----------------------------------+
| Movement:       | Unarmored 30' Glide 90' Climb 20' |
+-----------------+-----------------------------------+
| No. Appearing:  | 2d4, Wild 3d6, Lair 10d6          |
+-----------------+-----------------------------------+
| Save As:        | Fighter: 1                        |
+-----------------+-----------------------------------+
| Morale:         | 8                                 |
+-----------------+-----------------------------------+
| Treasure Type:  | Q, R each; D in lair              |
+-----------------+-----------------------------------+
| XP:             | 25                                |
+-----------------+-----------------------------------+

The arboreal **Aeromi** is a fur-covered humanoid about 4 to 5 feet in height with a long bushy tail. It has relatively long limbs ending in claws; they are primarily used for climbing, but make for formidable weapons as well. It is quite dexterous (accounting for the armor class). An aeromi has stretchable skin between its upper and lower limbs and along the side of its body (a patagium) that allows it to glide effectively between trees. This patagium is not continuous but instead is anchored in key points on its body. It speaks its own language that includes various clicks and chittering, but many also speak Common to some degree.

Aeromi are nocturnal and have Darkvision to a range of 90' when outdoors, even on the darkest of nights. On a clear night with ample star- or moonlight, an aeromi can see nearly as well as a human can in daylight. In indoor areas or underground the aeromi can see with Darkvision out to 30'. An aeromi suffers a -1 attack penalty in bright sunlight or within the radius of a **light** spell.

An aeromi climbs readily among the massive redwood and sequoia trees that it lives among. Aeromi villages are built upon platforms and bridges in the upper reaches of these forests.

An aeromi tends to carry little to allow mobility, but may utilize belts and clothing that can be fastened between the anchor points of its patagium. Aeromi may utilize humanoid weaponry when needed.

One out of every eight aeromi will be a warrior of 2 Hit Dice (75 XP). A regular aeromi gains a +1 bonus to its Morale if it’s led by such a warrior. In aeromi villages, one out of every twelve will be a sub-chief of 4 Hit Dice (240 XP) that does 1d6 damage with each claw attack. In lairs of 30 or more, there will be an aeromi chieftain of 6 Hit Dice (500 XP) that does 2d4 damage with each claw attack. In the village, aeromi never fail a morale check as long as the chieftain is alive. In addition, there is a chance equal to 1-2 on 1d6 of a shaman being present in any village. A shaman is equivalent to a warrior aeromi statistically, but has Clerical (or Druidic if that optional supplement is utilized by the GM) abilities at level 1d4+1. Each village also has a chance equal to 1 on 1d6 of a witch doctor being present; such an aeromi has the abilities of a Magic-User at level 1d4.
