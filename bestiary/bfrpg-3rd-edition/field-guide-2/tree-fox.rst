.. title: Tree-Fox
.. slug: tree-fox
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-1
.. description: Tree-Fox
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 16             |
+-----------------+----------------+
| Hit Dice:       | 1              |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 1d3/1d3/1d4    |
+-----------------+----------------+
| Movement:       | 40' Climb 30'  |
+-----------------+----------------+
| No. Appearing:  | 2d4            |
+-----------------+----------------+
| Save As:        | Fighter: 1     |
+-----------------+----------------+
| Morale:         | 8              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 25             |
+-----------------+----------------+

The feral **Tree-Fox** is a rather squirrel-like mammal in overall shape, though substantially larger. They are not actually related to squirrels, but more closely to that of foxes or other canines. There is a wide variety of tree-fox coloration and overall appearance. Their claws are used for climbing and make for effective attacks as well. Tree-foxes will form small packs to improve their hunting effectiveness. They are sometimes domesticated by forest dwellers for use as guardians or pets.

Some tree-fox breeds are so large as to have 2 or more hit dice and stronger attacks, and these varieties appear more wolf-like, so much as to be called "tree-wolves". Thankfully they are quite rare.
