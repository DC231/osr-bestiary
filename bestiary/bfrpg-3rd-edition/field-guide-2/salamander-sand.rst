.. title: Salamander, Sand*
.. slug: salamander-sand
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-7
.. description: Salamander, Sand*
.. type: text
.. image: jpg

+-----------------+-------------------------+
| Armor Class:    | 18‡                     |
+-----------------+-------------------------+
| Hit Dice:       | 7* (+4)                 |
+-----------------+-------------------------+
| No. of Attacks: | 1 bite                  |
+-----------------+-------------------------+
| Damage:         | 1d6 + petrification     |
+-----------------+-------------------------+
| Movement:       | 20'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d3, Wild 2d4, Lair 1d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 7              |
+-----------------+-------------------------+
| Morale:         | 8                       |
+-----------------+-------------------------+
| Treasure Type:  | L                       |
+-----------------+-------------------------+
| XP:             | 735                     |
+-----------------+-------------------------+

**Sand Salamanders** come from the Elemental Plane of Earth. A sand salamander resembles a giant sea turtle with six flippers and a serpentine neck and head, with scales of varying shades of gray or brown.

The sand salamander's most feared attack is its bite; anyone bitten by one must save vs. Petrify or be turned to stone. In addition to attacking, a sand salamander can temporarily transform any stone within a 20' radius into sand. Characters in the affected area must save vs. Paralysis each round in order to move through the sand, and if the save is successful, the character is still reduced to half his or her normal movement.

A sand salamander is immune to piercing attacks and suffers half damage from cutting attacks. It is intelligent and can speak the language of the Plane of Earth; many may also know Elvish, Common, or Dragon.
