.. title: Moonlight Butterfly
.. slug: moonlight-butterfly
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-1
.. description: Moonlight Butterfly
.. type: text
.. image:

+-----------------+---------------+
| Armor Class:    | 15            |
+-----------------+---------------+
| Hit Dice:       | 1 HP**        |
+-----------------+---------------+
| No. of Attacks: | None          |
+-----------------+---------------+
| Damage:         | Special       |
+-----------------+---------------+
| Movement:       | Fly 30'       |
+-----------------+---------------+
| No. Appearing:  | 1             |
+-----------------+---------------+
| Save As:        | Magic-User: 1 |
+-----------------+---------------+
| Morale:         | 6             |
+-----------------+---------------+
| Treasure Type:  | None          |
+-----------------+---------------+
| XP:             | 100           |
+-----------------+---------------+

A **Moonlight Butterfly** is a large nocturnal butterfly spanning a foot. During the night it will seek out intelligent creatures that are asleep, consuming the dreams of all within a 30’ radius. This process takes a turn, during which it will emit a pale white light reminiscent of the moon.

Creatures that have had their dreams eaten are effectively left in a comatose state, not waking up to mundane stimuli such as to dawn, loud noises, or pain. They are left extremely sensitive to magic and any spell cast within 100’ of a moonlight butterfly’s victims will cause him or her to awaken violently, losing half their present hit points due to having their senses overloaded.
