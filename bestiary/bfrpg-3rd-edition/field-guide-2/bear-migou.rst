.. title: Bear, Migou
.. slug: bear-migou
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-6
.. description: Bear, Migou
.. type: text
.. image:

+-----------------+-----------------------------------------+
| Armor Class:    | 14                                      |
+-----------------+-----------------------------------------+
| Hit Dice:       | 6*                                      |
+-----------------+-----------------------------------------+
| No. of Attacks: | 2 claws/1 bite + 1 hug or 1 special     |
+-----------------+-----------------------------------------+
| Damage:         | 1d6 claw, 1d12 bite, 2d6 hug or special |
+-----------------+-----------------------------------------+
| Movement:       | 60'                                     |
+-----------------+-----------------------------------------+
| No. Appearing:  | 1                                       |
+-----------------+-----------------------------------------+
| Save As:        | Fighter: 6                              |
+-----------------+-----------------------------------------+
| Morale:         | 9                                       |
+-----------------+-----------------------------------------+
| Treasure Type:  | None                                    |
+-----------------+-----------------------------------------+
| XP:             | 485                                     |
+-----------------+-----------------------------------------+

The **Migou**, or Yueh-Teh, is strange, magical creature. Migou are solitary; mating is a yearly event and a female gives birth to live young that follow her around for a year before going their own way. The migou is a relentless and extremely savage predator, preferentially attacking humanoids and canines as delicacies. The migou’s hind paws bear a strange resemblance to humanoid feet, and they can move rapidly on two legs as well as four. The front paws of the migou are covered in long shaggy fur, and its claws run in a vertical row on the front paws, resembling a spiky fin.

Migou have a mental power which make them a vastly more formidable opponent than a normal bear. This attack is called **phantasmal visions** and is a form of illusion that creates an effect similar to **hold person**. Any normal living creature within a 60’ radius of the migou must save vs. Paralysis or enter a kind of mental freeze, standing still and staring off into space for at most ten minutes. Each time a paralyzed victim suffers an injury he or she may immediately re-roll this saving throw. Creatures within the area who make their saving throw are not immune, and must save again if the migou uses its power again. If the migou chooses to use this power it must forego its normal attacks. Migou are immune to the powers of other migou.
