.. title: Geminate Serpent, Shadow
.. slug: geminate-serpent-shadow
.. date: 2019-11-08 13:50:52.588217
.. tags: hd-8
.. description: Geminate Serpent, Shadow
.. type: text
.. image:

+-----------------+----------------------------------------------+
| Armor Class:    | 21                                           |
+-----------------+----------------------------------------------+
| Hit Dice:       | 8**                                          |
+-----------------+----------------------------------------------+
| No. of Attacks: | 2 bite or 1 breath or constrict*             |
+-----------------+----------------------------------------------+
| Damage:         | 2d12+energy drain (1 level) or breathor 2d4* |
+-----------------+----------------------------------------------+
| Movement:       | 10'                                          |
+-----------------+----------------------------------------------+
| No. Appearing:  | Lair 1d3+1                                   |
+-----------------+----------------------------------------------+
| Save As:        | Fighter: 11                                  |
+-----------------+----------------------------------------------+
| Morale:         | 7                                            |
+-----------------+----------------------------------------------+
| Treasure Type:  | H                                            |
+-----------------+----------------------------------------------+
| XP:             | 2,200                                        |
+-----------------+----------------------------------------------+

**Shadow Geminate Serpents** are dark beings seldom seen, living in the shadows. These serpents were born of the shadow and cannot move outside of one; if they do they take 2d8 damage each round they are in direct sun or moonlight. If they die in light they turn to ash. The shadow geminate serpent moves silently with the same skill as a thief of double its age category. They use this to pick victims off the back of groups and weaken them in preparation for their child. If their cover is blown they are very likely to run away and try again later. To aid them in this, their breath has the same effect as the spell **darkness**.

**Shadow Geminate Serpent Age Table**

+--------------+-----+-----+------+------+------+------+------+
| Age Category | 1   | 2   | 3    | 4    | 5    | 6    | 7    |
+--------------+-----+-----+------+------+------+------+------+
| Length       | 60’ | 70’ | 80’  | 90’  | 90’  | 100’ | 110’ |
+--------------+-----+-----+------+------+------+------+------+
| Hit Dice     | 6   | 7   | 8    | 8    | 9    | 9    | 10   |
+--------------+-----+-----+------+------+------+------+------+
| Attack Bonus | +6  | +6  | +7   | +8   | +9   | +9   | +10  |
+--------------+-----+-----+------+------+------+------+------+
| Breath Type  | Darkness                                     |
+--------------+-----+-----+------+------+------+------+------+
| Length       | -   | 30’ | 30’  | 40’  | 50’  | 50’  | 60’  |
+--------------+-----+-----+------+------+------+------+------+
| Width        | -   | 10’ | 20’  | 20’  | 30’  | 40’  | 40’  |
+--------------+-----+-----+------+------+------+------+------+
| Bite         | 2d6 | 2d8 | 2d10 | 2d12 | 2d12 | 5d6  | 5d6  |
+--------------+-----+-----+------+------+------+------+------+
| Constrict    | 1d8 | 1d8 | 2d4  | 2d4  | 2d6  | 3d4  | 4d4  |
+--------------+-----+-----+------+------+------+------+------+
| Talk         | -   | 10% | 20%  | 40%  | 50%  | 50%  | 60%  |
+--------------+-----+-----+------+------+------+------+------+
