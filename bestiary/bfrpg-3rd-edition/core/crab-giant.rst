.. title: Crab, Giant
.. slug: crab-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: amphibian, beast, hd-3, hd-3
.. description: Crab, Giant
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 18                      |
+-----------------+-------------------------+
| Hit Dice:       | 3                       |
+-----------------+-------------------------+
| No. of Attacks: | 2 pincers               |
+-----------------+-------------------------+
| Damage:         | 2d6/2d6                 |
+-----------------+-------------------------+
| Movement:       | 20' Swim 20'            |
+-----------------+-------------------------+
| No. Appearing:  | 1d2, Wild 1d6, Lair 1d6 |
+-----------------+-------------------------+
| Save As:        | Fighter: 3              |
+-----------------+-------------------------+
| Morale:         | 7                       |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 145                     |
+-----------------+-------------------------+

Giant crabs naturally resemble the ordinary variety, but are much larger, averaging 5' in diameter (not counting their legs). These creatures are often found in water-filled caves, particularly those connected to a river, lake or sea, and are tolerant of both fresh and salt water. Also, they are able to live in stagnant water, though they prefer a better environment.

Giant crabs carry their eyes on armored stalks, which means that no bonus is awarded for attacking them from behind.
