.. title: Blood Rose
.. slug: blood-rose
.. date: 2019-11-08 13:50:36.109736
.. tags: hd-2, hd-2, plant
.. description: Blood Rose
.. type: text
.. image: jpg

+-----------------+-------------------------+
| Armor Class:    | 13                      |
+-----------------+-------------------------+
| Hit Dice:       | 2* to 4*                |
+-----------------+-------------------------+
| No. of Attacks: | 1 to 3 plus blood drain |
+-----------------+-------------------------+
| Damage:         | 1d6                     |
+-----------------+-------------------------+
| Movement:       | 1'                      |
+-----------------+-------------------------+
| No. Appearing:  | Wild 1d8                |
+-----------------+-------------------------+
| Save As:        | Fighter: 2              |
+-----------------+-------------------------+
| Morale:         | 12                      |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 100 - 280               |
+-----------------+-------------------------+

Blood roses appear to be normal rose bushes, but are actually animated plants, dimly aware of their surroundings. These plants are always in bloom, bearing beautiful flowers that are normally white (or rarely, yellow) in color.

The fragrance of the flowers is detectable up to 30' from the plant in ideal conditions. Blood roses can move about slowly, and will try to find locations sheltered from the wind in order to achieve those ideal conditions. Living creatures who smell the fragrance must save vs. Poison or become befuddled, dropping anything carried and approaching the plant. Each round such a creature or character is within the affected area, this save must be made. Befuddled characters will not resist the plant-creature's attacks; if affected creatures are removed from the area, the effect of the fragrance will expire 2d4 rounds later. Undead monsters, constructs, etc. are not affected.

Each blood rose plant will have 1, 2 or 3 whiplike canes studded with thorns with which it can attack. When a cane hits, it wraps around the victim and begins to drain blood, doing 1d6 points of damage per round. A blood rose which has recently (within one day) “eaten” this way will have flowers ranging from pink to deep wine red in color, which will fade slowly back to white or yellow as the plant digests the blood it has consumed.
