.. title: Dragon, White
.. slug: dragon-white
.. date: 2019-11-08 13:50:36.109736
.. tags: dragon, flying, hd-6, hd-6
.. description: Dragon, White
.. type: text
.. image:

+-----------------+---------------------------------+
| Armor Class:    | 17                              |
+-----------------+---------------------------------+
| Hit Dice:       | 6**                             |
+-----------------+---------------------------------+
| No. of Attacks: | 2 claws/1 bite or breath/1 tail |
+-----------------+---------------------------------+
| Damage:         | 1d4/1d4/2d8 or breath/1d4       |
+-----------------+---------------------------------+
| Movement:       | 30' Fly 80' (10')               |
+-----------------+---------------------------------+
| No. Appearing:  | 1, Wild 1, Lair 1d4             |
+-----------------+---------------------------------+
| Save As:        | Fighter: 6 (as Hit Dice)        |
+-----------------+---------------------------------+
| Morale:         | 8                               |
+-----------------+---------------------------------+
| Treasure Type:  | H                               |
+-----------------+---------------------------------+
| XP:             | 610                             |
+-----------------+---------------------------------+

White Dragons prefer to live in cold regions, whether in the highest mountains or in the cold northern lands. They are the least intelligent of dragons, though this does not mean that they are stupid by any stretch of the imagination. They are motivated completely by a drive to live, to reproduce, and (of course) to accumulate treasure; they kill to live, not for pleasure.

White dragons prefer sudden assaults, swooping down from aloft or bursting from beneath water, snow, or ice. Typically, a white dragon begins with its icy breath weapon, then tries to eliminate a single opponent with a follow-up attack.

White dragons are immune to normal cold, and take only half damage from magical cold or ice.

ERROR: One of the span's columns extends beyond the bounds of the table: [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]]
