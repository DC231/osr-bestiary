.. title: Hyena
.. slug: hyena
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, hd-2, hd-2
.. description: Hyena
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 13         |
+-----------------+------------+
| Hit Dice:       | 2+1        |
+-----------------+------------+
| No. of Attacks: | 1 bite     |
+-----------------+------------+
| Damage:         | 1d6        |
+-----------------+------------+
| Movement:       | 60'        |
+-----------------+------------+
| No. Appearing:  | 1d8        |
+-----------------+------------+
| Save As:        | Fighter: 2 |
+-----------------+------------+
| Morale:         | 8          |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 75         |
+-----------------+------------+

Hyenas are doglike carnivores who exhibit some of the behaviors of canines but are not related. They not only hunt but also scavenge and steal meals. A hungry hyena will chew on anything that is even remotely tainted by blood, meat or other food traces. They will mostly be found in the same savanna-like environments where lions and zebras may be found. They can live in clans of up to a hundred individuals, though smaller groups are more common. They are among the favorite pets of gnolls, who may take them into regions where they are not normally found.
