.. title: Centaur
.. slug: centaur
.. date: 2019-11-08 13:50:36.109736
.. tags: hd-4, hd-4, humanoid
.. description: Centaurs appear to be half man, half horse, having the torso, arms and head of a man in the position a horse's head would otherwise occupy.
.. type: text
.. image: png

+-----------------+--------------------------+
| Armor Class:    | 15 (13)                  |
+-----------------+--------------------------+
| Hit Dice:       | 4                        |
+-----------------+--------------------------+
| No. of Attacks: | 2 hooves/1 weapon        |
+-----------------+--------------------------+
| Damage:         | 1d6/1d6/1d6 or by weapon |
+-----------------+--------------------------+
| Movement:       | 50' Unarmored 60' (10')  |
+-----------------+--------------------------+
| No. Appearing:  | Wild 2d10                |
+-----------------+--------------------------+
| Save As:        | Fighter: 4               |
+-----------------+--------------------------+
| Morale:         | 8                        |
+-----------------+--------------------------+
| Treasure Type:  | A                        |
+-----------------+--------------------------+
| XP:             | 240                      |
+-----------------+--------------------------+

Centaurs appear to be half man, half horse, having the torso, arms and head of a man in the position a horse's head would otherwise occupy. A centaur is as big as a heavy horse, but much taller and slightly heavier; average males are about 7 feet tall and weigh about 2,100 pounds, and females are just a bit smaller. **Centaurs may charge with a spear or lance** just as a man on horseback, with the same bonuses. They typically wear leather armor when prepared for combat.

Centaurs are generally haughty and aloof, but very honorable. Most would rather die than allow humans, demi-humans, or humanoids to ride on their backs.
