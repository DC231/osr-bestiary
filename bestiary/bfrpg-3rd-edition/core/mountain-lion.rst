.. title: Mountain Lion
.. slug: mountain-lion
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, hd-3, hd-3
.. description: Mountain Lion
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 14                 |
+-----------------+--------------------+
| Hit Dice:       | 3+2                |
+-----------------+--------------------+
| No. of Attacks: | 2 claws/1 bite     |
+-----------------+--------------------+
| Damage:         | 1d4/1d4/1d6        |
+-----------------+--------------------+
| Movement:       | 50'                |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d4, Lair 1d4 |
+-----------------+--------------------+
| Save As:        | Fighter: 3         |
+-----------------+--------------------+
| Morale:         | 8                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 145                |
+-----------------+--------------------+

These great cats are about 7 feet long (from nose to tail-tip) and weigh about 140 pounds. They see well in darkness and may be found hunting day or night.
