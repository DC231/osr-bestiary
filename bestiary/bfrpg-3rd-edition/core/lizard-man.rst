.. title: Lizard Man
.. slug: lizard-man
.. date: 2019-11-08 13:50:36.109736
.. tags: hd-2, hd-2, humanoid
.. description: Lizard Man
.. type: text
.. image: png

+-----------------+------------------------------------------+
| Armor Class:    | 15 (12)                                  |
+-----------------+------------------------------------------+
| Hit Dice:       | 2                                        |
+-----------------+------------------------------------------+
| No. of Attacks: | 1 weapon                                 |
+-----------------+------------------------------------------+
| Damage:         | 1d6+1 or by weapon +1                    |
+-----------------+------------------------------------------+
| Movement:       | 20' Unarmored 30'Swim 40' (not in armor) |
+-----------------+------------------------------------------+
| No. Appearing:  | 2d4, Wild 2d4, Lair 6d6                  |
+-----------------+------------------------------------------+
| Save As:        | Fighter: 2                               |
+-----------------+------------------------------------------+
| Morale:         | 11                                       |
+-----------------+------------------------------------------+
| Treasure Type:  | D                                        |
+-----------------+------------------------------------------+
| XP:             | 75                                       |
+-----------------+------------------------------------------+

A lizard man is usually 6 to 7 feet tall with green, gray, or brown scales. Its tail is used for balance and is 3 to 4 feet long. Adult males can weigh from 200 to 250 pounds. Due to their great Strength they always receive a +1 to damage done with melee weapons. They wear leather armor and carry shields in battle.

Lizard men are excellent swimmers and can hold their breath for an extended period of time (up to a full turn). They cannot swim while wearing armor; however, they often hide in the water even while armored, standing on the bottom with just nose and eyes exposed (similar to a crocodile). When they are able to employ this maneuver, lizard men surprise on 1-4 on 1d6.

Lizard men are largely indifferent to other races, being primarily interested in their own survival. If aroused, however, they are fearsome warriors, using simple but sound tactics.
