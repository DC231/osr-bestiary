.. title: Vampire*
.. slug: vampire
.. date: 2019-11-08 13:50:36.109736
.. tags: hd-7, hd-7, undead
.. description: Vampires are savvy, powerful, blood-drinking creatures of the night. They appear just as they did in life, although their features are often hardened and feral.
.. type: text
.. image: png

+-----------------+----------------------------------+
| Armor Class:    | 18 to 20 ‡                       |
+-----------------+----------------------------------+
| Hit Dice:       | 7** to 9** (+8)                  |
+-----------------+----------------------------------+
| No. of Attacks: | 1 weapon or special              |
+-----------------+----------------------------------+
| Damage:         | 1d8 or by weapon +3 or special   |
+-----------------+----------------------------------+
| Movement:       | 40' Fly 60'                      |
+-----------------+----------------------------------+
| No. Appearing:  | 1d6, Wild 1d6, Lair 1d6          |
+-----------------+----------------------------------+
| Save As:        | Fighter: 7 to 9 (as Hit Dice)    |
+-----------------+----------------------------------+
| Morale:         | 11                               |
+-----------------+----------------------------------+
| Treasure Type:  | F                                |
+-----------------+----------------------------------+
| XP:             | 800 - 1,225                      |
+-----------------+----------------------------------+

Vampires appear just as they did in life, although their features are often hardened and feral, with the predatory look of wolves. They often embrace finery and decadence and may assume the guise of nobility. Despite their human appearance, vampires can be easily recognized, for they cast no shadows and throw no reflections in mirrors. They speak any languages they knew in life.

A vampire can **charm** anyone who meets its gaze; a save vs. Spells is allowed to resist, but at a penalty of -2 due to the power of the charm. This charm is so powerful that the victim will not resist being bitten by the vampire.

The **bite** inflicts 1d3 damage, then each round thereafter one energy level is drained from the victim. The vampire regenerates a 1d6 hit points (if needed) for each energy level drained. If the victim dies from the energy drain, he or she will arise as a vampire at the next sunset (but not less than 12 hours later). Vampires spawned in this way are under the permanent control of the vampire who created them. If using the bite attack, the vampire suffers a *penalty of -5 to Armor Class* due to the vulnerable position it must assume. For this reason, the bite is rarely used in combat.

Vampires have great Strength, gaining a bonus of *+3 to damage when using melee weapons,* and a vampire will generally choose to use a melee weapon (or even its bare hands) in combat rather than attempting to bite.

Vampires are *unharmed by non-magical weapons,* and like all undead are *immune to sleep, charm and hold* spells. If reduced to 0 hit points in combat, the vampire is not destroyed, though it may appear to be. The vampire will begin to *regenerate* 1d8 hours later, recovering 1 hit point per turn, and resuming normal activity as soon as the first point is restored.

Vampires **command** *the lesser creatures of the night* and once per day can **call forth**
10d10 :doc:`rats <rat>`,
5d4 :doc:`giant rats <rat>`,
10d10 :doc:`bats <bat-and-bat-giant>`,
3d6 :doc:`giant bats <bat-and-bat-giant>`,
or a pack of 3d6 :doc:`wolves <wolf>`
(assuming any such creatures are nearby). These creatures arrive in 2d6 rounds and serve the vampire for up to 1 hour.

A vampire can **assume the form of a** :doc:`giant bat <bat-and-bat-giant>` **or a** :doc:`dire wolf <wolf>` at will, requiring one round to complete the transformation. The flying movement listed is for the giant bat form. In animal form, the vampire can use the normal attacks for that form. It *can't use its other powers while in animal form,* except that creatures summoned are still controlled, and charms already in effect continue in effect.

Repelling a Vampire
-------------------
For all their power, vampires have a number of weaknesses. *Vampires cannot tolerate the strong odor of garlic* and will not enter an area laced with it. Similarly, *they recoil from a mirror or a strongly presented holy symbol.* These things don’t harm the vampire – they merely keep it at bay. *A recoiling vampire must stay at least 5 feet away from a creature holding the mirror or holy symbol and cannot touch or make melee attacks against the creature holding the item for the rest of the encounter.*

*Vampires are unable to cross running water,* although they can be carried over it while resting in their coffins or aboard a ship. They are utterly *unable to enter a home or other building unless invited* in by someone with the authority to do so. They may freely enter public places, since these are by definition open to all.

Slaying a Vampire
-----------------
Reducing a vampire’s hit points to 0 or lower incapacitates it but doesn’t always destroy it, as described above. However, certain attacks can slay vampires. Exposing any vampire to *direct sunlight disorients it*: It can act for only one round and is destroyed utterly in the next round if it cannot escape. Similarly, *immersing a vampire in running water* robs it of one-third of its hit points each round until it is destroyed at the end of the third round of immersion. Driving a *wooden stake* through a vampire’s heart instantly slays the monster. However, it returns to life if the stake is removed, unless the body is destroyed, by water or sunlight as described above, or by burning it completely in a funeral pyre.
