.. title: Manticore
.. slug: manticore
.. date: 2019-11-08 13:50:36.109736
.. tags: flying, hd-6, hd-6, monstrosity
.. description: Manticore
.. type: text
.. image:

+-----------------+-----------------------------------------+
| Armor Class:    | 18                                      |
+-----------------+-----------------------------------------+
| Hit Dice:       | 6+1*                                    |
+-----------------+-----------------------------------------+
| No. of Attacks: | 2 claws/1 bite or 6 spikes (180' range) |
+-----------------+-----------------------------------------+
| Damage:         | 1d4/1d4/2d4 or 1d6 per spike            |
+-----------------+-----------------------------------------+
| Movement:       | 40' Fly 60' (10')                       |
+-----------------+-----------------------------------------+
| No. Appearing:  | 1d2, Wild 1d4, Lair 1d4                 |
+-----------------+-----------------------------------------+
| Save As:        | Fighter: 6                              |
+-----------------+-----------------------------------------+
| Morale:         | 9                                       |
+-----------------+-----------------------------------------+
| Treasure Type:  | D                                       |
+-----------------+-----------------------------------------+
| XP:             | 555                                     |
+-----------------+-----------------------------------------+

Manticores look like an overgrown lion with thick leathery wings and an ugly humanoid face, often like that of a human or bearded dwarf. Their tail ends in an assortment of spikes, which the beast may fire as projectiles; a maximum of 24 are available, and the manticore regrows 1d6 per day. A typical manticore is about 10 feet long and weighs about 1,000 pounds.

Manticores are vicious predators, having a preference for human flesh. They will use their ranged attacks to “soften up” potential prey before closing to melee.
