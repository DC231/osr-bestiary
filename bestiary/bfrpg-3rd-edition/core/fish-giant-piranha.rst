.. title: Fish, Giant Piranha
.. slug: fish-giant-piranha
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast, hd-4, hd-4
.. description: Fish, Giant Piranha
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 15         |
+-----------------+------------+
| Hit Dice:       | 4          |
+-----------------+------------+
| No. of Attacks: | 1 bite     |
+-----------------+------------+
| Damage:         | 1d8        |
+-----------------+------------+
| Movement:       | Swim 50'   |
+-----------------+------------+
| No. Appearing:  | Wild 2d4   |
+-----------------+------------+
| Save As:        | Fighter: 4 |
+-----------------+------------+
| Morale:         | 7 (11)     |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 240        |
+-----------------+------------+

Giant piranha average 5' in length at adulthood, and are aggressive carnivores. They are able to sense blood in the water just as sharks do, and once they smell or taste blood in the water, their morale rises to the parenthesized figure.
