.. title: Leech, Giant
.. slug: leech-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast, hd-6, hd-6
.. description: Leech, Giant
.. type: text
.. image:

+-----------------+-----------------+
| Armor Class:    | 17              |
+-----------------+-----------------+
| Hit Dice:       | 6               |
+-----------------+-----------------+
| No. of Attacks: | 1 bite + hold   |
+-----------------+-----------------+
| Damage:         | 1d6 + 1d6/round |
+-----------------+-----------------+
| Movement:       | 30'             |
+-----------------+-----------------+
| No. Appearing:  | Wild 1d4        |
+-----------------+-----------------+
| Save As:        | Fighter: 6      |
+-----------------+-----------------+
| Morale:         | 10              |
+-----------------+-----------------+
| Treasure Type:  | None            |
+-----------------+-----------------+
| XP:             | 500             |
+-----------------+-----------------+

Giant leeches are slimy, segmented wormlike creatures which live in water. Salt or fresh, clean or stagnant, there are giant leech varieties for all wet environments. However, only a true leech expert can tell the various types apart. An average giant leech will be 4 to 6 feet long.

Once a giant leech hits in combat, it attaches to the victim and sucks blood, causing an additional 1d6 damage each round until the victim or the leech is dead. There is no way to remove the leech other than to kill it.
