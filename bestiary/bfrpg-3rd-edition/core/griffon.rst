.. title: Griffon
.. slug: griffon
.. date: 2019-11-08 13:50:36.109736
.. tags: flying, hd-7, hd-7, monstrosity
.. description: Griffon
.. type: text
.. image: png

+-----------------+--------------------------+
| Armor Class:    | 18                       |
+-----------------+--------------------------+
| Hit Dice:       | 7                        |
+-----------------+--------------------------+
| No. of Attacks: | 2 claws/1 bite           |
+-----------------+--------------------------+
| Damage:         | 1d4/1d4/2d8              |
+-----------------+--------------------------+
| Movement:       | 40' (10') Fly 120' (10') |
+-----------------+--------------------------+
| No. Appearing:  | Wild 2d8, Lair 2d8       |
+-----------------+--------------------------+
| Save As:        | Fighter: 7               |
+-----------------+--------------------------+
| Morale:         | 8                        |
+-----------------+--------------------------+
| Treasure Type:  | E                        |
+-----------------+--------------------------+
| XP:             | 670                      |
+-----------------+--------------------------+

Griffons are large carnivorous creatures resembling lions with the head, foreclaws and wings of eagles. From nose to tail, an adult griffon can measure as much as 8 feet. Neither males nor females are endowed with a mane. A pair of broad, golden wings emerge from the creature’s back and span 25 feet or more. An adult griffon weighs about 500 pounds.

Griffons nest on high mountaintops, soaring down to feed on horses, the beast’s preferred prey. They hunt and travel in ocks. A Griffon will attack a horse over anything else, diving low to swipe with their claws. They are not above retreating and coming back later, when there may be less of a defense mounted.

Griffons can be trained as mounts if raised in captivity, but even in this case they may try to attack horses if any come within about 120'. Roll a morale check in this case; if the check is failed, the griffon will try to attack immediately. A light load for a griffon is up to 400 pounds; a heavy load, up to 900 pounds.
