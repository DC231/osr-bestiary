.. title: Shrew, Giant
.. slug: shrew-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, hd-1, hd-1
.. description: Shrew, Giant
.. type: text
.. image:

+-----------------+-------------------------+
| Armor Class:    | 16                      |
+-----------------+-------------------------+
| Hit Dice:       | 1*                      |
+-----------------+-------------------------+
| No. of Attacks: | 2 bites                 |
+-----------------+-------------------------+
| Damage:         | 1d6/1d6                 |
+-----------------+-------------------------+
| Movement:       | 60'                     |
+-----------------+-------------------------+
| No. Appearing:  | 1d4, Wild 1d8, Lair 1d8 |
+-----------------+-------------------------+
| Save As:        | Fighter: 2              |
+-----------------+-------------------------+
| Morale:         | 10                      |
+-----------------+-------------------------+
| Treasure Type:  | None                    |
+-----------------+-------------------------+
| XP:             | 37                      |
+-----------------+-------------------------+

Giant shrews resemble giant rats, but are larger, being up to 6' long, and darker in color. They have a very fast metabolic rate and must eat almost constantly. Giant shrews are omnivorous, and aggressively defend their nests and the immediate territory around them.

Giant shrews move so swiftly that they are able to bite twice per round, and they may attack two different adjacent opponents in this way.

A few giant shrew species (generally no more than 5% of those encountered) are venomous. The bite of such a giant shrew will kill the victim unless a save vs. Poison is made. A victim bitten twice in a round need only save once for that round, but of course will have to save again in subsequent rounds if bitten again. Venomous giant shrews are considered 1* with respect to hit dice.
