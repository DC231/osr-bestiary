.. title: Golem, Bone*
.. slug: golem-bone
.. date: 2019-11-08 13:50:36.109736
.. tags: construct, hd-8, hd-8
.. description: Golem, Bone*
.. type: text
.. image:

+-----------------+------------------------------+
| Armor Class:    | 19 ‡                         |
+-----------------+------------------------------+
| Hit Dice:       | 8*                           |
+-----------------+------------------------------+
| No. of Attacks: | 4 weapons                    |
+-----------------+------------------------------+
| Damage:         | 1d6/1d6/1d6/1d6 or by weapon |
+-----------------+------------------------------+
| Movement:       | 40' (10')                    |
+-----------------+------------------------------+
| No. Appearing:  | 1                            |
+-----------------+------------------------------+
| Save As:        | Fighter: 4                   |
+-----------------+------------------------------+
| Morale:         | 12                           |
+-----------------+------------------------------+
| Treasure Type:  | None                         |
+-----------------+------------------------------+
| XP:             | 945                          |
+-----------------+------------------------------+

Bone golems are huge four-armed monsters created from the skeletons of at least two dead humanoids. Though made of bone, they are not undead and cannot be turned.

Instead of four one-handed weapons, a bone golem can be armed with two two-handed weapons, giving 2 attacks per round and a damage figure of 1d10/1d10 or by weapon.

When a bone golem enters combat, there is a cumulative 1% chance each round that its elemental spirit breaks free and the golem goes berserk. The uncontrolled golem goes on a rampage, attacking the nearest living creature or smashing some object smaller than itself if no creature is within reach, then moving on to spread more destruction. The golem’s creator, if within 60 feet, can try to regain control by speaking firmly and persuasively to the golem; he or she must make a save vs. Spells to succeed at this, and at least 1 round of time is required for each check. It takes 1 round of inactivity by the golem to reset the golem’s berserk chance to 0%.
