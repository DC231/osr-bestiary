.. title: Ghast
.. slug: ghast
.. date: 2019-11-08 13:50:36.109736
.. tags: hd-2, hd-2, undead
.. description: Ghast
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 15                               |
+-----------------+----------------------------------+
| Hit Dice:       | 2**                              |
+-----------------+----------------------------------+
| No. of Attacks: | 2 claws/1 bite                   |
+-----------------+----------------------------------+
| Damage:         | 1d4/1d4/1d4 + paralysis + stench |
+-----------------+----------------------------------+
| Movement:       | 30'                              |
+-----------------+----------------------------------+
| No. Appearing:  | 1d4 Wild/Lair 1d8                |
+-----------------+----------------------------------+
| Save As:        | Fighter: 2                       |
+-----------------+----------------------------------+
| Morale:         | 9                                |
+-----------------+----------------------------------+
| Treasure Type:  | B                                |
+-----------------+----------------------------------+
| XP:             | 125                              |
+-----------------+----------------------------------+

Although these creatures look just like their lesser kin, the ghoul, they are far more deadly and cunning. Those hit by a ghast’s bite or claw attack must save vs. Paralyzation or be paralyzed for 2d8 turns. Elves are immune to this paralysis. Ghasts try to attack with surprise whenever possible, striking from behind tombstones and bursting from shallow graves; when these methods are employed, they are able to surprise opponents on 1-3 on 1d6. They are undead, and thus are immune to sleep, charm and hold magics. They may be Turned by Clerics using the same column as the ghoul. As they are superior to ghouls, in a mixed group of ghasts and ghouls the GM should apply Turning effects to the ordinary ghouls first.

Humanoids bitten by ghasts may be infected with ghast fever. Each time a humanoid is bitten, there is a 10% chance of the infection being passed. The afflicted humanoid is allowed to save vs. Death Ray; if the save is failed, the humanoid dies within a day.

An afflicted humanoid who dies of ghast fever rises as a ghast at the next midnight. A humanoid who becomes a ghast in this way retains none of the knowledge or abilities he or she possessed in life. The newly-risen ghast is not under the control of any other ghasts, but hungers for the flesh of the living and behaves like any other ghast in all respects.

The stink of death and corruption surrounding these creatures is overwhelming. Living creatures within 10 feet must succeed on a save vs. Poison or be sickened for 2d6 rounds (-2 to attack rolls). A creature that successfully saves cannot be affected again by the same ghast’s stench for 24 hours. A **neutralize poison** spell removes the effect from a sickened creature.
