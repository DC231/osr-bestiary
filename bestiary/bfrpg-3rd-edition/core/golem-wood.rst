.. title: Golem, Wood*
.. slug: golem-wood
.. date: 2019-11-08 13:50:36.109736
.. tags: construct, hd-2, hd-2
.. description: Golem, Wood*
.. type: text
.. image:

+-----------------+------------+
| Armor Class:    | 13 ‡       |
+-----------------+------------+
| Hit Dice:       | 2+2*       |
+-----------------+------------+
| No. of Attacks: | 1 fist     |
+-----------------+------------+
| Damage:         | 1d8        |
+-----------------+------------+
| Movement:       | 40'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 1 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 100        |
+-----------------+------------+

Wood golems are small constructs, not more than 4' in height, and are crudely made. Being made of wood makes them vulnerable to fire-based attacks; thus, wood golems suffer one extra point of damage per die from fire; any saving throws against such effects are at a penalty of -2. They move stiffly, suffering a -1 penalty to Initiative.
