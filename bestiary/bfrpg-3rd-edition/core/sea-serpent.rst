.. title: Sea Serpent
.. slug: sea-serpent
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast, hd-6, hd-6
.. description: Sea Serpent
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 17             |
+-----------------+----------------+
| Hit Dice:       | 6              |
+-----------------+----------------+
| No. of Attacks: | 1 bite         |
+-----------------+----------------+
| Damage:         | 2d6            |
+-----------------+----------------+
| Movement:       | Swim 50' (10') |
+-----------------+----------------+
| No. Appearing:  | Wild 2d6       |
+-----------------+----------------+
| Save As:        | Fighter: 6     |
+-----------------+----------------+
| Morale:         | 8              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 500            |
+-----------------+----------------+

Sea serpents are, obviously, serpentine monsters which live in the sea. They range from 20' to 40' long. A sea serpent can choose to wrap around a ship and constrict; in this case, roll 2d10 for damage.
