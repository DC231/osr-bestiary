.. title: Golem, Flesh*
.. slug: golem-flesh
.. date: 2019-11-08 13:50:36.109736
.. tags: construct, hd-9, hd-9
.. description: Golem, Flesh*
.. type: text
.. image: jpg

+-----------------+------------+
| Armor Class:    | 20 ‡       |
+-----------------+------------+
| Hit Dice:       | 9** (+8)   |
+-----------------+------------+
| No. of Attacks: | 2 fists    |
+-----------------+------------+
| Damage:         | 2d8/2d8    |
+-----------------+------------+
| Movement:       | 30'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 5 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 1,225      |
+-----------------+------------+

A flesh golem is a ghoulish collection of stolen humanoid body parts, stitched together into a single composite form. No natural animal willingly tracks a flesh golem. The golem wears whatever clothing its creator desires, usually just a ragged pair of trousers. It has no possessions and no weapons. It stands 8 feet tall and weighs almost 500 pounds. A flesh golem cannot speak, although it can emit a hoarse roar of sorts. It walks and moves with a stiff-jointed gait, as if not in complete control of its body.

When a flesh golem enters combat, there is a cumulative 1% chance each round that its elemental spirit will break free. Such a golem will go on a rampage, attacking the nearest living creature or smashing some object smaller than itself if no creature is within reach, then moving on to cause more destruction. The golem’s creator, if within 60 feet, can try to regain control by speaking firmly and persuasively to the golem; he or she must make a save vs. Spells to succeed at this, and at least 1 round of time is required for each check. It takes 1 round of inactivity by the golem to reset the golem’s berserk chance to 0%.

A magical attack that deals cold or fire damage slows a flesh golem (as the **slow** spell) for 2d6 rounds, with no saving throw. A magical attack that deals electricity damage breaks any slow effect on the golem and heals 1 point of damage for every 3 full points of damage the attack would otherwise deal. If the amount of healing would cause the golem to exceed its full normal hit points, the excess is ignored. For example, a flesh golem hit by a lightning bolt heals 3 points of damage if the attack would have dealt 11 points of damage.
