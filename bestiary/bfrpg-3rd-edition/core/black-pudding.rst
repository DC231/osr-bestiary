.. title: Black Pudding*
.. slug: black-pudding
.. date: 2019-11-08 13:50:36.109736
.. tags: hd-10, hd-10, ooze
.. description: Black puddings are amorphous creatures that live only to eat. They inhabit underground areas in search of organic matter, living or dead.
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 14          |
+-----------------+-------------+
| Hit Dice:       | 10* (+9)    |
+-----------------+-------------+
| No. of Attacks: | 1 pseudopod |
+-----------------+-------------+
| Damage:         | 3d8         |
+-----------------+-------------+
| Movement:       | 20'         |
+-----------------+-------------+
| No. Appearing:  | 1           |
+-----------------+-------------+
| Save As:        | Fighter: 10 |
+-----------------+-------------+
| Morale:         | 12          |
+-----------------+-------------+
| Treasure Type:  | None        |
+-----------------+-------------+
| XP:             | 1,390       |
+-----------------+-------------+

Black puddings are amorphous creatures that live only to eat. They inhabit underground areas throughout the world, scouring caverns, ruins, and dungeons in search of organic matter, living or dead. They attack any creatures they encounter, lashing out with pseudopods or simply engulfing opponents with their bodies, which secrete acids that help them catch and digest their prey.

*If attacked with normal or magical weapons, or with lightning or electricity, a black pudding suffers no injury, but will be split into two puddings;* the GM should divide the original black pudding's hit dice between the two however he or she sees fit, with the limitation that neither pudding may have less than two hit dice. A two hit die black pudding is simply unharmed by such attacks, but cannot be split further.

*Cold or ice based attacks do not harm a black pudding, but such an attack will paralyze the pudding* for one round per die of damage the attack would normally cause. Other attack forms will affect a black pudding normally; the preferred method of killing one usually involves fire.

The typical black pudding measures 10 feet across and 2 feet thick, and weighs about 10,000 pounds. Black puddings of smaller sizes may be encountered, possibly as a result of the splitting described above.
