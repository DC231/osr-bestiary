.. title: Dinosaur, Pterodactyl (and Pteranodon)
.. slug: dinosaur-pterodactyl-and-pteranodon
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, flying, hd-1, hd-1
.. description: Dinosaur, Pterodactyl (and Pteranodon)
.. type: text
.. image: png

+-----------------+-----------------+----------------+
|                 | **Pterodactyl** | **Pteranodon** |
+=================+=================+================+
| Armor Class:    | 12              | 13             |
+-----------------+-----------------+----------------+
| Hit Dice:       | 1               | 5              |
+-----------------+-----------------+----------------+
| No. of Attacks: | 1 bite          | 1 bite         |
+-----------------+-----------------+----------------+
| Damage:         | 1d4             | 2d6            |
+-----------------+-----------------+----------------+
| Movement:       | Fly 60' (10')   | Fly 60' (15')  |
+-----------------+-----------------+----------------+
| No. Appearing:  | Wild 2d4        | Wild 1d4       |
+-----------------+-----------------+----------------+
| Save As:        | Fighter: 1      | Fighter: 3     |
+-----------------+-----------------+----------------+
| Morale:         | 7               | 8              |
+-----------------+-----------------+----------------+
| Treasure Type:  | None            | None           |
+-----------------+-----------------+----------------+
| XP:             | 25              | 360            |
+-----------------+-----------------+----------------+

Pterodactyls are prehistoric winged reptilian creatures, having a wingspan of around 25 to 30 inches. Though they eat mostly fish, they may attack smaller characters or scavenge unguarded packs.

Pteranodons are essentially giant-sized pterodactyls, having wingspans of 25 feet or more. They are predators, and may attack adventuring parties.
