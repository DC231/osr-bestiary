.. title: Orc
.. slug: orc
.. date: 2019-11-08 13:50:36.109736
.. tags: hd-1, hd-1, humanoid
.. description: Orc
.. type: text
.. image:

+-----------------+--------------------------+
| Armor Class:    | 14 (11)                  |
+-----------------+--------------------------+
| Hit Dice:       | 1                        |
+-----------------+--------------------------+
| No. of Attacks: | 1 weapon                 |
+-----------------+--------------------------+
| Damage:         | 1d8 or by weapon         |
+-----------------+--------------------------+
| Movement:       | 30' Unarmored 40'        |
+-----------------+--------------------------+
| No. Appearing:  | 2d4, Wild 3d6, Lair 10d6 |
+-----------------+--------------------------+
| Save As:        | Fighter: 1               |
+-----------------+--------------------------+
| Morale:         | 8                        |
+-----------------+--------------------------+
| Treasure Type:  | Q, R each; D in lair     |
+-----------------+--------------------------+
| XP:             | 25                       |
+-----------------+--------------------------+

Orcs are grotesque humanoids bent on war and domination. They have lupine ears, reddish eyes, truncated, upturned noses, and black hair (but very little body hair). An adult male orc is a little over 6 feet tall and weighs about 210 pounds; females are slightly smaller. Orcs prefer wearing vivid colors that many humans would consider unpleasant, such as blood red, mustard yellow, yellow-green, and deep purple. They utilize all manner of weapons and armor scavenged from battlefields.

Orcs have Darkvision to a range of 60'. They suffer a -1 attack penalty in bright sunlight or within the radius of a spell causing magical light. They speak their own rough and simple language, but many also speak some common or goblin.

One out of every eight orcs will be a warrior of 2 Hit Dice (75 XP). Orcs gain a +1 bonus to their morale if they are led by a warrior. In orc lairs, one out of every twelve will be a chieftain of 4 Hit Dice (240 XP) in chainmail with an Armor Class of 15 (11), a movement 20', and having a +1 bonus to damage due to strength. In lairs of 30 or more, there will be an orc king of 6 Hit Dice (500 XP), with an Armor Class of 16 (11), in chainmail with a shield, movement 20', and having a +2 bonus to damage. In the lair, orcs never fail a morale check as long as the orc king is alive. In addition, a lair has a chance equal to 1-2 on 1d6 of a shaman being present. A shaman is equivalent to a warrior orc statistically, but has Clerical abilities at level 1d4+1.
