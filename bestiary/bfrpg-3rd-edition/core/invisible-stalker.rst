.. title: Invisible Stalker
.. slug: invisible-stalker
.. date: 2019-11-08 13:50:36.109736
.. tags: elemental, hd-8, hd-8
.. description: Invisible Stalker
.. type: text
.. image:

+-----------------+-------------+
| Armor Class:    | 19          |
+-----------------+-------------+
| Hit Dice:       | 8*          |
+-----------------+-------------+
| No. of Attacks: | 1           |
+-----------------+-------------+
| Damage:         | 4d4         |
+-----------------+-------------+
| Movement:       | 40'         |
+-----------------+-------------+
| No. Appearing:  | 1 (special) |
+-----------------+-------------+
| Save As:        | Fighter: 8  |
+-----------------+-------------+
| Morale:         | 12          |
+-----------------+-------------+
| Treasure Type:  | None        |
+-----------------+-------------+
| XP:             | 945         |
+-----------------+-------------+

Invisible stalkers are creatures native to the Elemental Plane of Air. They sometimes serve wizards and sorcerers, who summon them to perform specific tasks.

A summoned invisible stalker undertakes whatever task the summoner commands, even if the task sends it hundreds or thousands of miles away. The creature follows a command until the task is completed and obeys only the summoner. However, it resents protracted missions or complex tasks and seeks to pervert its instructions accordingly.

Invisible stalkers have an amorphous form. A **detect invisible** spell shows only a dim outline of a cloud. Don't forget to apply the standard penalty of -4 on the attack die when an invisible stalker is attacked by a creature which is unable to see it.
