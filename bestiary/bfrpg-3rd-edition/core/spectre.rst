.. title: Spectre*
.. slug: spectre
.. date: 2019-11-08 13:50:36.109736
.. tags: hd-6, hd-6, undead
.. description: Spectre*
.. type: text
.. image:

+-----------------+-----------------------------+
| Armor Class:    | 17 ‡                        |
+-----------------+-----------------------------+
| Hit Dice:       | 6**                         |
+-----------------+-----------------------------+
| No. of Attacks: | 1 touch                     |
+-----------------+-----------------------------+
| Damage:         | Energy drain 2 levels/touch |
+-----------------+-----------------------------+
| Movement:       | Fly 100'                    |
+-----------------+-----------------------------+
| No. Appearing:  | 1d4, Lair 1d8               |
+-----------------+-----------------------------+
| Save As:        | Fighter: 6                  |
+-----------------+-----------------------------+
| Morale:         | 11                          |
+-----------------+-----------------------------+
| Treasure Type:  | E                           |
+-----------------+-----------------------------+
| XP:             | 610                         |
+-----------------+-----------------------------+

A spectre looks much as it did in life and can be easily recognized by those who knew the individual or have seen the individual’s face in a painting or a drawing. In many cases, the evidence of a violent death is visible on its body. A spectre is roughly human-sized and is weightless.

Like all undead, they may be Turned by Clerics and are immune to **sleep**, **charm** and **hold** magics. Due to their incorporeal nature, they cannot be harmed by non-magical weapons.
