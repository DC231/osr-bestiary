.. title: Mastodon
.. slug: mastodon
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, hd-15, hd-15
.. description: Mastodon
.. type: text
.. image:

+-----------------+----------------------+
| Armor Class:    | 18                   |
+-----------------+----------------------+
| Hit Dice:       | 15* (+11)            |
+-----------------+----------------------+
| No. of Attacks: | 2 tusks or 1 trample |
+-----------------+----------------------+
| Damage:         | 2d6/2d6 or 4d8       |
+-----------------+----------------------+
| Movement:       | 40' (15')            |
+-----------------+----------------------+
| No. Appearing:  | Wild 2d8             |
+-----------------+----------------------+
| Save As:        | Fighter: 15          |
+-----------------+----------------------+
| Morale:         | 8                    |
+-----------------+----------------------+
| Treasure Type:  | special              |
+-----------------+----------------------+
| XP:             | 2,975                |
+-----------------+----------------------+

Mastodons (and mammoths, which can also be represented by the above statistics) are hairy relatives of the elephant found in cold “lost world” areas.

A mastodon has no treasure as such, but the tusks of a mastodon are worth 2d4 x 100 gp.
