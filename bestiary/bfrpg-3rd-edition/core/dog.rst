.. title: Dog
.. slug: dog
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, hd-1, hd-1
.. description: Dog
.. type: text
.. image:

+-----------------+------------+---------------+
|                 | **Normal** | **Riding**    |
+=================+============+===============+
| Armor Class:    | 14         | 14            |
+-----------------+------------+---------------+
| Hit Dice:       | 1+1        | 2             |
+-----------------+------------+---------------+
| No. of Attacks: | 1 bite     | 1 bite        |
+-----------------+------------+---------------+
| Damage:         | 1d4 + hold | 1d4+1 + hold  |
+-----------------+------------+---------------+
| Movement:       | 50'        | 50'           |
+-----------------+------------+---------------+
| No. Appearing:  | Wild 3d4   | domestic only |
+-----------------+------------+---------------+
| Save As:        | Fighter: 1 | Fighter: 2    |
+-----------------+------------+---------------+
| Morale:         | 9          | 9             |
+-----------------+------------+---------------+
| Treasure Type:  | None       | None          |
+-----------------+------------+---------------+
| XP:             | 25         | 75            |
+-----------------+------------+---------------+

Normal dogs include most medium and large breeds, including wild dogs. After biting an opponent, a dog can hold on, doing 1d4 damage automatically every round, until killed or until the victim spends an attack breaking free (which requires a save vs. Death Ray, adjusted by the character's Strength bonus).

Riding dogs are a large breed, used primarily by Halflings for transport. They may be trained for war, and equipped with barding to improve their Armor Class. They can maintain a hold in the same way that normal dogs do. A light load for a riding dog is up to 150 pounds; a heavy load, up to 300 pounds.
