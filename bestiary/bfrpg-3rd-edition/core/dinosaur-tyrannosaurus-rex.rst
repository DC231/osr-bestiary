.. title: Dinosaur, Tyrannosaurus Rex
.. slug: dinosaur-tyrannosaurus-rex
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, hd-18, hd-18
.. description: Dinosaur, Tyrannosaurus Rex
.. type: text
.. image: png

+-----------------+------------+
| Armor Class:    | 23         |
+-----------------+------------+
| Hit Dice:       | 18 (+12)   |
+-----------------+------------+
| No. of Attacks: | 1 bite     |
+-----------------+------------+
| Damage:         | 6d6        |
+-----------------+------------+
| Movement:       | 40' (10')  |
+-----------------+------------+
| No. Appearing:  | Wild 1d4   |
+-----------------+------------+
| Save As:        | Fighter: 9 |
+-----------------+------------+
| Morale:         | 11         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 4,000      |
+-----------------+------------+

The tyrannosaurus rex is a bipedal carnivorous dinosaur. Despite its enormous size and 6-ton weight, a tyrannosaurus is a swift runner. Its head is nearly 6 feet long, and its teeth are from 3 to 6 inches in length. It is slightly more than 30 feet long from nose to tail. A tyrannosaurus pursues and eats just about anything it sees. Its tactics are simple – charge in and bite.

The statistics above can also be used to represent other large bipedal carnosaurs, such as the allosaurus.
