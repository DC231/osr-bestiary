.. title: Scorpion, Giant
.. slug: scorpion-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: hd-4, hd-4, insect
.. description: Scorpion, Giant
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 15                     |
+-----------------+------------------------+
| Hit Dice:       | 4*                     |
+-----------------+------------------------+
| No. of Attacks: | 2 claws/1 stinger      |
+-----------------+------------------------+
| Damage:         | 1d10/1d10/1d6 + poison |
+-----------------+------------------------+
| Movement:       | 50' (10')              |
+-----------------+------------------------+
| No. Appearing:  | 1d6, Wild 1d6          |
+-----------------+------------------------+
| Save As:        | Fighter: 2             |
+-----------------+------------------------+
| Morale:         | 11                     |
+-----------------+------------------------+
| Treasure Type:  | None                   |
+-----------------+------------------------+
| XP:             | 280                    |
+-----------------+------------------------+

Giant scorpions are quite large, sometimes as large as a donkey. They are aggressive predators and generally attack on sight. If a claw attack hits, the giant scorpion receives a +2 attack bonus with its stinger (but two claw hits do not give a double bonus). Those hit by the stinger must save vs. Poison or die. Giant scorpions are most commonly found in desert areas or caverns.
