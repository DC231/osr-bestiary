.. title: Golem, Clay*
.. slug: golem-clay
.. date: 2019-11-08 13:50:36.109736
.. tags: construct, hd-11, hd-11
.. description: Golem, Clay*
.. type: text
.. image: jpg

+-----------------+------------+
| Armor Class:    | 22 ‡       |
+-----------------+------------+
| Hit Dice:       | 11** (+9)  |
+-----------------+------------+
| No. of Attacks: | 1 fist     |
+-----------------+------------+
| Damage:         | 3d10       |
+-----------------+------------+
| Movement:       | 20'        |
+-----------------+------------+
| No. Appearing:  | 1          |
+-----------------+------------+
| Save As:        | Fighter: 6 |
+-----------------+------------+
| Morale:         | 12         |
+-----------------+------------+
| Treasure Type:  | None       |
+-----------------+------------+
| XP:             | 1,765      |
+-----------------+------------+

This golem has a humanoid body made from clay. A clay golem wears no clothing except for a metal or stiff leather garment around its hips. A clay golem cannot speak or make any vocal noise. It walks and moves with a slow, clumsy gait. It weighs around 600 pounds.

When a clay golem enters combat, there is a cumulative 1% chance each round that its elemental spirit will break free. Such a golem will go on a rampage, attacking the nearest living creature or smashing some object smaller than itself if no creature is within reach, then moving on to cause more destruction. Once a clay golem goes berserk, no known method can reestablish control.

The damage a clay golem deals doesn’t heal naturally, and magical healing cures only 1 point per die rolled (but add all bonuses normally).
