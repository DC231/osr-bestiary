.. title: Dragon
.. slug: dragon
.. date: 2019-11-08 13:50:36.109736
.. tags: dragon, flying
.. description: Dragon
.. type: text
.. image: png

Dragons are large (sometimes very large) winged reptilian monsters. Unlike wyverns, dragons have four legs as well as two wings; this is how experts distinguish “true” dragons from other large reptilian monsters. All dragons are long-lived, and they grow slowly for as long as they live. For this reason, they are described as having seven “age categories,” ranging from 3 less to 3 more hit dice than the average. For convenience, a table is provided following the description of each dragon type; this table shows the variation in hit dice, damage from their various attacks, and other features peculiar to dragons.

If one dragon is encountered, it is equally likely to be a male or female ranging from -2 to +3 hit dice (1d6-3); two are a mated pair ranging from -1 to +2 hit dice (1d4-2). If three or four are encountered, they consist of a mated pair plus one or two young of -3 hit dice in size. If this is the case, the parents receive a Morale of 12 in combat since they are protecting their young.

A dragon attacks with its powerful claws and bite, its long, whiplike tail, and most famously with its breath weapon. It prefers to fight on the wing, staying out of reach until it has worn down the enemy with the breath weapon (or possibly with spells, if the dragon can cast any). Older, more intelligent dragons are adept at sizing up the opposition and eliminating the most dangerous foes first (or avoiding them while picking off weaker enemies).

Each dragon can use its breath weapon as many times per day as it has hit dice, except that dragons of the lowest age category do not yet have a breath weapon. The breath may be used no more often than every other round, and the dragon may use its claws and tail at the same time. The tail swipe attack may only be used if there are opponents behind the dragon, while the claws may be used only on those opponents in front of the creature. Due to their serpentine necks, dragons may bite in any direction, even behind them.

The breath weapon of a dragon does 1d8 points of damage per hit die (so, a 7 hit die dragon does 7d8 points of damage with its breath). Victims may make a save vs. Dragon Breath for half damage. The breath weapon may be projected in any direction around the dragon, even behind, for the same reason that the dragon can bite those behind it.

There are three shapes (or areas of effect) which a dragon's breath weapon can cover. Each variety has a “normal” shape, which that type of dragon can use from the second age category (-2 hit dice) onward. Upon reaching the sixth age category (+2 hit dice), a dragon learns to shape its breath weapon into one of the other shapes (GM's option); at the seventh age category (+3 hit dice), the dragon is competent at producing all three shapes.

The shapes are:

**Cone Shaped:** The breath weapon begins at the dragon's mouth, and is about 2' wide at that point; it extends up to the maximum length (based on the dragon type and age) and is the maximum width at that point (again, as given for the dragon's type and age).

**Line Shaped:** The breath weapon is 5' wide and extends the given length in a straight line.

**Cloud Shaped:** The breath weapon covers an area up to the maximum given width (based on the dragon type and age) in both length and width (that is, the length figure given for the dragon type and age is ignored). A cloud-shaped breath weapon is, at most, 20' deep or high.

All dragons save for those of the lowest age category are able to speak Dragon. Each type has a given chance of “talking;” this is the chance that the dragon will know Common or a demi-human or humanoid language. Many who talk choose to learn Elvish. If the first roll for “talking” is successful, the GM may roll again, with each additional roll adding another language which the dragon may speak.

Some dragons learn to cast spells; the odds that a dragon can cast spells are the same as the odds that a dragon will learn to speak to lesser creatures, but each is rolled for separately.

Although goals and ideals vary among varieties, all dragons are covetous. They like to hoard wealth, collecting mounds of coins and gathering as many gems, jewels, and magic items as possible. Those with large hoards are loath to leave them for long, venturing out of their lairs only to patrol the immediate area or to get food. For dragons, there is no such thing as enough treasure. It’s pleasing to look at, and they bask in its radiance. Dragons like to make beds of their hoards, shaping nooks and mounds to fit their bodies. Note that, for most monsters, the Treasure Type given is for a lair of average numbers; for dragons, the Treasure Type is for a single individual of average age. Note also that mated pairs do not share treasure! Rather than adjusting the treasure amounts for the number of monsters, adjust for the age of the dragon; a dragon of the highest age category would have about double the given amount, while one of the next to lowest age category would have perhaps a tenth that amount (hatchlings have no treasure).
