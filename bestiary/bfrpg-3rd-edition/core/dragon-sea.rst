.. title: Dragon, Sea
.. slug: dragon-sea
.. date: 2019-11-08 13:50:36.109736
.. tags: amphibian, dragon, flying, hd-8, hd-8
.. description: Dragon, Sea
.. type: text
.. image:

+-----------------+----------------------------------+
| Armor Class:    | 19                               |
+-----------------+----------------------------------+
| Hit Dice:       | 8**                              |
+-----------------+----------------------------------+
| No. of Attacks: | 2 claws/1 bite or breath         |
+-----------------+----------------------------------+
| Damage:         | 1d6/1d6/3d8 or breath            |
+-----------------+----------------------------------+
| Movement:       | 10' Fly 60' (20') Swim 60' (15') |
+-----------------+----------------------------------+
| No. Appearing:  | 1, Wild 1, Lair 1d4              |
+-----------------+----------------------------------+
| Save As:        | Fighter: 8 (as Hit Dice)         |
+-----------------+----------------------------------+
| Morale:         | 8                                |
+-----------------+----------------------------------+
| Treasure Type:  | H                                |
+-----------------+----------------------------------+
| XP:             | 1,015                            |
+-----------------+----------------------------------+

Though they live in the water and are somewhat adapted to it, Sea Dragons still must breathe air, similar to dolphins or whales. A Sea Dragon may hold its breath up to three turns while swimming or performing other moderate activity.

These dragons have much the same physical structure as other dragons, but their feet are webbed and their tails are short, flat and broad; these adaptations help the sea dragon swim efficiently, but severely limit their ability to walk on dry land. Unlike other dragons, sea dragons do not have a tail attack. The breath weapon of a sea dragon is a cloud of steam; they are immune to damage from non-magical steam (including the breath weapon of another sea dragon), and suffer only half damage from magical steam attacks.

Young sea dragons are light bluish-gray in color (similar to dolphins), darkening to a deep slate color in older individuals.

Sea dragons are neutral in outlook, in much the same way as white dragons. They often maintain lairs in air-filled undersea caverns.

ERROR: One of the span's columns extends beyond the bounds of the table: [[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7]]
