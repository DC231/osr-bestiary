.. title: Lizard, Giant Tuatara
.. slug: lizard-giant-tuatara
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, hd-6, hd-6
.. description: Lizard, Giant Tuatara
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 16             |
+-----------------+----------------+
| Hit Dice:       | 6              |
+-----------------+----------------+
| No. of Attacks: | 2 claws/1 bite |
+-----------------+----------------+
| Damage:         | 1d4/1d4/2d6    |
+-----------------+----------------+
| Movement:       | 40' (10')      |
+-----------------+----------------+
| No. Appearing:  | 1d2, Wild 1d4  |
+-----------------+----------------+
| Save As:        | Fighter: 5     |
+-----------------+----------------+
| Morale:         | 6              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 500            |
+-----------------+----------------+

Giant tuataras are large, being 10' to 12' long, and heavily built. They are predators with a powerful shearing bite. Giant tuataras are more resistant to cold than most lizards, and are thus sometimes found hunting deep underground. They are also known to hibernate in cold weather.
