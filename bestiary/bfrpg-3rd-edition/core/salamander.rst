.. title: Salamander*
.. slug: salamander
.. date: 2019-11-08 13:50:36.109736
.. tags: elemental, hd-8, hd-8
.. description: Salamander*
.. type: text
.. image: png

+-----------------+-------------------------+---------------------------------+
|                 | Flame                   | Frost                           |
+=================+=========================+=================================+
| Armor Class:    | 19 ‡                    | 21 ‡                            |
+-----------------+-------------------------+---------------------------------+
| Hit Dice:       | 8*                      | 12* (+10)                       |
+-----------------+-------------------------+---------------------------------+
| No. of Attacks: | 2 claws/1 bite+ heat    | 4 claws/1 bite+ cold            |
+-----------------+-------------------------+---------------------------------+
| Damage:         | 1d4/1d4/1d8+ 1d8/round  | 1d6/1d6/1d6/1d6/2d6 + 1d8/round |
+-----------------+-------------------------+---------------------------------+
| Movement:       | 40'                     | 40'                             |
+-----------------+-------------------------+---------------------------------+
| No. Appearing:  | 1d4+1,Wild 2d4,Lair 2d4 | 1d3,Wild 1d3,Lair 1d3           |
+-----------------+-------------------------+---------------------------------+
| Save As:        | Fighter: 8              | Fighter: 12                     |
+-----------------+-------------------------+---------------------------------+
| Morale:         | 8                       | 9                               |
+-----------------+-------------------------+---------------------------------+
| Treasure Type:  | F                       | E                               |
+-----------------+-------------------------+---------------------------------+
| XP:             | 945                     | 1,975                           |
+-----------------+-------------------------+---------------------------------+

Salamanders are large, lizard-like creatures from the elemental planes. They are sometimes found on the material plane; they can arrive through naturally-occurring dimensional rifts, or they may be summoned by high-level Magic-Users. Due to their highly magical nature, they cannot be harmed by non-magical weapons.

**Flame salamanders** come from the Elemental Plane of Fire. They look like giant snakes, more than 12' long, with dragonlike heads and lizard forelimbs. Their scales are all the colors of flame, red and orange and yellow. A flame salamander is flaming hot, and all non-fire-resistant creatures within 20' of the monster suffer 1d8 points of damage per round from the heat. They are immune to damage from any fire or heat attack. Flame salamanders are intelligent; they speak the language of the Plane of Fire, and many will also know Elvish, Common, and/or Dragon.

**Frost salamanders** come from the Elemental Plane of Water. They look like giant lizards with six legs. Their scales are the colors of ice, white, pale gray and pale blue. Frost salamanders are very cold, and all non-cold-resistant creatures within 20' suffer 1d8 points of damage per round from the cold. Frost salamanders are completely immune to all types of cold-based attacks. They are quite intelligent; all speak the language of the Plane of Water, and many also speak Common, Elvish, and/or Dragon.

Flame and frost salamanders hate each other, and each type will attack the other on sight, in preference over any other foe. If summoned by a Magic-User, a salamander is often assigned to protect a location, doorway, or treasure hoard; in such a case, the salamander will attack anyone attempting to gain unauthorized access to the protected area. Those which arrive through natural rifts may have any goals or motivations the GM wishes, and thus may choose to parley, fight, or even ignore adventurers.
