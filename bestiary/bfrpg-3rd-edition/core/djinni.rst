.. title: Djinni*
.. slug: djinni
.. date: 2019-11-08 13:50:36.109736
.. tags: elemental, flying, hd-7, hd-7
.. description: Djinni*
.. type: text
.. image: png

+-----------------+-----------------------+
| Armor Class:    | 15 ‡                  |
+-----------------+-----------------------+
| Hit Dice:       | 7+1**                 |
+-----------------+-----------------------+
| No. of Attacks: | 1 fist or 1 whirlwind |
+-----------------+-----------------------+
| Damage:         | 2d8 or 2d6            |
+-----------------+-----------------------+
| Movement:       | 30' Fly 80'           |
+-----------------+-----------------------+
| No. Appearing:  | 1                     |
+-----------------+-----------------------+
| Save As:        | Fighter: 12           |
+-----------------+-----------------------+
| Morale:         | 12 (8)                |
+-----------------+-----------------------+
| Treasure Type:  | None                  |
+-----------------+-----------------------+
| XP:             | 800                   |
+-----------------+-----------------------+

The djinn (singular djinni) are humanoid creatures from the Elemental Plane of Air. A djinni in its natural form is about 10½ feet tall and weighs about 1,000 pounds.

Djinn disdain physical combat, preferring to use their magical powers and aerial abilities against foes. A djinni overmatched in combat usually takes flight and becomes a whirlwind (see below) to harass those who follow; the 12 morale reflects a djinni's absolute control over its own fear, but does not indicate that the creature will throw its life away easily. Use the “8” figure to determine whether an outmatched djinn decides to leave a combat.

Djinn have a number of magical powers, which can be used at will (that is, without needing magic words or gestures): **create food and drink**, creating tasty and nourishing food for up to 2d6 humans or similar creatures, once per day; become **invisible**, with unlimited uses per day; **create normal items**, creating up to 1,000 pounds of soft goods or wooden items of permanent nature or metal goods lasting at most a day, once per day; assume **gaseous form**, as the potion, up to one hour per day; and **create illusions**, as the spell **phantasmal force** but including sound as well as visual elements, three times per day.

Djinn may assume the form of a whirlwind at will, with no limit as to the number of times per day this power may be used; a djinni in whirlwind form fights as if it were an air elemental.

Due to their highly magical nature, djinn cannot be harmed by non-magical weapons. They are immune to normal cold, and suffer only half damage from magical attacks based on either cold or wind.
