.. title: Goblin
.. slug: goblin
.. date: 2019-11-08 13:50:36.109736
.. tags: goblinoid, hd-1, hd-1, humanoid
.. description: Goblin
.. type: text
.. image: png

+-----------------+---------------------------+
| Armor Class:    | 14 (11)                   |
+-----------------+---------------------------+
| Hit Dice:       | 1-1                       |
+-----------------+---------------------------+
| No. of Attacks: | 1 weapon                  |
+-----------------+---------------------------+
| Damage:         | 1d6 or by weapon          |
+-----------------+---------------------------+
| Movement:       | 20' Unarmored 30'         |
+-----------------+---------------------------+
| No. Appearing:  | 2d4 ,Wild 6d10, Lair 6d10 |
+-----------------+---------------------------+
| Save As:        | Fighter: 1                |
+-----------------+---------------------------+
| Morale:         | 7 or see below            |
+-----------------+---------------------------+
| Treasure Type:  | R each; C in lair         |
+-----------------+---------------------------+
| XP:             | 10                        |
+-----------------+---------------------------+

Goblins are small, wicked humanoids that favor ambushes, overwhelming odds, dirty tricks, and any other edge they can devise. An adult goblin stands 3 to 3½ feet tall and weigh 40 to 45 pounds. Its eyes are usually bright and crafty-looking, varying in color from red to yellow. A goblin’s skin color ranges from yellow through any shade of orange to a deep red; usually all members of a single tribe are about the same color. Goblins wear clothing of dark leather, tending toward drab, soiled-looking colors. They have Darkvision with a 30' range.

The statistics given above are for a standard Goblin in leather armor with a shield; they have a natural Movement rate of 30' and a natural Armor Class of 11.

Some goblins ride **dire wolves** into combat, and large groups of goblins will often employ them to track and attack their foes.

One out of every eight goblins will be a warrior of 3-3 Hit Dice (145 XP). Goblins gain a +1 bonus to their morale if they are led by a warrior. In a lair or other settlement, one out of every fifteen will be a chieftain of 5-5 Hit Dice (360 XP) in chainmail with an Armor Class of 15 (11) and movement of 10' that gains a +1 bonus to damage due to strength. In lairs or settlements of 30 or more goblins, there will be a goblin king of 7-7 Hit Dice (670 XP), with an Armor Class of 16 (11), wearing chainmail and carrying a shield, with a movement of 10', and having a +1 bonus to damage. Goblins have a +2 bonus to morale while their king is present (this is not cumulative with the bonus given by a warrior leader). In addition, a lair has a chance equal to 1 on 1d6 of a shaman being present (or 1-2 on 1d6 if a goblin king is present). A shaman is equivalent to a regular goblin statistically, but has Clerical abilities at level 1d4+1.
