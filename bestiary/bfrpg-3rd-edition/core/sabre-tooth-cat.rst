.. title: Sabre-Tooth Cat
.. slug: sabre-tooth-cat
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, hd-8, hd-8
.. description: Sabre-Tooth Cat
.. type: text
.. image:

+-----------------+--------------------+
| Armor Class:    | 14                 |
+-----------------+--------------------+
| Hit Dice:       | 8                  |
+-----------------+--------------------+
| No. of Attacks: | 2 claws/1 bite     |
+-----------------+--------------------+
| Damage:         | 1d6/1d6/2d8        |
+-----------------+--------------------+
| Movement:       | 50'                |
+-----------------+--------------------+
| No. Appearing:  | Wild 1d4, Lair 1d4 |
+-----------------+--------------------+
| Save As:        | Fighter: 8         |
+-----------------+--------------------+
| Morale:         | 10                 |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 875                |
+-----------------+--------------------+

The sabre-tooth cat, or *smilodon*, is a prehistoric great cat with very large canine teeth. They behave much as do mountain lions or jaguars.
