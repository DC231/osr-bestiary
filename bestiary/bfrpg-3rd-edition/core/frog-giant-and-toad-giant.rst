.. title: Frog, Giant (and Toad, Giant)
.. slug: frog-giant-and-toad-giant
.. date: 2019-11-08 13:50:36.109736
.. tags: amphibian, beast, hd-2, hd-2
.. description: Frog, Giant (and Toad, Giant)
.. type: text
.. image: png

+-----------------+--------------------+
| Armor Class:    | 13                 |
+-----------------+--------------------+
| Hit Dice:       | 2                  |
+-----------------+--------------------+
| No. of Attacks: | 1 tongue or 1 bite |
+-----------------+--------------------+
| Damage:         | grab or 1d4+1      |
+-----------------+--------------------+
| Movement:       | 30' Swim 30'       |
+-----------------+--------------------+
| No. Appearing:  | 1d4, Wild 1d4      |
+-----------------+--------------------+
| Save As:        | Fighter: 2         |
+-----------------+--------------------+
| Morale:         | 6                  |
+-----------------+--------------------+
| Treasure Type:  | None               |
+-----------------+--------------------+
| XP:             | 75                 |
+-----------------+--------------------+

Giant frogs are enlarged versions of the common frog; most resemble bullfrogs in appearance, but an adult giant frog is 3' long and weighs about 250 pounds. They are predators, but will normally only attack creatures smaller than themselves. Giant toads are statistically just like giant frogs; however, they are often found in “drier” areas as they do not have to maintain a wet skin surface.

A giant frog can stretch its tongue out up to 15' and drag up to dwarf-sized prey to its mouth; on every subsequent round, the victim is hit automatically. On a natural 20 attack roll, the victim is swallowed whole, taking 1d6 damage per round thereafter. Each giant frog can swallow only one such victim.
