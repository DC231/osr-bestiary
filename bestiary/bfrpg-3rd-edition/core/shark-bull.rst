.. title: Shark, Bull
.. slug: shark-bull
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast, hd-2, hd-2
.. description: Shark, Bull
.. type: text
.. image:

+-----------------+----------------+
| Armor Class:    | 13             |
+-----------------+----------------+
| Hit Dice:       | 2              |
+-----------------+----------------+
| No. of Attacks: | 1 bite         |
+-----------------+----------------+
| Damage:         | 2d4            |
+-----------------+----------------+
| Movement:       | Swim 60' (10') |
+-----------------+----------------+
| No. Appearing:  | Wild 3d6       |
+-----------------+----------------+
| Save As:        | Fighter: 2     |
+-----------------+----------------+
| Morale:         | 7              |
+-----------------+----------------+
| Treasure Type:  | None           |
+-----------------+----------------+
| XP:             | 75             |
+-----------------+----------------+

Bull sharks are so named because of their stocky, broad build. Male bull sharks can grow up to 7' long and weigh around 200 pounds, while females have been known to be up to 12' long, weighing up to 500 pounds.

Bull sharks are able to tolerate fresh water, and often travel up rivers in search of prey.
