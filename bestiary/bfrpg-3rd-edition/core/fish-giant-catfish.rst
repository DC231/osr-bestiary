.. title: Fish, Giant Catfish
.. slug: fish-giant-catfish
.. date: 2019-11-08 13:50:36.109736
.. tags: aquatic, beast, hd-8, hd-8
.. description: Fish, Giant Catfish
.. type: text
.. image:

+-----------------+---------------------------+
| Armor Class:    | 16                        |
+-----------------+---------------------------+
| Hit Dice:       | 8                         |
+-----------------+---------------------------+
| No. of Attacks: | 1 bite/2 fins             |
+-----------------+---------------------------+
| Damage:         | 2d8/1d4+poison/1d4+poison |
+-----------------+---------------------------+
| Movement:       | Swim 30' (10')            |
+-----------------+---------------------------+
| No. Appearing:  | Wild 1d2                  |
+-----------------+---------------------------+
| Save As:        | Fighter: 8                |
+-----------------+---------------------------+
| Morale:         | 8                         |
+-----------------+---------------------------+
| Treasure Type:  | None                      |
+-----------------+---------------------------+
| XP:             | 875                       |
+-----------------+---------------------------+

Giant catfish fins are edged with a natural poison that causes a painful burning sensation for 3d10 rounds if a save vs. Poison is failed. The pain causes the affected character or creature to suffer a -1 penalty on all attack rolls and saving throws; further poisonings will increase this penalty by -1 each, down to a maximum penalty of -5 as well as adding 6 rounds to the duration of the poison effect.

Because of its large size (15 to 20 feet long) and body design, a giant catfish cannot target more than one of its attacks on any single creature; that is, it cannot bite and fin the same opponent, nor use both fins on one victim.
