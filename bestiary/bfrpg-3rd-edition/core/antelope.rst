.. title: Antelope
.. slug: antelope
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, hd-1, hd-1
.. description: Antelope
.. type: text
.. image:

+-----------------+-------------------------------+
| Armor Class:    | 13                            |
+-----------------+-------------------------------+
| Hit Dice:       | 1 to 4                        |
+-----------------+-------------------------------+
| No. of Attacks: | 1 butt                        |
+-----------------+-------------------------------+
| Damage:         | 1d4 or 1d6 or 1d8             |
+-----------------+-------------------------------+
| Movement:       | 80' (10')                     |
+-----------------+-------------------------------+
| No. Appearing:  | Wild 3d10                     |
+-----------------+-------------------------------+
| Save As:        | Fighter: 1 to 4 (as Hit Dice) |
+-----------------+-------------------------------+
| Morale:         | 5 (7)                         |
+-----------------+-------------------------------+
| Treasure Type:  | None                          |
+-----------------+-------------------------------+
| XP:             | 25 - 240                      |
+-----------------+-------------------------------+

The statistics above represent common sorts of wild herd animals, including deer (1 hit die), aurochs (2 hit dice), elk or moose (3 hit dice) and bison (4 hit dice). Such creatures are often skittish and likely to flee if provoked, but males are more aggressive in the presence of females (the parenthesized morale applies in this case).
