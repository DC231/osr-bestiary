.. title: Hyenodon
.. slug: hyenodon
.. date: 2019-11-08 13:50:36.109736
.. tags: beast, hd-3, hd-3
.. description: Hyenodon
.. type: text
.. image:

+-----------------+------------------------+
| Armor Class:    | 13                     |
+-----------------+------------------------+
| Hit Dice:       | 3+1                    |
+-----------------+------------------------+
| No. of Attacks: | 1 bite                 |
+-----------------+------------------------+
| Damage:         | 1d8                    |
+-----------------+------------------------+
| Movement:       | 40'                    |
+-----------------+------------------------+
| No. Appearing:  | 1d6, 1d8 Wild Lair 1d8 |
+-----------------+------------------------+
| Save As:        | Fighter: 3             |
+-----------------+------------------------+
| Morale:         | 8                      |
+-----------------+------------------------+
| Treasure Type:  | None                   |
+-----------------+------------------------+
| XP:             | 145                    |
+-----------------+------------------------+

These ancient four legged predators are named for their tooth shape, and while they are not technically prehistoric hyenas, the statistics work for the giant prehistoric varieties of hyenas as well. Many varieties of hyenodons were smallish, sometimes no bigger than a common hyena, and the statistics for standard hyenas may be used for them. The above statistics are for the larger types of Hyenodons or giant varieties of Hyena. A notable feature is that their massively built skull features a long jaw (similar to that of a crocodile) full of teeth, with four great fangs.
