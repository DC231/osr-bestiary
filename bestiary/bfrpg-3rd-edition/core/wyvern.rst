.. title: Wyvern
.. slug: wyvern
.. date: 2019-11-08 13:50:36.109736
.. tags: dragon, hd-7, hd-7
.. description: Wyvern
.. type: text
.. image:

+-----------------+-------------------------------------------+
| Armor Class:    | 18                                        |
+-----------------+-------------------------------------------+
| Hit Dice:       | 7*                                        |
+-----------------+-------------------------------------------+
| No. of Attacks: | 1 bite/1 stinger or 2 talons/1 stinger    |
+-----------------+-------------------------------------------+
| Damage:         | 2d8/1d6 + poisonor 1d10/1d10/1d6 + poison |
+-----------------+-------------------------------------------+
| Movement:       | 30' (10') Fly 80' (15')                   |
+-----------------+-------------------------------------------+
| No. Appearing:  | Wild 1d6, Lair 1d6                        |
+-----------------+-------------------------------------------+
| Save As:        | Fighter: 7                                |
+-----------------+-------------------------------------------+
| Morale:         | 9                                         |
+-----------------+-------------------------------------------+
| Treasure Type:  | E                                         |
+-----------------+-------------------------------------------+
| XP:             | 735                                       |
+-----------------+-------------------------------------------+

A distant cousin to the true dragons, the wyvern is a huge flying lizard with a poisonous stinger in its tail. A wyvern’s body is 15 feet long, and dark brown to gray; half that length is tail. Its wingspan is about 20 feet. A wyvern weighs about one ton. They are built more like bats than lizards, having two legs and two wings; contrast this with true dragons, which have four legs and two wings.

Wyverns are of animal intelligence, but are excellent predators with good hunting abilities. When attacking they will make a loud hiss, or sometimes a deep-throated growl much like that of a bull alligator.

Wyverns attack nearly anything that isn’t obviously more powerful than themselves. A wyvern dives from the air, clawing at its opponent with its talons and stinging it to death. Any living creature hit by the wyvern's stinger must save vs. Poison or die. A wyvern can slash with its talons only when making a flyby attack or when landing.

If a wyvern hits with both its talons, it may attempt to carry off its victim; only victims weighing 300 pounds or less can be carried off, and the wyvern can only carry a victim for at most 6 rounds. While flying with a victim, the wyvern cannot make any further attacks against it, but of course if the victim makes a nuisance of itself (such as by injuring the wyvern), it may be dropped.
