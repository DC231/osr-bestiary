The Free Bestiary, Rebooted
===========================

This project is a reboot of the awesome `Free Bestiary <OSRB_>`_, which is based
on the creatures from Basic Fantasy Roleplaying 3rd Edition. However, that
project hasn't been updated in over three years, and Basic Fantasy Roleplaying
will soon be releasing a 4th edition, with new creatures, changed creatures, new
art, and a new licence.

I have already tagged the existing creatures as 3rd edition, and will be adding
the 4th edition bestiary over time. Additionally, new tags will be added
allowing search by numbers of hit dice, preferred environments, and anything
else we think of.

I will also be tightening up the attributions and licencing to make sure this project
has permission for all the content.

`Visit the rebooted bestiary here <OSRB_REBOOTED_>`_.


.. _OSRB: https://gitlab.com/clayadavis/osr-bestiary/
.. _OSRB_REBOOTED: https://dc231.gitlab.io/osr-bestiary/
