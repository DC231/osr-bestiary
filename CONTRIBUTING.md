This is a very small project, but contributing in a structured way is still helpful.

1. First, if you have bug reports, then please submit an issue to this repository.
2. If you want to contribute more, such as helping add tags to existing creatures, or helping add 4th edition creatures, then contact me to request addition to this project.
3. If you are familiar with the Fork & Pull Request model of open source development, then feel free to fork this repo, make your changes and then submit a PR. See the instructions below on setting up a local build environment.

Setting Up a Build Environment
------------------------------

1. Clone the repo to your local system.
2. Create a Python 3 virtual environment using the `requirements.txt` file and `pip`.
3. The `Makefile` contains targets for cleaning, building, and launching the debug Nikola server.
