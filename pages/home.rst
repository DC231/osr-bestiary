.. title: The Free Bestiary, Rebooted
.. slug: index
.. date: 2023-04-16 19:42:23 UTC+10:00
.. tags:
.. category:
.. link:
.. description: Searchable bestiary for old-school TTRPGs.
.. type: text
.. hidetitle: true

Welcome to *The Free Bestiary* for old-school, OSR, and old-school-inspired
RPGs. This project is open-source, mobile-friendly, and free of ads and
trackers. Check out the Bestiary_ section to get started.

This site, including all of the monster data, can be found on our our GitLab_
repository. You can help out by adding tags, correcting typos, or just raising
issues on the repo.

This project owes a huge debt to `Basic Fantasy Role-Playing Game`_ (BFRPG) for
providing an amazing and open-source collection of monsters to build upon.

This site is currently being updated to the BFRPG 4th Edition creatures. We are
also exploring adding new tags to allow filtering on different dimensions such
as hit dice and preferred environments.

.. _bestiary: link://category_index
.. _GitLab: https://gitlab.com/dc231/osr-bestiary
.. _`Basic Fantasy Role-Playing Game`: https://www.basicfantasy.org/
